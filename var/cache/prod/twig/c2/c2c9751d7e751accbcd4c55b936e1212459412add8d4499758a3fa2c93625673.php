<?php

/* :default:index.html.twig */
class __TwigTemplate_49937cd21410e5f8d004eafbcedcc9ba8ba896a57f504a0af1d4f9a27aeabcc4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", ":default:index.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        // line 4
        echo "
<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>


  
  ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 19
        echo "

";
    }

    // line 12
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 13
        echo "\t  
\t";
        // line 14
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 15
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
        }
        unset($context["asset_url"]);
        // line 17
        echo "\t  
\t ";
    }

    // line 23
    public function block_body($context, array $blocks = array())
    {
        // line 24
        echo "
<div class=\"right_col\" role=\"main\">
<div class=\"col-md-12\">
\t<div class=\"x_panel\">
\t  <div class=\"x_title\">
\t\t<h2>Racourcis</h2>
\t\t<ul class=\"nav navbar-right panel_toolbox\">
\t\t  <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
\t\t  </li>
\t\t  <li class=\"dropdown\">
\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>
\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t  <li><a href=\"#\">Settings 1</a>
\t\t\t  </li>
\t\t\t  <li><a href=\"#\">Settings 2</a>
\t\t\t  </li>
\t\t\t</ul>
\t\t  </li>
\t\t  <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
\t\t  </li>
\t\t</ul>
\t\t<div class=\"clearfix\"></div>
\t  </div>
\t  <div class=\"x_content\">
\t\t\t
\t\t<div class=\"animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12\">
\t\t\t<a href=\"/incident/new\">
\t<div class=\"tile-stats\">
\t  <div class=\"icon\">  <i class=\"fa fa-plus\"></i> </div>
\t  <div class=\"count\">&nbsp</div>
\t  <h3>Ajouter Incident</h3>
\t  <p>Ajouter incident Metier/Application</p>
\t</div>
\t\t\t\t </a> 
\t\t\t  </div>
\t
\t\t<div class=\"animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12\">
\t<div class=\"tile-stats\">
\t  <div class=\"icon\"><i class=\"fa fa-bug\"></i></div>
\t  <div class=\"count\"> ";
        // line 63
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["incidents"]) ? $context["incidents"] : null)), "html", null, true);
        echo "</div>
\t  <h3> Incidents En cours </h3>
\t  <p>";
        // line 65
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["incidents"]) ? $context["incidents"] : null)), "html", null, true);
        echo " incidents sont en cours</p>
\t\t\t</div>
\t\t</div>
\t\t  
\t\t  <a href=\"/template/list\">
\t\t<div class=\"animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12\">
\t<div class=\"tile-stats\">
\t  <div class=\"icon\"><i class=\"fa fa-envelope\"></i></div>
\t  <div class=\"count\">  &nbsp </div>
\t  <h3> Mail d'information  </h3>
\t  <p> afficher les templates prédifinies </p>
\t\t\t</div>
\t\t</div>
\t\t  </a>
\t\t  
\t  </div>
\t</div>
\t\t\t  </div>
\t
\t<!-- Liste des incidents en cours -->
\t\t\t<div class=\"col-md-12\">
\t<div class=\"x_panel\">
\t  <div class=\"x_title\">
\t\t<h2>Incidents en cours</h2>
\t\t<ul class=\"nav navbar-right panel_toolbox\">
\t\t  <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
\t\t  </li>
\t\t  <li class=\"dropdown\">
\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>
\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t  <li><a href=\"#\">Settings 1</a>
\t\t\t  </li>
\t\t\t  <li><a href=\"#\">Settings 2</a>
\t\t\t  </li>
\t\t\t</ul>
\t\t  </li>
\t\t  <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
\t\t  </li>
\t\t</ul>
\t\t<div class=\"clearfix\"></div>
\t  </div>
\t  <div class=\"x_content\">
\t\t\t
\t\t\t<table class=\"table\">
\t\t  <thead>
\t\t\t<tr>
\t\t\t  <th>Titre</th>
\t\t\t  <th>Metiers Impactés</th>
\t\t\t  <th> Prochain Pos prévue </th>
\t\t\t  <th> Impact </th>
\t\t\t  
\t\t\t  <th> Actions </th>
\t\t\t</tr>
\t\t  </thead>
\t\t  <tbody>
\t\t\t
\t\t\t
\t\t\t ";
        // line 122
        if ((isset($context["incidents"]) ? $context["incidents"] : null)) {
            // line 123
            echo "\t\t\t\t ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["incidents"]) ? $context["incidents"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["incident"]) {
                // line 124
                echo "\t\t\t\t\t<tr>
\t\t\t\t\t <td> ";
                // line 125
                echo twig_escape_filter($this->env, $this->getAttribute($context["incident"], "Titre", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t <td>
\t\t\t\t\t\t ";
                // line 127
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["incident"], "impactMetier", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["metier"]) {
                    // line 128
                    echo "\t\t\t\t\t\t\t<span class=\"tag\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["metier"], "nomMetier", array()), "html", null, true);
                    echo "</span>
\t\t\t\t\t\t ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['metier'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 130
                echo "\t\t\t\t\t\t ";
                if (array_key_exists("application", $context)) {
                    // line 131
                    echo "\t\t\t\t\t\t ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["incident"], "impactApplication", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["application"]) {
                        // line 132
                        echo "\t\t\t\t\t\t\t<span class=\"tag\">";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["application"], "nomApplication", array()), "html", null, true);
                        echo " - ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["application"], "metiers", array()), "get", array(0 => 0), "method"), "getNomMetier", array(), "method"), "html", null, true);
                        echo "</span>
\t\t\t\t\t\t ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['application'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 134
                    echo "\t\t\t\t\t ";
                }
                // line 135
                echo "\t\t\t\t\t </td>
\t\t\t\t\t <td>
\t\t\t\t\t\t";
                // line 137
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["incident"], "dateFin", array()), "F jS \\a\\t g:ia", "Europe/Paris"), "html", null, true);
                echo "
\t\t\t\t\t </td>
\t\t\t\t\t <td>
\t\t\t\t\t\t\t";
                // line 140
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["incident"], "impact", array()), "nomImpact", array()), "html", null, true);
                echo "
\t\t\t\t\t </td>
\t\t\t\t\t <td>
\t\t\t\t\t\t<a href=\"";
                // line 143
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("pos_add", array("idIncident" => $this->getAttribute($context["incident"], "Id", array()))), "html", null, true);
                echo "\"\" class=\"btn btn-primary btn-xs\"><i class=\"fa fa-info\"></i> POS </a>
\t\t\t\t\t\t<a href=\"";
                // line 144
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("diffusion_send", array("idIncident" => $this->getAttribute($context["incident"], "Id", array()), "idClient" => $this->getAttribute($this->getAttribute($context["incident"], "client", array()), "getId", array(), "method"))), "html", null, true);
                echo "\" class=\"btn btn-info btn-xs\"><i class=\"fa fa-envelope\"></i> Email </a>
\t\t\t\t\t\t<a href=\"";
                // line 145
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_update", array("idIncident" => $this->getAttribute($context["incident"], "Id", array()), "idClient" => $this->getAttribute($this->getAttribute($context["incident"], "client", array()), "getId", array(), "method"))), "html", null, true);
                echo "\" class=\"btn btn-info btn-xs\"><i class=\"fa fa-bug\"></i> Modifier </a>
\t\t\t\t\t\t
\t\t\t\t\t </td>
\t\t\t\t\t 
\t\t  
\t
\t
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['incident'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 153
            echo "\t";
        }
        // line 154
        echo "\t\t

\t\t  </tbody>
\t\t</table>
\t\t  
\t\t  
\t  </div>
\t</div>
\t\t\t  </div>
\t
\t
\t<!-- fin liste des incidents en cours -->

\t</div>



  ";
    }

    // line 174
    public function block_javascripts($context, array $blocks = array())
    {
        // line 175
        echo "\t\t";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 176
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
        }
        unset($context["asset_url"]);
        // line 178
        echo "\t  ";
    }

    public function getTemplateName()
    {
        return ":default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  402 => 178,  346 => 176,  341 => 175,  338 => 174,  317 => 154,  314 => 153,  300 => 145,  296 => 144,  292 => 143,  286 => 140,  280 => 137,  276 => 135,  273 => 134,  262 => 132,  257 => 131,  254 => 130,  245 => 128,  241 => 127,  236 => 125,  233 => 124,  228 => 123,  226 => 122,  166 => 65,  161 => 63,  120 => 24,  117 => 23,  112 => 17,  62 => 15,  58 => 14,  55 => 13,  52 => 12,  46 => 19,  44 => 12,  34 => 4,  31 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":default:index.html.twig", "C:\\xampp2\\htdocs\\symfony\\app/Resources\\views/default/index.html.twig");
    }
}
