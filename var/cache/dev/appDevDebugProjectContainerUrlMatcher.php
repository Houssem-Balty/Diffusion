<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevDebugProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ($pathinfo === '/_profiler/open') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        if (0 === strpos($pathinfo, '/application')) {
            // application_list
            if ($pathinfo === '/application/list') {
                return array (  '_controller' => 'AppBundle\\Controller\\ApplicationController::listAction',  '_route' => 'application_list',);
            }

            // application_add
            if ($pathinfo === '/application/add') {
                return array (  '_controller' => 'AppBundle\\Controller\\ApplicationController::addAction',  '_route' => 'application_add',);
            }

            if (0 === strpos($pathinfo, '/application/de')) {
                // application_deactivate
                if (0 === strpos($pathinfo, '/application/deactivate') && preg_match('#^/application/deactivate/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'application_deactivate')), array (  '_controller' => 'AppBundle\\Controller\\ApplicationController::deactivateAction',));
                }

                // application_details
                if (0 === strpos($pathinfo, '/application/details') && preg_match('#^/application/details/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'application_details')), array (  '_controller' => 'AppBundle\\Controller\\ApplicationController::detailsAction',));
                }

            }

            // application_update
            if (0 === strpos($pathinfo, '/application/update') && preg_match('#^/application/update/(?P<idApplication>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'application_update')), array (  '_controller' => 'AppBundle\\Controller\\ApplicationController::updateAction',));
            }

        }

        if (0 === strpos($pathinfo, '/c')) {
            if (0 === strpos($pathinfo, '/categorie')) {
                // categorie_list
                if ($pathinfo === '/categorie/list') {
                    return array (  '_controller' => 'AppBundle\\Controller\\CategorieController::listAction',  '_route' => 'categorie_list',);
                }

                // categorie_add
                if ($pathinfo === '/categorie/add') {
                    return array (  '_controller' => 'AppBundle\\Controller\\CategorieController::addAction',  '_route' => 'categorie_add',);
                }

                if (0 === strpos($pathinfo, '/categorie/de')) {
                    // categorie_deactivate
                    if (0 === strpos($pathinfo, '/categorie/deactivate') && preg_match('#^/categorie/deactivate/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'categorie_deactivate')), array (  '_controller' => 'AppBundle\\Controller\\CategorieController::deactivateAction',));
                    }

                    // categorie_delete
                    if (0 === strpos($pathinfo, '/categorie/delete') && preg_match('#^/categorie/delete/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'categorie_delete')), array (  '_controller' => 'AppBundle\\Controller\\CategorieController::deleteAction',));
                    }

                }

                // categorie_update
                if (0 === strpos($pathinfo, '/categorie/update') && preg_match('#^/categorie/update/(?P<idCategorie>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'categorie_update')), array (  '_controller' => 'AppBundle\\Controller\\CategorieController::updateAction',));
                }

            }

            if (0 === strpos($pathinfo, '/client')) {
                // client_list
                if ($pathinfo === '/client/list') {
                    return array (  '_controller' => 'AppBundle\\Controller\\ClientController::listAction',  '_route' => 'client_list',);
                }

                // client_add
                if ($pathinfo === '/client/add') {
                    return array (  '_controller' => 'AppBundle\\Controller\\ClientController::addAction',  '_route' => 'client_add',);
                }

                // client_update
                if (0 === strpos($pathinfo, '/client/update') && preg_match('#^/client/update/(?P<idClient>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'client_update')), array (  '_controller' => 'AppBundle\\Controller\\ClientController::updateAction',));
                }

            }

        }

        // home
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'home');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'home',);
        }

        // settings
        if (rtrim($pathinfo, '/') === '/settings') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'settings');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::testAction',  '_route' => 'settings',);
        }

        if (0 === strpos($pathinfo, '/diffusion/send')) {
            // diffusion_send
            if (preg_match('#^/diffusion/send/(?P<idIncident>[^/]++)/(?P<idClient>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'diffusion_send')), array (  '_controller' => 'AppBundle\\Controller\\DiffusionController::sendAction',));
            }

            // template_diffusion_send
            if (preg_match('#^/diffusion/send/(?P<idTemplate>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'template_diffusion_send')), array (  '_controller' => 'AppBundle\\Controller\\DiffusionController::templateSendAction',));
            }

        }

        if (0 === strpos($pathinfo, '/i')) {
            if (0 === strpos($pathinfo, '/information/diffusion/history')) {
                // information_diffusion_history
                if ($pathinfo === '/information/diffusion/history') {
                    return array (  '_controller' => 'AppBundle\\Controller\\DiffusionController::informationHistoryAction',  '_route' => 'information_diffusion_history',);
                }

                // information_diffusion_client_history
                if (preg_match('#^/information/diffusion/history/(?P<idTemplate>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'information_diffusion_client_history')), array (  '_controller' => 'AppBundle\\Controller\\DiffusionController::informationHistoryTemlpateAction',));
                }

            }

            if (0 === strpos($pathinfo, '/impact')) {
                if (0 === strpos($pathinfo, '/impactapplication')) {
                    // impactapplication_list
                    if ($pathinfo === '/impactapplication/list') {
                        return array (  '_controller' => 'AppBundle\\Controller\\ImpactApplicationController::listAction',  '_route' => 'impactapplication_list',);
                    }

                    // impactapplication_add
                    if ($pathinfo === '/impactapplication/add') {
                        return array (  '_controller' => 'AppBundle\\Controller\\ImpactApplicationController::addAction',  '_route' => 'impactapplication_add',);
                    }

                    // impactapplication_update
                    if (0 === strpos($pathinfo, '/impactapplication/update') && preg_match('#^/impactapplication/update/(?P<idConfig>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'impactapplication_update')), array (  '_controller' => 'AppBundle\\Controller\\ImpactApplicationController::updateAction',));
                    }

                }

                // impact_list
                if ($pathinfo === '/impact/list') {
                    return array (  '_controller' => 'AppBundle\\Controller\\ImpactController::listAction',  '_route' => 'impact_list',);
                }

                // impact_details
                if (0 === strpos($pathinfo, '/impact/details') && preg_match('#^/impact/details/(?P<idImpact>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'impact_details')), array (  '_controller' => 'AppBundle\\Controller\\ImpactController::detailsAction',));
                }

                // impact_add
                if ($pathinfo === '/impact/add') {
                    return array (  '_controller' => 'AppBundle\\Controller\\ImpactController::addAction',  '_route' => 'impact_add',);
                }

                // impact_update
                if (0 === strpos($pathinfo, '/impact/update') && preg_match('#^/impact/update/(?P<idImpact>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'impact_update')), array (  '_controller' => 'AppBundle\\Controller\\ImpactController::updateAction',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/metier')) {
            // impact_delete
            if (0 === strpos($pathinfo, '/metier/delete') && preg_match('#^/metier/delete/(?P<idImpact>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'impact_delete')), array (  '_controller' => 'AppBundle\\Controller\\ImpactController::deleteAction',));
            }

            // impact_activate
            if (0 === strpos($pathinfo, '/metier/activate') && preg_match('#^/metier/activate/(?P<idImpact>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'impact_activate')), array (  '_controller' => 'AppBundle\\Controller\\ImpactController::activateAction',));
            }

        }

        if (0 === strpos($pathinfo, '/i')) {
            if (0 === strpos($pathinfo, '/impactmetier')) {
                // impactmetier_list
                if ($pathinfo === '/impactmetier/list') {
                    return array (  '_controller' => 'AppBundle\\Controller\\ImpactMetierController::listAction',  '_route' => 'impactmetier_list',);
                }

                // impactmetier_add
                if ($pathinfo === '/impactmetier/add') {
                    return array (  '_controller' => 'AppBundle\\Controller\\ImpactMetierController::addAction',  '_route' => 'impactmetier_add',);
                }

                // impactmetier_update
                if (0 === strpos($pathinfo, '/impactmetier/update') && preg_match('#^/impactmetier/update/(?P<idConfig>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'impactmetier_update')), array (  '_controller' => 'AppBundle\\Controller\\ImpactMetierController::updateAction',));
                }

            }

            if (0 === strpos($pathinfo, '/incident')) {
                // incident_list
                if ($pathinfo === '/incident/list') {
                    return array (  '_controller' => 'AppBundle\\Controller\\IncidentController::listAction',  '_route' => 'incident_list',);
                }

                // incident_add
                if (0 === strpos($pathinfo, '/incident/add') && preg_match('#^/incident/add/(?P<idClient>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'incident_add')), array (  '_controller' => 'AppBundle\\Controller\\IncidentController::addAction',));
                }

                // nouvel_incident
                if (rtrim($pathinfo, '/') === '/incident/new') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'nouvel_incident');
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\IncidentController::ajouterAction',  '_route' => 'nouvel_incident',);
                }

                // metier_custom_add_metier
                if (0 === strpos($pathinfo, '/incident/metier') && preg_match('#^/incident/metier/(?P<idMetier>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'metier_custom_add_metier')), array (  '_controller' => 'AppBundle\\Controller\\IncidentController::customAddAction',));
                }

                // incident_custom_add_application
                if (0 === strpos($pathinfo, '/incident/application') && preg_match('#^/incident/application/(?P<idApplication>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'incident_custom_add_application')), array (  '_controller' => 'AppBundle\\Controller\\IncidentController::customappAddAction',));
                }

                // incident_clone
                if (0 === strpos($pathinfo, '/incident/clone') && preg_match('#^/incident/clone/(?P<idIncident>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'incident_clone')), array (  '_controller' => 'AppBundle\\Controller\\IncidentController::cloneAction',));
                }

                // incident_update
                if (0 === strpos($pathinfo, '/incident/update') && preg_match('#^/incident/update/(?P<idIncident>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'incident_update')), array (  '_controller' => 'AppBundle\\Controller\\IncidentController::updateAction',));
                }

                // incident_delete
                if (0 === strpos($pathinfo, '/incident/delete') && preg_match('#^/incident/delete/(?P<idIncident>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'incident_delete')), array (  '_controller' => 'AppBundle\\Controller\\IncidentController::deleteAction',));
                }

                // incident_history
                if (0 === strpos($pathinfo, '/incident/history') && preg_match('#^/incident/history/(?P<idIncident>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'incident_history')), array (  '_controller' => 'AppBundle\\Controller\\IncidentController::getHistory',));
                }

                // incident_clore
                if (0 === strpos($pathinfo, '/incident/clore') && preg_match('#^/incident/clore/(?P<idIncident>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'incident_clore')), array (  '_controller' => 'AppBundle\\Controller\\IncidentController::cloreAction',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/user/log')) {
            // main_page
            if ($pathinfo === '/user/login') {
                return array (  '_controller' => 'AppBundle\\Controller\\LoginController::loginAction',  '_route' => 'main_page',);
            }

            // logout
            if ($pathinfo === '/user/logout') {
                return array (  '_controller' => 'AppBundle\\Controller\\LoginController::logoutAction',  '_route' => 'logout',);
            }

        }

        if (0 === strpos($pathinfo, '/m')) {
            if (0 === strpos($pathinfo, '/mail')) {
                // mail_list
                if ($pathinfo === '/mailing/list') {
                    return array (  '_controller' => 'AppBundle\\Controller\\MailController::listAction',  '_route' => 'mail_list',);
                }

                // mail_add
                if ($pathinfo === '/mail/add') {
                    return array (  '_controller' => 'AppBundle\\Controller\\MailController::addAction',  '_route' => 'mail_add',);
                }

                if (0 === strpos($pathinfo, '/mailinglist')) {
                    // mailinglist_list
                    if ($pathinfo === '/mailinglist/list') {
                        return array (  '_controller' => 'AppBundle\\Controller\\MailingListController::listAction',  '_route' => 'mailinglist_list',);
                    }

                    // mailinglist_details
                    if (0 === strpos($pathinfo, '/mailinglist/details') && preg_match('#^/mailinglist/details/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'mailinglist_details')), array (  '_controller' => 'AppBundle\\Controller\\MailingListController::detailsAction',));
                    }

                    // mailinglist_add
                    if ($pathinfo === '/mailinglist/add') {
                        return array (  '_controller' => 'AppBundle\\Controller\\MailingListController::addAction',  '_route' => 'mailinglist_add',);
                    }

                    // mailinglist_delete
                    if (0 === strpos($pathinfo, '/mailinglist/delete') && preg_match('#^/mailinglist/delete/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'mailinglist_delete')), array (  '_controller' => 'AppBundle\\Controller\\MailingListController::deleteAction',));
                    }

                    // mailinglist_update
                    if (0 === strpos($pathinfo, '/mailinglist/update') && preg_match('#^/mailinglist/update/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'mailinglist_update')), array (  '_controller' => 'AppBundle\\Controller\\MailingListController::updateAction',));
                    }

                }

            }

            if (0 === strpos($pathinfo, '/metier')) {
                // metier_list
                if ($pathinfo === '/metier/list') {
                    return array (  '_controller' => 'AppBundle\\Controller\\MetierController::listAction',  '_route' => 'metier_list',);
                }

                // metier_details
                if (0 === strpos($pathinfo, '/metier/details') && preg_match('#^/metier/details/(?P<idMetier>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'metier_details')), array (  '_controller' => 'AppBundle\\Controller\\MetierController::detailsAction',));
                }

                // metier_add
                if ($pathinfo === '/metier/add') {
                    return array (  '_controller' => 'AppBundle\\Controller\\MetierController::addAction',  '_route' => 'metier_add',);
                }

                // metier_update
                if (0 === strpos($pathinfo, '/metier/update') && preg_match('#^/metier/update/(?P<idMetier>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'metier_update')), array (  '_controller' => 'AppBundle\\Controller\\MetierController::updateAction',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/pos')) {
            // pos_list
            if (0 === strpos($pathinfo, '/pos/list') && preg_match('#^/pos/list/(?P<idIncident>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pos_list')), array (  '_controller' => 'AppBundle\\Controller\\PosController::listAction',));
            }

            if (0 === strpos($pathinfo, '/pos/add')) {
                // pos_add
                if (preg_match('#^/pos/add/(?P<idIncident>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pos_add')), array (  '_controller' => 'AppBundle\\Controller\\PosController::addAction',));
                }

                // pos_update
                if (preg_match('#^/pos/add/(?P<idIncident>[^/]++)/(?P<idPos>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'pos_update')), array (  '_controller' => 'AppBundle\\Controller\\PosController::updateAction',));
                }

            }

            // pos_delete
            if (0 === strpos($pathinfo, '/pos/delete') && preg_match('#^/pos/delete/(?P<idIncident>[^/]++)/(?P<idPos>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'pos_delete')), array (  '_controller' => 'AppBundle\\Controller\\PosController::deleteAction',));
            }

        }

        if (0 === strpos($pathinfo, '/template')) {
            // template_list
            if (rtrim($pathinfo, '/') === '/template/list') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'template_list');
                }

                return array (  '_controller' => 'AppBundle\\Controller\\TemplateController::listAction',  '_route' => 'template_list',);
            }

            // template_add
            if ($pathinfo === '/template/add') {
                return array (  '_controller' => 'AppBundle\\Controller\\TemplateController::addAction',  '_route' => 'template_add',);
            }

            // template_update
            if (0 === strpos($pathinfo, '/template/update') && preg_match('#^/template/update/(?P<idTemplate>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'template_update')), array (  '_controller' => 'AppBundle\\Controller\\TemplateController::updateAction',));
            }

            // template_delete
            if (0 === strpos($pathinfo, '/template/delete') && preg_match('#^/template/delete/(?P<idTemplate>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'template_delete')), array (  '_controller' => 'AppBundle\\Controller\\TemplateController::deleteAction',));
            }

        }

        // client_information_list
        if ($pathinfo === '/client/information/list') {
            return array (  '_controller' => 'AppBundle\\Controller\\TemplateController::listClientsAction',  '_route' => 'client_information_list',);
        }

        if (0 === strpos($pathinfo, '/user')) {
            // user_test
            if ($pathinfo === '/user/test') {
                return array (  '_controller' => 'AppBundle\\Controller\\UserController::testAction',  '_route' => 'user_test',);
            }

            // user_list
            if ($pathinfo === '/user/list') {
                return array (  '_controller' => 'AppBundle\\Controller\\UserController::listAction',  '_route' => 'user_list',);
            }

            // user_add
            if ($pathinfo === '/user/add') {
                return array (  '_controller' => 'AppBundle\\Controller\\UserController::addAction',  '_route' => 'user_add',);
            }

            // user_persist
            if (0 === strpos($pathinfo, '/user/persist/{') && preg_match('#^/user/persist/\\{(?P<userName>[^/]+)\\}$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_persist')), array (  '_controller' => 'AppBundle\\Controller\\UserController::persistAction',));
            }

            // user_delete
            if (0 === strpos($pathinfo, '/user/delete') && preg_match('#^/user/delete/(?P<userID>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'user_delete')), array (  '_controller' => 'AppBundle\\Controller\\UserController::deleteAction',));
            }

        }

        // toolbox_getuser
        if (0 === strpos($pathinfo, '/toolbox/getuser?motclef') && preg_match('#^/toolbox/getuser\\?motclef\\=(?P<motclef>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'toolbox_getuser')), array (  '_controller' => 'AppBundle\\Controller\\toolBoxController::getUserFromLdap',));
        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if (rtrim($pathinfo, '/') === '/profile') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ($pathinfo === '/profile/edit') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_profile_edit;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }
            not_fos_user_profile_edit:

        }

        if (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/register')) {
                // fos_user_registration_register
                if (rtrim($pathinfo, '/') === '/register') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_registration_register;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
                }
                not_fos_user_registration_register:

                if (0 === strpos($pathinfo, '/register/c')) {
                    // fos_user_registration_check_email
                    if ($pathinfo === '/register/check-email') {
                        if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                            $allow = array_merge($allow, array('GET', 'HEAD'));
                            goto not_fos_user_registration_check_email;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
                    }
                    not_fos_user_registration_check_email:

                    if (0 === strpos($pathinfo, '/register/confirm')) {
                        // fos_user_registration_confirm
                        if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirm;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                        }
                        not_fos_user_registration_confirm:

                        // fos_user_registration_confirmed
                        if ($pathinfo === '/register/confirmed') {
                            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                                $allow = array_merge($allow, array('GET', 'HEAD'));
                                goto not_fos_user_registration_confirmed;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                        }
                        not_fos_user_registration_confirmed:

                    }

                }

            }

            if (0 === strpos($pathinfo, '/resetting')) {
                // fos_user_resetting_request
                if ($pathinfo === '/resetting/request') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_request;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
                }
                not_fos_user_resetting_request:

                // fos_user_resetting_send_email
                if ($pathinfo === '/resetting/send-email') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_resetting_send_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
                }
                not_fos_user_resetting_send_email:

                // fos_user_resetting_check_email
                if ($pathinfo === '/resetting/check-email') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_fos_user_resetting_check_email;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
                }
                not_fos_user_resetting_check_email:

                // fos_user_resetting_reset
                if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_resetting_reset;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
                }
                not_fos_user_resetting_reset:

            }

        }

        // fos_user_change_password
        if ($pathinfo === '/profile/change-password') {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
        }
        not_fos_user_change_password:

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
