<?php

/* @FOSUser/Profile/edit.html.twig */
class __TwigTemplate_c2f9d2ca90a37233e21102e89bdebfc1beaabe9e0b5f8fb15d58dc1deba06403 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Profile/edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7f5a10ff7ab26f235e7654e53d18fb42f02f8592e9d1c81370b307e608586343 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f5a10ff7ab26f235e7654e53d18fb42f02f8592e9d1c81370b307e608586343->enter($__internal_7f5a10ff7ab26f235e7654e53d18fb42f02f8592e9d1c81370b307e608586343_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7f5a10ff7ab26f235e7654e53d18fb42f02f8592e9d1c81370b307e608586343->leave($__internal_7f5a10ff7ab26f235e7654e53d18fb42f02f8592e9d1c81370b307e608586343_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_290e592ef987f0e2757d157e817acca2f6fdfd7ce86d5896f5fa32bf8d3c53df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_290e592ef987f0e2757d157e817acca2f6fdfd7ce86d5896f5fa32bf8d3c53df->enter($__internal_290e592ef987f0e2757d157e817acca2f6fdfd7ce86d5896f5fa32bf8d3c53df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "@FOSUser/Profile/edit.html.twig", 4)->display($context);
        
        $__internal_290e592ef987f0e2757d157e817acca2f6fdfd7ce86d5896f5fa32bf8d3c53df->leave($__internal_290e592ef987f0e2757d157e817acca2f6fdfd7ce86d5896f5fa32bf8d3c53df_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Profile/edit.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Profile\\edit.html.twig");
    }
}
