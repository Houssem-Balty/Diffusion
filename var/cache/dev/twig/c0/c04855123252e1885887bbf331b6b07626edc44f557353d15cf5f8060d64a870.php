<?php

/* FOSUserBundle:Resetting:check_email.html.twig */
class __TwigTemplate_87232f18b1509e088f0d073fbb6947d72c93f920c7477b34169b505e5260e450 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e9c80cefa07c1d3fa30e14c1a51d4b6143017be476643e4c3ee5684b8d791139 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e9c80cefa07c1d3fa30e14c1a51d4b6143017be476643e4c3ee5684b8d791139->enter($__internal_e9c80cefa07c1d3fa30e14c1a51d4b6143017be476643e4c3ee5684b8d791139_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e9c80cefa07c1d3fa30e14c1a51d4b6143017be476643e4c3ee5684b8d791139->leave($__internal_e9c80cefa07c1d3fa30e14c1a51d4b6143017be476643e4c3ee5684b8d791139_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_bfbbc5a57cc6db49b8196be8959c7f73cb589af9ebe760192a2e2053dcdfeb09 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bfbbc5a57cc6db49b8196be8959c7f73cb589af9ebe760192a2e2053dcdfeb09->enter($__internal_bfbbc5a57cc6db49b8196be8959c7f73cb589af9ebe760192a2e2053dcdfeb09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => (isset($context["tokenLifetime"]) ? $context["tokenLifetime"] : null)), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_bfbbc5a57cc6db49b8196be8959c7f73cb589af9ebe760192a2e2053dcdfeb09->leave($__internal_bfbbc5a57cc6db49b8196be8959c7f73cb589af9ebe760192a2e2053dcdfeb09_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 7,  40 => 6,  34 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Resetting:check_email.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Resetting/check_email.html.twig");
    }
}
