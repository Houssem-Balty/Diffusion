<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_4e2dd73bed3cd90be1e5b6356561c66519a03b8769b822cedc54f610a107abdd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b9444bca25710770dc3908b9f9d7e0fb0e7ef53e12230370d75a7f8349bcf3bd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b9444bca25710770dc3908b9f9d7e0fb0e7ef53e12230370d75a7f8349bcf3bd->enter($__internal_b9444bca25710770dc3908b9f9d7e0fb0e7ef53e12230370d75a7f8349bcf3bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_b9444bca25710770dc3908b9f9d7e0fb0e7ef53e12230370d75a7f8349bcf3bd->leave($__internal_b9444bca25710770dc3908b9f9d7e0fb0e7ef53e12230370d75a7f8349bcf3bd_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_c1709c193b01587e44865f782e2ae2b7807855134b1e6012de60f97070fd704e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c1709c193b01587e44865f782e2ae2b7807855134b1e6012de60f97070fd704e->enter($__internal_c1709c193b01587e44865f782e2ae2b7807855134b1e6012de60f97070fd704e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_c1709c193b01587e44865f782e2ae2b7807855134b1e6012de60f97070fd704e->leave($__internal_c1709c193b01587e44865f782e2ae2b7807855134b1e6012de60f97070fd704e_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
