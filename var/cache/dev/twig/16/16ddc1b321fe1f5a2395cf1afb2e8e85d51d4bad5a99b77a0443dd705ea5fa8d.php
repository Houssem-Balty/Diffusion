<?php

/* :incident:incident.html.twig */
class __TwigTemplate_26368ed917ebf68bfa80ce013b208b1dfc3750f936d6842bf5521a2512b1d58a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", ":incident:incident.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b52faee8f9c3af8b1de5b8974d6d9d7c2925353857874769e61451356ecb018 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0b52faee8f9c3af8b1de5b8974d6d9d7c2925353857874769e61451356ecb018->enter($__internal_0b52faee8f9c3af8b1de5b8974d6d9d7c2925353857874769e61451356ecb018_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":incident:incident.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0b52faee8f9c3af8b1de5b8974d6d9d7c2925353857874769e61451356ecb018->leave($__internal_0b52faee8f9c3af8b1de5b8974d6d9d7c2925353857874769e61451356ecb018_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_bf3f9f6f3de4d4f703f87ea215a07334c119f3715470a00be43cb737e0f48f86 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bf3f9f6f3de4d4f703f87ea215a07334c119f3715470a00be43cb737e0f48f86->enter($__internal_bf3f9f6f3de4d4f703f87ea215a07334c119f3715470a00be43cb737e0f48f86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "
<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

<script src=\"https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js\"></script>
<link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css\"/>

  ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 20
        echo "
";
        
        $__internal_bf3f9f6f3de4d4f703f87ea215a07334c119f3715470a00be43cb737e0f48f86->leave($__internal_bf3f9f6f3de4d4f703f87ea215a07334c119f3715470a00be43cb737e0f48f86_prof);

    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_186155ab5c2eed426c2a4b57294402d0b6cf12ffffb10ca98b5cbf04743986ed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_186155ab5c2eed426c2a4b57294402d0b6cf12ffffb10ca98b5cbf04743986ed->enter($__internal_186155ab5c2eed426c2a4b57294402d0b6cf12ffffb10ca98b5cbf04743986ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 14
        echo "
    ";
        // line 15
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 16
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
        // line 18
        echo "
     ";
        
        $__internal_186155ab5c2eed426c2a4b57294402d0b6cf12ffffb10ca98b5cbf04743986ed->leave($__internal_186155ab5c2eed426c2a4b57294402d0b6cf12ffffb10ca98b5cbf04743986ed_prof);

    }

    // line 23
    public function block_body($context, array $blocks = array())
    {
        $__internal_096fd6d9fda5ed0094e333f5d8de3ee45b0e8c80ed849aff8cd39c098f185637 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_096fd6d9fda5ed0094e333f5d8de3ee45b0e8c80ed849aff8cd39c098f185637->enter($__internal_096fd6d9fda5ed0094e333f5d8de3ee45b0e8c80ed849aff8cd39c098f185637_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 24
        echo "

<div class=\"right_col\" role=\"main\" style=\"min-height: 3742px;\">
\t<div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2> Suivie Incident <small>Liste des incidents</small></h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li class=\"dropdown\">
                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>
                        <ul class=\"dropdown-menu\" role=\"menu\">
                          <li><a href=\"#\">Settings 1</a>
                          </li>
                          <li><a href=\"#\">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">





                    <div id=\"datatable_wrapper\" class=\"dataTables_wrapper form-inline dt-bootstrap no-footer\">
\t\t\t\t\t\t<div class=\"row\">

\t\t\t\t\t\t\t<div class=\"col-sm-6\">

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t<table id=\"datatable\" class=\"table table-striped table-bordered dataTable no-footer\" role=\"grid\" aria-describedby=\"datatable_info\">

\t\t\t\t\t  <thead>
                        <tr role=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting_asc\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-sort=\"ascending\" aria-label=\"Name: activate to sort column descending\" style=\"width: 143px;\">#</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting_asc\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-sort=\"ascending\" aria-label=\"Name: activate to sort column descending\" style=\"width: 143px;\">Titre</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Position: activate to sort column ascending\" style=\"width: 236px;\">Client</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Office: activate to sort column ascending\" style=\"width: 105px;\">Statut</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Age: activate to sort column ascending\" style=\"width: 50px;\">Impact Metier</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Start date: activate to sort column ascending\" style=\"width: 101px;\">Impact Application</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Salary: activate to sort column ascending\" style=\"width: 77px;\">Date Debut</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Salary: activate to sort column ascending\" style=\"width: 77px;\">Date Fin</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Salary: activate to sort column ascending\" style=\"width: 77px;\">Niveau impact</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Salary: activate to sort column ascending\" style=\"width: 77px;\">Action</th>
\t\t\t\t\t\t\t\t\t\t\t\t</tr>
                      </thead>


                      <tbody>

                     \t\t";
        // line 83
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["incidents"]) ? $context["incidents"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["incident"]) {
            // line 84
            echo "\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td> ";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "id", array()), "html", null, true);
            echo "     </td>
\t\t\t\t\t\t\t<td> ";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "titre", array()), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t<td> ";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "client", array()), "getClientName", array(), "method"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t<td> ";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "statut", array()), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t";
            // line 90
            if ($this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "impactMetier", array())) {
                // line 91
                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                $context["metierImpactes"] = $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "impactMetier", array());
                // line 92
                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["metierImpactes"]) ? $context["metierImpactes"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["metierImpacte"]) {
                    // line 93
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li> \t";
                    // line 94
                    echo twig_escape_filter($this->env, $this->getAttribute($context["metierImpacte"], "nomMetier", array()), "html", null, true);
                    echo " </li>
\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['metierImpacte'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 97
                echo "\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 98
                echo "\t\t\t\t\t\t\t\t\t\t Pas d'impact metier
\t\t\t\t\t\t\t\t\t\t ";
            }
            // line 100
            echo "\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t<td>

\t\t\t\t\t\t\t";
            // line 103
            if ($this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "impactApplication", array())) {
                // line 104
                echo "\t\t\t\t\t\t\t\t\t";
                $context["applicationImpactees"] = $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "impactApplication", array());
                // line 105
                echo "\t\t\t\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["applicationImpactees"]) ? $context["applicationImpactees"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["applicationImpactee"]) {
                    // line 106
                    echo "\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t<li> \t";
                    // line 107
                    echo twig_escape_filter($this->env, $this->getAttribute($context["applicationImpactee"], "nomApplication", array()), "html", null, true);
                    echo " </li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['applicationImpactee'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 110
                echo "\t\t\t\t\t\t\t";
            } else {
                // line 111
                echo "\t\t\t\t\t\t\t Pas d'impact metier
\t\t\t\t\t\t\t ";
            }
            // line 113
            echo "
\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t";
            // line 116
            if (($this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "dateDebut", array()) != null)) {
                // line 117
                echo "\t\t\t\t\t\t\t\t";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "dateDebut", array()), "F jS \\a\\t g:ia", "Europe/Paris"), "html", null, true);
                echo "
\t\t\t\t\t\t\t";
            } else {
                // line 119
                echo "\t\t\t\t\t\t\t\t<span style=\"color:red\">\tNon connue  </span>
\t\t\t\t\t\t\t";
            }
            // line 121
            echo "\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t";
            // line 123
            if (($this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "dateFin", array()) != null)) {
                // line 124
                echo "\t\t\t\t\t\t\t\t";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "dateFin", array()), "F jS \\a\\t g:ia"), "html", null, true);
                echo "
\t\t\t\t\t\t\t";
            } else {
                // line 126
                echo "\t\t\t\t\t\t\t\t<span style=\"color:red\">En cours  </span>
\t\t\t\t\t\t\t";
            }
            // line 128
            echo "
\t\t\t\t\t\t\t</td>

\t\t\t\t\t\t\t<td> ";
            // line 131
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "impact", array()), "nomImpact", array()), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t<td> <a href=\"";
            // line 132
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("pos_add", array("idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\"> <i class=\"glyphicon glyphicon-plus\"></i> Pos </a>
\t\t\t\t\t\t\t<a href=\"";
            // line 133
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_history", array("idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\"> <i class=\"glyphicon glyphicon-menu-hamburger\"></i> Historique </a>
\t\t\t\t\t\t\t <a href=\"";
            // line 134
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_delete", array("idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\" onclick=\"return confirm('Vous êtes sure de vouloir supprimer lincident ? Cette action est irréversible ')\"> <i class=\"glyphicon glyphicon-trash\"></i> Supprimer  </a>
\t\t\t\t\t\t\t <a href=\"";
            // line 135
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("diffusion_send", array("idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()), "idClient" => $this->getAttribute($this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "client", array()), "getId", array(), "method"))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\"> <i class=\"glyphicon glyphicon-envelope\"></i> Envoyer mail </a>
\t\t\t\t\t\t\t <a href=\"";
            // line 136
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_clone", array("idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()), "idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\"> <i class=\"glyphicon glyphicon-duplicate\"> </i>  Cloner </a>
\t\t\t\t\t\t\t <a href=\"";
            // line 137
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_clore", array("idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()), "idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\"> <i class=\"glyphicon glyphicon-thumbs-up\"> </i> clore  </a>
\t\t\t\t\t\t\t <a href=\"";
            // line 138
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_update", array("idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()), "idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\"><i class=\"glyphicon glyphicon-pencil\"> </i>  Mettre à jour </a>

\t\t\t\t\t\t\t</td>



\t\t\t\t\t\t</div>
\t\t\t\t\t</tr>
 \t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['incident'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 147
        echo "

\t\t\t\t\t\t</tbody>
                    </table>
\t\t\t\t\t</div></div>


\t\t\t\t  </div>



\t\t\t\t\t</div>
                  </div>
                </div>





</div>




";
        
        $__internal_096fd6d9fda5ed0094e333f5d8de3ee45b0e8c80ed849aff8cd39c098f185637->leave($__internal_096fd6d9fda5ed0094e333f5d8de3ee45b0e8c80ed849aff8cd39c098f185637_prof);

    }

    // line 173
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_632fba11a0c15e88c2a05b5490b4ca8cd32666eafadd81a98f3fd5e281bf2043 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_632fba11a0c15e88c2a05b5490b4ca8cd32666eafadd81a98f3fd5e281bf2043->enter($__internal_632fba11a0c15e88c2a05b5490b4ca8cd32666eafadd81a98f3fd5e281bf2043_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 174
        echo "\t    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 175
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
        }
        unset($context["asset_url"]);
        // line 177
        echo "      ";
        
        $__internal_632fba11a0c15e88c2a05b5490b4ca8cd32666eafadd81a98f3fd5e281bf2043->leave($__internal_632fba11a0c15e88c2a05b5490b4ca8cd32666eafadd81a98f3fd5e281bf2043_prof);

    }

    public function getTemplateName()
    {
        return ":incident:incident.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  468 => 177,  412 => 175,  407 => 174,  401 => 173,  370 => 147,  355 => 138,  351 => 137,  347 => 136,  343 => 135,  339 => 134,  335 => 133,  331 => 132,  327 => 131,  322 => 128,  318 => 126,  312 => 124,  310 => 123,  306 => 121,  302 => 119,  296 => 117,  294 => 116,  289 => 113,  285 => 111,  282 => 110,  273 => 107,  270 => 106,  265 => 105,  262 => 104,  260 => 103,  255 => 100,  251 => 98,  248 => 97,  239 => 94,  236 => 93,  231 => 92,  228 => 91,  226 => 90,  221 => 88,  217 => 87,  213 => 86,  209 => 85,  206 => 84,  202 => 83,  141 => 24,  135 => 23,  127 => 18,  77 => 16,  73 => 15,  70 => 14,  64 => 13,  56 => 20,  54 => 13,  43 => 4,  37 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":incident:incident.html.twig", "C:\\xampp2\\htdocs\\symfony\\app/Resources\\views/incident/incident.html.twig");
    }
}
