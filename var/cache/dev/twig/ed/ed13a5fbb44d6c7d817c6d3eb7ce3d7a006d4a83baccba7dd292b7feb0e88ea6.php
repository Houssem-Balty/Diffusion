<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_7d46c1d29b8415801074f06982834f6a6d1b17db1e4bc1a08a17dc1acd074ef5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7a8306b0b484720995f8c6a0fc6078ebbb408bd31ab33003aedfcbeb261709cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7a8306b0b484720995f8c6a0fc6078ebbb408bd31ab33003aedfcbeb261709cd->enter($__internal_7a8306b0b484720995f8c6a0fc6078ebbb408bd31ab33003aedfcbeb261709cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_4ffac47a95e04ce967995a4beb2835aa55ce311fc8783ef169ce8b3d8613be8a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4ffac47a95e04ce967995a4beb2835aa55ce311fc8783ef169ce8b3d8613be8a->enter($__internal_4ffac47a95e04ce967995a4beb2835aa55ce311fc8783ef169ce8b3d8613be8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7a8306b0b484720995f8c6a0fc6078ebbb408bd31ab33003aedfcbeb261709cd->leave($__internal_7a8306b0b484720995f8c6a0fc6078ebbb408bd31ab33003aedfcbeb261709cd_prof);

        
        $__internal_4ffac47a95e04ce967995a4beb2835aa55ce311fc8783ef169ce8b3d8613be8a->leave($__internal_4ffac47a95e04ce967995a4beb2835aa55ce311fc8783ef169ce8b3d8613be8a_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_6634507812457045a5d9f862667daf8837299068a26281d8932529e145c530d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6634507812457045a5d9f862667daf8837299068a26281d8932529e145c530d4->enter($__internal_6634507812457045a5d9f862667daf8837299068a26281d8932529e145c530d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_8e73f52bcf6f41f8f3c4e3df633373ca704f7688c434d98e15bc11e4a58afdc5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e73f52bcf6f41f8f3c4e3df633373ca704f7688c434d98e15bc11e4a58afdc5->enter($__internal_8e73f52bcf6f41f8f3c4e3df633373ca704f7688c434d98e15bc11e4a58afdc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_8e73f52bcf6f41f8f3c4e3df633373ca704f7688c434d98e15bc11e4a58afdc5->leave($__internal_8e73f52bcf6f41f8f3c4e3df633373ca704f7688c434d98e15bc11e4a58afdc5_prof);

        
        $__internal_6634507812457045a5d9f862667daf8837299068a26281d8932529e145c530d4->leave($__internal_6634507812457045a5d9f862667daf8837299068a26281d8932529e145c530d4_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_584a1f2bce6845616022e342d2dfbb0e24b76dd1594a17ee53039b47edba6255 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_584a1f2bce6845616022e342d2dfbb0e24b76dd1594a17ee53039b47edba6255->enter($__internal_584a1f2bce6845616022e342d2dfbb0e24b76dd1594a17ee53039b47edba6255_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_22d8f0af502c5404289cc86719c3d4fcbf5a9e9c805653b43dadf9ee3a9eb580 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_22d8f0af502c5404289cc86719c3d4fcbf5a9e9c805653b43dadf9ee3a9eb580->enter($__internal_22d8f0af502c5404289cc86719c3d4fcbf5a9e9c805653b43dadf9ee3a9eb580_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_22d8f0af502c5404289cc86719c3d4fcbf5a9e9c805653b43dadf9ee3a9eb580->leave($__internal_22d8f0af502c5404289cc86719c3d4fcbf5a9e9c805653b43dadf9ee3a9eb580_prof);

        
        $__internal_584a1f2bce6845616022e342d2dfbb0e24b76dd1594a17ee53039b47edba6255->leave($__internal_584a1f2bce6845616022e342d2dfbb0e24b76dd1594a17ee53039b47edba6255_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_c04b28860f558f205d265cefd5ba3eab3b1638577d16e464b4b3105bc8cc5a87 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c04b28860f558f205d265cefd5ba3eab3b1638577d16e464b4b3105bc8cc5a87->enter($__internal_c04b28860f558f205d265cefd5ba3eab3b1638577d16e464b4b3105bc8cc5a87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_4d768c7f4a08a941367bf6163137f6542e3219c5f007ac75c8e06ea3b5e7696c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d768c7f4a08a941367bf6163137f6542e3219c5f007ac75c8e06ea3b5e7696c->enter($__internal_4d768c7f4a08a941367bf6163137f6542e3219c5f007ac75c8e06ea3b5e7696c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_4d768c7f4a08a941367bf6163137f6542e3219c5f007ac75c8e06ea3b5e7696c->leave($__internal_4d768c7f4a08a941367bf6163137f6542e3219c5f007ac75c8e06ea3b5e7696c_prof);

        
        $__internal_c04b28860f558f205d265cefd5ba3eab3b1638577d16e464b4b3105bc8cc5a87->leave($__internal_c04b28860f558f205d265cefd5ba3eab3b1638577d16e464b4b3105bc8cc5a87_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception_full.html.twig");
    }
}
