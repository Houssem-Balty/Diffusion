<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_abc526a18fb6b7bfaa5287e2ef36adb4571ba47ac358024097e22db52c3a9843 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ed57df4442e9572cd8f9066e8b9993d2a884fa4554d44114bd5337de90abfa41 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed57df4442e9572cd8f9066e8b9993d2a884fa4554d44114bd5337de90abfa41->enter($__internal_ed57df4442e9572cd8f9066e8b9993d2a884fa4554d44114bd5337de90abfa41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_ed57df4442e9572cd8f9066e8b9993d2a884fa4554d44114bd5337de90abfa41->leave($__internal_ed57df4442e9572cd8f9066e8b9993d2a884fa4554d44114bd5337de90abfa41_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/search_widget.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\search_widget.html.php");
    }
}
