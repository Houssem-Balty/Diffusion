<?php

/* impactApplication/impactApplication.html.twig */
class __TwigTemplate_2d43e33b3f197da0099ba3acbe6789706168807f8328f22a34350afcb8ec59a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("loginTemplate.html.twig", "impactApplication/impactApplication.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "loginTemplate.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9d04dfd674d5eee0e3f40434fc6c2bdd6b631678d9de18b65f5c62c9a8106af7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d04dfd674d5eee0e3f40434fc6c2bdd6b631678d9de18b65f5c62c9a8106af7->enter($__internal_9d04dfd674d5eee0e3f40434fc6c2bdd6b631678d9de18b65f5c62c9a8106af7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "impactApplication/impactApplication.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9d04dfd674d5eee0e3f40434fc6c2bdd6b631678d9de18b65f5c62c9a8106af7->leave($__internal_9d04dfd674d5eee0e3f40434fc6c2bdd6b631678d9de18b65f5c62c9a8106af7_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_8aa357ba6915f341a96c423757c379b4881bba4fb36029f1c25e57eb47026782 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8aa357ba6915f341a96c423757c379b4881bba4fb36029f1c25e57eb47026782->enter($__internal_8aa357ba6915f341a96c423757c379b4881bba4fb36029f1c25e57eb47026782_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "
";
        
        $__internal_8aa357ba6915f341a96c423757c379b4881bba4fb36029f1c25e57eb47026782->leave($__internal_8aa357ba6915f341a96c423757c379b4881bba4fb36029f1c25e57eb47026782_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_d888778a3e0160f48205a395d372767a156822b0c8fa82f85fc9d1a09a0bd060 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d888778a3e0160f48205a395d372767a156822b0c8fa82f85fc9d1a09a0bd060->enter($__internal_d888778a3e0160f48205a395d372767a156822b0c8fa82f85fc9d1a09a0bd060_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "
\t\t<h1> Les impacts </h1>

    <table class=\"table\">
      <tr>
        <td> # </td>
        <td> NOM IMPACT </td>
        <td> Configuration </td>
        <td> ACTION </td>
      </tr>

    ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["applications"]) ? $context["applications"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["impactapplication"]) {
            // line 20
            echo "     <tr>
        <td> ";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["impactapplication"], 0, array(), "array"), "Id", array()), "html", null, true);
            echo " </td>
        <td>  ";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["impactapplication"], 0, array(), "array"), "nomImpactApplication", array()), "html", null, true);
            echo " </td>
        <td>
          <ul>
          ";
            // line 25
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["impactapplication"], 0, array(), "array"), "Configuration", array()));
            foreach ($context['_seq'] as $context["application"] => $context["impact"]) {
                // line 26
                echo "          <li> ";
                echo twig_escape_filter($this->env, $context["application"], "html", null, true);
                echo " : ";
                echo twig_escape_filter($this->env, $context["impact"], "html", null, true);
                echo " </li>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['application'], $context['impact'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "          </ul>
          </td>
        <td> <a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_add_custom_appli", array("idConfig" => $this->getAttribute($this->getAttribute($context["impactapplication"], 0, array(), "array"), "Id", array()))), "html", null, true);
            echo "\"> Utiliser la Configuration </a> </td>

      </tr>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['impactapplication'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "

\t";
        // line 36
        $this->displayBlock('javascripts', $context, $blocks);
        // line 41
        echo "
";
        
        $__internal_d888778a3e0160f48205a395d372767a156822b0c8fa82f85fc9d1a09a0bd060->leave($__internal_d888778a3e0160f48205a395d372767a156822b0c8fa82f85fc9d1a09a0bd060_prof);

    }

    // line 36
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_e13278d4c148e10aea2450b60194394d91e0f0efe9bec0db9cd88a877872d19c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e13278d4c148e10aea2450b60194394d91e0f0efe9bec0db9cd88a877872d19c->enter($__internal_e13278d4c148e10aea2450b60194394d91e0f0efe9bec0db9cd88a877872d19c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 37
        echo "\t    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 38
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
        }
        unset($context["asset_url"]);
        // line 40
        echo "\t";
        
        $__internal_e13278d4c148e10aea2450b60194394d91e0f0efe9bec0db9cd88a877872d19c->leave($__internal_e13278d4c148e10aea2450b60194394d91e0f0efe9bec0db9cd88a877872d19c_prof);

    }

    public function getTemplateName()
    {
        return "impactApplication/impactApplication.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  196 => 40,  140 => 38,  135 => 37,  129 => 36,  121 => 41,  119 => 36,  115 => 34,  105 => 30,  101 => 28,  90 => 26,  86 => 25,  80 => 22,  76 => 21,  73 => 20,  69 => 19,  56 => 8,  50 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "impactApplication/impactApplication.html.twig", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\impactApplication\\impactApplication.html.twig");
    }
}
