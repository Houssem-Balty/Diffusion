<?php

/* /application/applicationAdd.html.twig */
class __TwigTemplate_99bae420d5fcec78f647e65ddcedbc67746a26023972a279f3b990e8224bf715 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", "/application/applicationAdd.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b124ca23715ed01de757970ffe703b998c5cbcd0c18f10b8407db50dffccd26f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b124ca23715ed01de757970ffe703b998c5cbcd0c18f10b8407db50dffccd26f->enter($__internal_b124ca23715ed01de757970ffe703b998c5cbcd0c18f10b8407db50dffccd26f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "/application/applicationAdd.html.twig"));

        $__internal_1f3b8dd5c7e986534427c58a7acdf204ada846995192c62b3914191d6deaf110 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f3b8dd5c7e986534427c58a7acdf204ada846995192c62b3914191d6deaf110->enter($__internal_1f3b8dd5c7e986534427c58a7acdf204ada846995192c62b3914191d6deaf110_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "/application/applicationAdd.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b124ca23715ed01de757970ffe703b998c5cbcd0c18f10b8407db50dffccd26f->leave($__internal_b124ca23715ed01de757970ffe703b998c5cbcd0c18f10b8407db50dffccd26f_prof);

        
        $__internal_1f3b8dd5c7e986534427c58a7acdf204ada846995192c62b3914191d6deaf110->leave($__internal_1f3b8dd5c7e986534427c58a7acdf204ada846995192c62b3914191d6deaf110_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_92f5807858597a4039aae4c435a4af000dab04c44631f1942bcc9345b9cc9ea0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_92f5807858597a4039aae4c435a4af000dab04c44631f1942bcc9345b9cc9ea0->enter($__internal_92f5807858597a4039aae4c435a4af000dab04c44631f1942bcc9345b9cc9ea0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_41303d1162c469a6abcc25afbef66dbdfd405fa6fd91cf6e1463c9eb7a9156f8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_41303d1162c469a6abcc25afbef66dbdfd405fa6fd91cf6e1463c9eb7a9156f8->enter($__internal_41303d1162c469a6abcc25afbef66dbdfd405fa6fd91cf6e1463c9eb7a9156f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
  
  ";
        // line 11
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "
";
        
        $__internal_41303d1162c469a6abcc25afbef66dbdfd405fa6fd91cf6e1463c9eb7a9156f8->leave($__internal_41303d1162c469a6abcc25afbef66dbdfd405fa6fd91cf6e1463c9eb7a9156f8_prof);

        
        $__internal_92f5807858597a4039aae4c435a4af000dab04c44631f1942bcc9345b9cc9ea0->leave($__internal_92f5807858597a4039aae4c435a4af000dab04c44631f1942bcc9345b9cc9ea0_prof);

    }

    // line 11
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_144673f59025b42ec9f14a8ad2f4a6a6da2301959f4215c148b4b23b1997d31f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_144673f59025b42ec9f14a8ad2f4a6a6da2301959f4215c148b4b23b1997d31f->enter($__internal_144673f59025b42ec9f14a8ad2f4a6a6da2301959f4215c148b4b23b1997d31f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_3da7fd55c3465204e350a4f9e2e0b5998582f34811a2b8f1878c70b9c9036811 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3da7fd55c3465204e350a4f9e2e0b5998582f34811a2b8f1878c70b9c9036811->enter($__internal_3da7fd55c3465204e350a4f9e2e0b5998582f34811a2b8f1878c70b9c9036811_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 12
        echo "      
    ";
        // line 13
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 14
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
        
        $__internal_3da7fd55c3465204e350a4f9e2e0b5998582f34811a2b8f1878c70b9c9036811->leave($__internal_3da7fd55c3465204e350a4f9e2e0b5998582f34811a2b8f1878c70b9c9036811_prof);

        
        $__internal_144673f59025b42ec9f14a8ad2f4a6a6da2301959f4215c148b4b23b1997d31f->leave($__internal_144673f59025b42ec9f14a8ad2f4a6a6da2301959f4215c148b4b23b1997d31f_prof);

    }

    // line 20
    public function block_body($context, array $blocks = array())
    {
        $__internal_a9fc342489201d2d4763be1eb4b3946cbc5d90b3ba91bd239c7be0b93e49423c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9fc342489201d2d4763be1eb4b3946cbc5d90b3ba91bd239c7be0b93e49423c->enter($__internal_a9fc342489201d2d4763be1eb4b3946cbc5d90b3ba91bd239c7be0b93e49423c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1b8eba067a66e5f37239c1dff1c66e07f742f102059fb7acea3d6154b24bed94 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b8eba067a66e5f37239c1dff1c66e07f742f102059fb7acea3d6154b24bed94->enter($__internal_1b8eba067a66e5f37239c1dff1c66e07f742f102059fb7acea3d6154b24bed94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 21
        echo "<div class=\"right_col\" role=\"main\" style=\"min-height: 478px;\">

        <div class=\"row\">
              <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2>Ajouter Application  <small>Renseigner les informations nécessaires</small></h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">
                    <br>
                   
\t\t  ";
        // line 39
        if (array_key_exists("success", $context)) {
            // line 40
            echo "                        ";
            if (((isset($context["success"]) ? $context["success"] : $this->getContext($context, "success")) != null)) {
                // line 41
                echo "                    <div class=\"alert alert-success alert-dismissible fade in\" role=\"alert\">
                        ";
                // line 42
                echo twig_escape_filter($this->env, (isset($context["success"]) ? $context["success"] : $this->getContext($context, "success")), "html", null, true);
                echo "                
                    </div>
\t\t\t";
            }
            // line 45
            echo "\t\t";
        }
        // line 46
        echo "\t\t
                ";
        // line 47
        if (array_key_exists("errors", $context)) {
            // line 48
            echo "                        ";
            if (((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")) != null)) {
                // line 49
                echo "                    <div class=\"alert alert-danger alert-dismissible fade in\" role=\"alert\">

                                        <ul> 
                                        ";
                // line 52
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
                foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                    // line 53
                    echo "                                                <li> ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                    echo " </li>
                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 55
                echo "                                        </ul>

                                    </div>
                ";
            }
            // line 59
            echo "\t\t";
        }
        // line 60
        echo "\t\t
                        ";
        // line 61
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("id" => "demo-form2", "data-parsley-validate" => "", "class" => "form-horizontal form-label-left", "novalidate" => "")));
        echo "

                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\">Titre <span class=\"required\">*</span>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          
                          ";
        // line 68
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nomApplication", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12")));
        echo "
                        </div>
                      </div>
\t\t      
\t\t        <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\">Titre <span class=\"required\">*</span>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          
                          ";
        // line 77
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "metiers", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12")));
        echo "
                        </div>
                      </div>
                      
                      <div class=\"form-group\">
                        <label for=\"middle-name\" class=\"control-label col-md-3 col-sm-3 col-xs-12\">Activation <span class=\"required\">*</span> </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          ";
        // line 84
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "activationStatus", array()), 'widget', array("attr" => array("class" => "iCheck-helper")));
        echo " 
                        </div>
                      </div>
                      
                  
                
                                
                      
                      <div class=\"ln_solid\"></div>
                      <div class=\"form-group\">
                        <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                          <input type=\"submit\" class=\"btn btn-success\">
                        </div>
                      </div>
                        ";
        // line 98
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
                    
                  </div>
                </div>
              </div>
            </div>
\t
\t<div class=\"row\">
\t\t<div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2> Applications </h2>


<table class=\"table\">
<tr>
<td> NAME </td>
<td> SITUATION </td>
<td> ACTION </td>
</tr>

";
        // line 119
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["applications"]) ? $context["applications"] : $this->getContext($context, "applications")));
        foreach ($context['_seq'] as $context["_key"] => $context["application"]) {
            // line 120
            echo "\t<tr>
\t\t<td> ";
            // line 121
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["application"], 0, array(), "array"), "nomApplication", array()), "html", null, true);
            echo " </td>
\t\t<td> ";
            // line 122
            if ($this->getAttribute($this->getAttribute($context["application"], 0, array(), "array"), "activationStatus", array())) {
                echo " Active ";
            } else {
                echo " Inactive ";
            }
            echo " </td>
\t  <td> <a class=\"btn btn-round btn-primary\" href=\"";
            // line 123
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("application_deactivate", array("id" => $this->getAttribute($this->getAttribute($context["application"], 0, array(), "array"), "id", array()))), "html", null, true);
            echo "\"> Activer/Desactiver </a>
\t\t\t<a class=\"btn btn-round btn-info\" href=\"";
            // line 124
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_custom_add_application", array("idApplication" => $this->getAttribute($this->getAttribute($context["application"], 0, array(), "array"), "id", array()))), "html", null, true);
            echo "\"> Déclarer incident </a>
\t\t\t<a class=\"btn btn-round btn-success\" href=\"";
            // line 125
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("application_details", array("id" => $this->getAttribute($this->getAttribute($context["application"], 0, array(), "array"), "id", array()))), "html", null, true);
            echo "\"> Détails </a>
\t\t\t<a class=\"btn btn-round btn-warning\" href=\"";
            // line 126
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("application_update", array("idApplication" => $this->getAttribute($this->getAttribute($context["application"], 0, array(), "array"), "id", array()))), "html", null, true);
            echo "\"> Mettre à jour </a>
\t\t\t
\t  </td>
\t  
\t  
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['application'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 132
        echo "
</table>
\t\t  </div>
\t\t</div>
\t     </div>
\t</div>

</div>






";
        
        $__internal_1b8eba067a66e5f37239c1dff1c66e07f742f102059fb7acea3d6154b24bed94->leave($__internal_1b8eba067a66e5f37239c1dff1c66e07f742f102059fb7acea3d6154b24bed94_prof);

        
        $__internal_a9fc342489201d2d4763be1eb4b3946cbc5d90b3ba91bd239c7be0b93e49423c->leave($__internal_a9fc342489201d2d4763be1eb4b3946cbc5d90b3ba91bd239c7be0b93e49423c_prof);

    }

    // line 148
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_935f3efc17d531c833de1b5d98ecef87c5cff08393f08e392ddbddcf62b332e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_935f3efc17d531c833de1b5d98ecef87c5cff08393f08e392ddbddcf62b332e6->enter($__internal_935f3efc17d531c833de1b5d98ecef87c5cff08393f08e392ddbddcf62b332e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_03fcc18528241c7d98a04d3311b067109ae8b744a307bc105da7aa46e415c7c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_03fcc18528241c7d98a04d3311b067109ae8b744a307bc105da7aa46e415c7c5->enter($__internal_03fcc18528241c7d98a04d3311b067109ae8b744a307bc105da7aa46e415c7c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 149
        echo "\t    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 150
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
        }
        unset($context["asset_url"]);
        // line 152
        echo "      ";
        
        $__internal_03fcc18528241c7d98a04d3311b067109ae8b744a307bc105da7aa46e415c7c5->leave($__internal_03fcc18528241c7d98a04d3311b067109ae8b744a307bc105da7aa46e415c7c5_prof);

        
        $__internal_935f3efc17d531c833de1b5d98ecef87c5cff08393f08e392ddbddcf62b332e6->leave($__internal_935f3efc17d531c833de1b5d98ecef87c5cff08393f08e392ddbddcf62b332e6_prof);

    }

    public function getTemplateName()
    {
        return "/application/applicationAdd.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  442 => 152,  386 => 150,  381 => 149,  372 => 148,  348 => 132,  336 => 126,  332 => 125,  328 => 124,  324 => 123,  316 => 122,  312 => 121,  309 => 120,  305 => 119,  281 => 98,  264 => 84,  254 => 77,  242 => 68,  232 => 61,  229 => 60,  226 => 59,  220 => 55,  211 => 53,  207 => 52,  202 => 49,  199 => 48,  197 => 47,  194 => 46,  191 => 45,  185 => 42,  182 => 41,  179 => 40,  177 => 39,  157 => 21,  148 => 20,  90 => 14,  86 => 13,  83 => 12,  74 => 11,  63 => 17,  61 => 11,  52 => 4,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'dashboard.html.twig' %}

{% block header %}
<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
  
  {% block stylesheets %}
      
    {% stylesheets '@AppBundle/Resources/public/css/*' filter='cssrewrite' %}
    <link rel=\"stylesheet\" href=\"{{ asset_url }}\" />
    {% endstylesheets %}
{% endblock %}

{% endblock %}

{% block body %}
<div class=\"right_col\" role=\"main\" style=\"min-height: 478px;\">

        <div class=\"row\">
              <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2>Ajouter Application  <small>Renseigner les informations nécessaires</small></h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">
                    <br>
                   
\t\t  {% if success is defined %}
                        {% if success != null %}
                    <div class=\"alert alert-success alert-dismissible fade in\" role=\"alert\">
                        {{  success }}                
                    </div>
\t\t\t{%endif %}
\t\t{%endif %}
\t\t
                {% if errors is defined %}
                        {% if errors != null %}
                    <div class=\"alert alert-danger alert-dismissible fade in\" role=\"alert\">

                                        <ul> 
                                        {% for error in errors %}
                                                <li> {{ error.message }} </li>
                                        {% endfor%}
                                        </ul>

                                    </div>
                {%endif %}
\t\t{%endif %}
\t\t
                        {{ form_start(form, { 'attr' : { 'id' : 'demo-form2', 'data-parsley-validate': '', 'class' : 'form-horizontal form-label-left', 'novalidate': ''  } }) }}

                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\">Titre <span class=\"required\">*</span>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          
                          {{ form_widget(form.nomApplication, { 'attr' : { 'class' : 'form-control col-md-7 col-xs-12'} } )}}
                        </div>
                      </div>
\t\t      
\t\t        <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\">Titre <span class=\"required\">*</span>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          
                          {{ form_widget(form.metiers, { 'attr' : { 'class' : 'form-control col-md-7 col-xs-12'} } )}}
                        </div>
                      </div>
                      
                      <div class=\"form-group\">
                        <label for=\"middle-name\" class=\"control-label col-md-3 col-sm-3 col-xs-12\">Activation <span class=\"required\">*</span> </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          {{ form_widget(form.activationStatus, { 'attr' : { 'class' : 'iCheck-helper'} } )}} 
                        </div>
                      </div>
                      
                  
                
                                
                      
                      <div class=\"ln_solid\"></div>
                      <div class=\"form-group\">
                        <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                          <input type=\"submit\" class=\"btn btn-success\">
                        </div>
                      </div>
                        {{ form_end(form) }}
                    
                  </div>
                </div>
              </div>
            </div>
\t
\t<div class=\"row\">
\t\t<div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2> Applications </h2>


<table class=\"table\">
<tr>
<td> NAME </td>
<td> SITUATION </td>
<td> ACTION </td>
</tr>

{% for application in applications %}
\t<tr>
\t\t<td> {{ application[0].nomApplication }} </td>
\t\t<td> {% if application[0].activationStatus %} Active {% else %} Inactive {% endif %} </td>
\t  <td> <a class=\"btn btn-round btn-primary\" href=\"{{path('application_deactivate',{'id' : application[0].id })}}\"> Activer/Desactiver </a>
\t\t\t<a class=\"btn btn-round btn-info\" href=\"{{path('incident_custom_add_application',{'idApplication' : application[0].id })}}\"> Déclarer incident </a>
\t\t\t<a class=\"btn btn-round btn-success\" href=\"{{path('application_details',{'id' : application[0].id })}}\"> Détails </a>
\t\t\t<a class=\"btn btn-round btn-warning\" href=\"{{path('application_update',{'idApplication' : application[0].id })}}\"> Mettre à jour </a>
\t\t\t
\t  </td>
\t  
\t  
{% endfor %}

</table>
\t\t  </div>
\t\t</div>
\t     </div>
\t</div>

</div>






{% endblock %}

    {% block javascripts %}
\t    {% javascripts '@AppBundle/Resources/public/js/*' %}
\t        <script src=\"{{ asset_url }}\"></script>
\t    {% endjavascripts %}
      {% endblock %}", "/application/applicationAdd.html.twig", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\application\\applicationAdd.html.twig");
    }
}
