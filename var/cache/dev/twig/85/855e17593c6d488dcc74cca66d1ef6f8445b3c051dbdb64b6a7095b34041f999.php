<?php

/* @FOSUser/Security/login_content.html.twig */
class __TwigTemplate_df95c4543738cd773eb3fb15d50c5fc4b66fc6cbc14449e9de84ace01324c1fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("loginTemplate.html.twig", "@FOSUser/Security/login_content.html.twig", 2);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "loginTemplate.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6ad5ea8a7162a19c007a099722ad1d9df36273855451fc8e3b182cc719fdfb1f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6ad5ea8a7162a19c007a099722ad1d9df36273855451fc8e3b182cc719fdfb1f->enter($__internal_6ad5ea8a7162a19c007a099722ad1d9df36273855451fc8e3b182cc719fdfb1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        $__internal_486fae9a9d7f93cdca623d70131ee8a504ba031bdd82a59c9621fde7c3736529 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_486fae9a9d7f93cdca623d70131ee8a504ba031bdd82a59c9621fde7c3736529->enter($__internal_486fae9a9d7f93cdca623d70131ee8a504ba031bdd82a59c9621fde7c3736529_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6ad5ea8a7162a19c007a099722ad1d9df36273855451fc8e3b182cc719fdfb1f->leave($__internal_6ad5ea8a7162a19c007a099722ad1d9df36273855451fc8e3b182cc719fdfb1f_prof);

        
        $__internal_486fae9a9d7f93cdca623d70131ee8a504ba031bdd82a59c9621fde7c3736529->leave($__internal_486fae9a9d7f93cdca623d70131ee8a504ba031bdd82a59c9621fde7c3736529_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_188ac2a6e1feff121d7034b83ab566051401580fd96bcbf969da9413356b87e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_188ac2a6e1feff121d7034b83ab566051401580fd96bcbf969da9413356b87e4->enter($__internal_188ac2a6e1feff121d7034b83ab566051401580fd96bcbf969da9413356b87e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_f23a1dfff0621820d67ed9a5e8566741b0a50fc8a7adc9be0e37e6f1bf7fffd6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f23a1dfff0621820d67ed9a5e8566741b0a50fc8a7adc9be0e37e6f1bf7fffd6->enter($__internal_f23a1dfff0621820d67ed9a5e8566741b0a50fc8a7adc9be0e37e6f1bf7fffd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        $this->displayParentBlock("header", $context, $blocks);
        echo "

<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

";
        
        $__internal_f23a1dfff0621820d67ed9a5e8566741b0a50fc8a7adc9be0e37e6f1bf7fffd6->leave($__internal_f23a1dfff0621820d67ed9a5e8566741b0a50fc8a7adc9be0e37e6f1bf7fffd6_prof);

        
        $__internal_188ac2a6e1feff121d7034b83ab566051401580fd96bcbf969da9413356b87e4->leave($__internal_188ac2a6e1feff121d7034b83ab566051401580fd96bcbf969da9413356b87e4_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_4a0c1aa501f92f97723b4d94799c7e5102946e32145275459f5a57a14ff8dbb3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a0c1aa501f92f97723b4d94799c7e5102946e32145275459f5a57a14ff8dbb3->enter($__internal_4a0c1aa501f92f97723b4d94799c7e5102946e32145275459f5a57a14ff8dbb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_5f8143e1f0d4722ef4121c59c0c40b4a0df6c592b7d6b33c41162827290ad8b7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5f8143e1f0d4722ef4121c59c0c40b4a0df6c592b7d6b33c41162827290ad8b7->enter($__internal_5f8143e1f0d4722ef4121c59c0c40b4a0df6c592b7d6b33c41162827290ad8b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 15
        $this->displayParentBlock("body", $context, $blocks);
        echo "



";
        // line 19
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 20
            echo "    <div class=\"alert alert-danger alert-dismissible fade in\"> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 22
        echo "<h1>Se connecter </h1>
<form action=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
    ";
        // line 24
        if ((isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token"))) {
            // line 25
            echo "        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
            echo "\" />
    ";
        }
        // line 27
        echo "

    <div>
      <input type=\"text\" id=\"username\" name=\"_username\"  class=\"form-control\" placeholder=\"pseudo\" required=\"required\" />
    </div>
    <div>
    <input type=\"password\" id=\"password\" name=\"_password\" placeholder=\"mot de passe\" class=\"form-control\" required=\"required\" />
    </div>
    <div>
    <input type=\"submit\" id=\"_submit\" name=\"_submit\" class=\"btn btn-default submit\" value=\"Se connecter\" />
    </div>

    <div class=\"clearfix\"></div>


      </p>

      <div class=\"clearfix\"></div>
      <br />

      <div>
        <h1> Outil de diffusion Cdiscount © </h1>
        <p>©2016 All Rights Reserved. Cdiscount!</p>
      </div>
    </div>
</form>

";
        // line 54
        $this->displayBlock('javascripts', $context, $blocks);
        // line 59
        echo "
";
        
        $__internal_5f8143e1f0d4722ef4121c59c0c40b4a0df6c592b7d6b33c41162827290ad8b7->leave($__internal_5f8143e1f0d4722ef4121c59c0c40b4a0df6c592b7d6b33c41162827290ad8b7_prof);

        
        $__internal_4a0c1aa501f92f97723b4d94799c7e5102946e32145275459f5a57a14ff8dbb3->leave($__internal_4a0c1aa501f92f97723b4d94799c7e5102946e32145275459f5a57a14ff8dbb3_prof);

    }

    // line 54
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_759587bd30c537108efd35e400c3946c20a321981ee3640bc08fccf074ed980c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_759587bd30c537108efd35e400c3946c20a321981ee3640bc08fccf074ed980c->enter($__internal_759587bd30c537108efd35e400c3946c20a321981ee3640bc08fccf074ed980c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_2f19cadf0d3ec0ba2df1ef13c4ddb7ce36510dae4626e69ef730bfba87814057 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2f19cadf0d3ec0ba2df1ef13c4ddb7ce36510dae4626e69ef730bfba87814057->enter($__internal_2f19cadf0d3ec0ba2df1ef13c4ddb7ce36510dae4626e69ef730bfba87814057_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 55
        echo "    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 56
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
        
        $__internal_2f19cadf0d3ec0ba2df1ef13c4ddb7ce36510dae4626e69ef730bfba87814057->leave($__internal_2f19cadf0d3ec0ba2df1ef13c4ddb7ce36510dae4626e69ef730bfba87814057_prof);

        
        $__internal_759587bd30c537108efd35e400c3946c20a321981ee3640bc08fccf074ed980c->leave($__internal_759587bd30c537108efd35e400c3946c20a321981ee3640bc08fccf074ed980c_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  164 => 56,  159 => 55,  150 => 54,  139 => 59,  137 => 54,  108 => 27,  102 => 25,  100 => 24,  96 => 23,  93 => 22,  87 => 20,  85 => 19,  78 => 15,  69 => 14,  51 => 4,  42 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% extends 'loginTemplate.html.twig' %}
{% block header %}
{{ parent() }}

<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

{% endblock %}


{% block body %}
{{ parent() }}



{% if error %}
    <div class=\"alert alert-danger alert-dismissible fade in\"> {{ error.messageKey|trans(error.messageData, 'security') }}</div>
{% endif %}
<h1>Se connecter </h1>
<form action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\">
    {% if csrf_token %}
        <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
    {% endif %}


    <div>
      <input type=\"text\" id=\"username\" name=\"_username\"  class=\"form-control\" placeholder=\"pseudo\" required=\"required\" />
    </div>
    <div>
    <input type=\"password\" id=\"password\" name=\"_password\" placeholder=\"mot de passe\" class=\"form-control\" required=\"required\" />
    </div>
    <div>
    <input type=\"submit\" id=\"_submit\" name=\"_submit\" class=\"btn btn-default submit\" value=\"Se connecter\" />
    </div>

    <div class=\"clearfix\"></div>


      </p>

      <div class=\"clearfix\"></div>
      <br />

      <div>
        <h1> Outil de diffusion Cdiscount © </h1>
        <p>©2016 All Rights Reserved. Cdiscount!</p>
      </div>
    </div>
</form>

{% block javascripts %}
    {% javascripts '@AppBundle/Resources/public/js/*' %}
        <script src=\"{{ asset_url }}\"></script>
    {% endjavascripts %}
{% endblock %}

{% endblock %}
", "@FOSUser/Security/login_content.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Security\\login_content.html.twig");
    }
}
