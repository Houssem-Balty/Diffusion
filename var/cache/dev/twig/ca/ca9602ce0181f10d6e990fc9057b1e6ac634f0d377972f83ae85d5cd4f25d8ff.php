<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_014a57dbbd3e9db07c25cec8f76e3ce1c55e9e04924f0fe4fc0bfcb4ed900156 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c5f24524378eaf82ea5023d6f7333588016cf9ef5ec44a8735c83c81ccf41926 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c5f24524378eaf82ea5023d6f7333588016cf9ef5ec44a8735c83c81ccf41926->enter($__internal_c5f24524378eaf82ea5023d6f7333588016cf9ef5ec44a8735c83c81ccf41926_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_c5f24524378eaf82ea5023d6f7333588016cf9ef5ec44a8735c83c81ccf41926->leave($__internal_c5f24524378eaf82ea5023d6f7333588016cf9ef5ec44a8735c83c81ccf41926_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/url_widget.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\url_widget.html.php");
    }
}
