<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_2cf1d6f5c615a9d822522176083fd00b0244a59c8962e75f68b74bd4f8b6bd09 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3de3a00ee63d32a0be887812f8d17e8d1d102ac125c08df8c1650f21a07439f0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3de3a00ee63d32a0be887812f8d17e8d1d102ac125c08df8c1650f21a07439f0->enter($__internal_3de3a00ee63d32a0be887812f8d17e8d1d102ac125c08df8c1650f21a07439f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3de3a00ee63d32a0be887812f8d17e8d1d102ac125c08df8c1650f21a07439f0->leave($__internal_3de3a00ee63d32a0be887812f8d17e8d1d102ac125c08df8c1650f21a07439f0_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_22b914de3819731e0c3d79a14c0f4bed201d9045811083080711c442c9cd041d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_22b914de3819731e0c3d79a14c0f4bed201d9045811083080711c442c9cd041d->enter($__internal_22b914de3819731e0c3d79a14c0f4bed201d9045811083080711c442c9cd041d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_22b914de3819731e0c3d79a14c0f4bed201d9045811083080711c442c9cd041d->leave($__internal_22b914de3819731e0c3d79a14c0f4bed201d9045811083080711c442c9cd041d_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Profile:edit.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Profile/edit.html.twig");
    }
}
