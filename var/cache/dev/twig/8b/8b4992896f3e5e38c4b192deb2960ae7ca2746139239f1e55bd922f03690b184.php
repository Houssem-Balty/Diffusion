<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_21d1c714f25cff4f78edd9d0658c1970700eab28032d801fc3e55f1a8927c437 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ac1e8e9d976eecda9326d7e1d53128afab04be3b2b872fba56ffe51206021b8c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ac1e8e9d976eecda9326d7e1d53128afab04be3b2b872fba56ffe51206021b8c->enter($__internal_ac1e8e9d976eecda9326d7e1d53128afab04be3b2b872fba56ffe51206021b8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ac1e8e9d976eecda9326d7e1d53128afab04be3b2b872fba56ffe51206021b8c->leave($__internal_ac1e8e9d976eecda9326d7e1d53128afab04be3b2b872fba56ffe51206021b8c_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_aba2f54d43a8fbe60054e9602a21308be30968f9148fd50f1fec91816baecb17 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aba2f54d43a8fbe60054e9602a21308be30968f9148fd50f1fec91816baecb17->enter($__internal_aba2f54d43a8fbe60054e9602a21308be30968f9148fd50f1fec91816baecb17_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_aba2f54d43a8fbe60054e9602a21308be30968f9148fd50f1fec91816baecb17->leave($__internal_aba2f54d43a8fbe60054e9602a21308be30968f9148fd50f1fec91816baecb17_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:ChangePassword:change_password.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle/Resources/views/ChangePassword/change_password.html.twig");
    }
}
