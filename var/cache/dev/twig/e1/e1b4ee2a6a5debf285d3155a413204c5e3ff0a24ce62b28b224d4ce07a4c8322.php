<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_a56c6ce2cfc1c1b232109752b52dc9f0702cc6f036b636c93f58da41213da3f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_73c12d0ce20a5560e7f9c0b1f63715bf0cdb5bb3b1de7c4bd4fde11ac5f4d764 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_73c12d0ce20a5560e7f9c0b1f63715bf0cdb5bb3b1de7c4bd4fde11ac5f4d764->enter($__internal_73c12d0ce20a5560e7f9c0b1f63715bf0cdb5bb3b1de7c4bd4fde11ac5f4d764_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_73c12d0ce20a5560e7f9c0b1f63715bf0cdb5bb3b1de7c4bd4fde11ac5f4d764->leave($__internal_73c12d0ce20a5560e7f9c0b1f63715bf0cdb5bb3b1de7c4bd4fde11ac5f4d764_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/form_widget_compound.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_widget_compound.html.php");
    }
}
