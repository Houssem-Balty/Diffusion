<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_f08cc7a4fd69a80300c90213069956d51926c51738a68f8021085cf4d52997aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5ce9e1d6a8c58e9bb4b9c5bcfb611764b605225f9fa86303fcc7ab29c30610eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5ce9e1d6a8c58e9bb4b9c5bcfb611764b605225f9fa86303fcc7ab29c30610eb->enter($__internal_5ce9e1d6a8c58e9bb4b9c5bcfb611764b605225f9fa86303fcc7ab29c30610eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_5ce9e1d6a8c58e9bb4b9c5bcfb611764b605225f9fa86303fcc7ab29c30610eb->leave($__internal_5ce9e1d6a8c58e9bb4b9c5bcfb611764b605225f9fa86303fcc7ab29c30610eb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/password_widget.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\password_widget.html.php");
    }
}
