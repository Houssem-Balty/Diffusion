<?php

/* FOSUserBundle:Registration:check_email.html.twig */
class __TwigTemplate_8dd2fc30c725854a416ee0debd87b07f4bcd32c778d4a59bab64ec93b88b66d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cbe04297d8fd327f980efaf25f8f24cee1a347ae22d9c86ba058e24c472b8cf3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cbe04297d8fd327f980efaf25f8f24cee1a347ae22d9c86ba058e24c472b8cf3->enter($__internal_cbe04297d8fd327f980efaf25f8f24cee1a347ae22d9c86ba058e24c472b8cf3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cbe04297d8fd327f980efaf25f8f24cee1a347ae22d9c86ba058e24c472b8cf3->leave($__internal_cbe04297d8fd327f980efaf25f8f24cee1a347ae22d9c86ba058e24c472b8cf3_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f8c7676bb7d660df37fb1c444663fa4245895ffd354e62b488d633d89b430f5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f8c7676bb7d660df37fb1c444663fa4245895ffd354e62b488d633d89b430f5a->enter($__internal_f8c7676bb7d660df37fb1c444663fa4245895ffd354e62b488d633d89b430f5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_f8c7676bb7d660df37fb1c444663fa4245895ffd354e62b488d633d89b430f5a->leave($__internal_f8c7676bb7d660df37fb1c444663fa4245895ffd354e62b488d633d89b430f5a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  34 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Registration:check_email.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Registration/check_email.html.twig");
    }
}
