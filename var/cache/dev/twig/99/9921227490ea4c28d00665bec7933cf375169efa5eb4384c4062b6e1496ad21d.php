<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_a323f78cab0045f8d948603bc492d8c26ed5db75cc989e4f8fee3d5faa13ca06 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4b9beb0a80f1fc8af10068978d0e0b71cd2b5baf05f12da56bb4492fe7ca1183 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4b9beb0a80f1fc8af10068978d0e0b71cd2b5baf05f12da56bb4492fe7ca1183->enter($__internal_4b9beb0a80f1fc8af10068978d0e0b71cd2b5baf05f12da56bb4492fe7ca1183_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : null), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_4b9beb0a80f1fc8af10068978d0e0b71cd2b5baf05f12da56bb4492fe7ca1183->leave($__internal_4b9beb0a80f1fc8af10068978d0e0b71cd2b5baf05f12da56bb4492fe7ca1183_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/money_widget.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\money_widget.html.php");
    }
}
