<?php

/* /pos/posAdd.html.twig */
class __TwigTemplate_1582621c769157fb00f5c1bd94850d9ca91df36b55aad0b2b018688827d4c050 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", "/pos/posAdd.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a1189071aac30ae8040495f7fa3c3bf5ec0001ffc76611ce7e533378a373c023 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a1189071aac30ae8040495f7fa3c3bf5ec0001ffc76611ce7e533378a373c023->enter($__internal_a1189071aac30ae8040495f7fa3c3bf5ec0001ffc76611ce7e533378a373c023_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "/pos/posAdd.html.twig"));

        $__internal_09f5ffa5a49aebc587108f4685fbeac1e9b7bbdac79d4f23578c663d4993ec3f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09f5ffa5a49aebc587108f4685fbeac1e9b7bbdac79d4f23578c663d4993ec3f->enter($__internal_09f5ffa5a49aebc587108f4685fbeac1e9b7bbdac79d4f23578c663d4993ec3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "/pos/posAdd.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a1189071aac30ae8040495f7fa3c3bf5ec0001ffc76611ce7e533378a373c023->leave($__internal_a1189071aac30ae8040495f7fa3c3bf5ec0001ffc76611ce7e533378a373c023_prof);

        
        $__internal_09f5ffa5a49aebc587108f4685fbeac1e9b7bbdac79d4f23578c663d4993ec3f->leave($__internal_09f5ffa5a49aebc587108f4685fbeac1e9b7bbdac79d4f23578c663d4993ec3f_prof);

    }

    // line 2
    public function block_header($context, array $blocks = array())
    {
        $__internal_9ddd3d11a7826298c25e7d3c73f73091de6009d0c42e49f4b3989728354b5733 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ddd3d11a7826298c25e7d3c73f73091de6009d0c42e49f4b3989728354b5733->enter($__internal_9ddd3d11a7826298c25e7d3c73f73091de6009d0c42e49f4b3989728354b5733_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_651706fd229b3b1b75295e217205f59d7f8e444b8899e35dc0b976c87caf2c1c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_651706fd229b3b1b75295e217205f59d7f8e444b8899e35dc0b976c87caf2c1c->enter($__internal_651706fd229b3b1b75295e217205f59d7f8e444b8899e35dc0b976c87caf2c1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 3
        echo "<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>


<script src=\"https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js\"> </script>
<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css\"/>
  ";
        // line 11
        $this->displayBlock('stylesheets', $context, $blocks);
        
        $__internal_651706fd229b3b1b75295e217205f59d7f8e444b8899e35dc0b976c87caf2c1c->leave($__internal_651706fd229b3b1b75295e217205f59d7f8e444b8899e35dc0b976c87caf2c1c_prof);

        
        $__internal_9ddd3d11a7826298c25e7d3c73f73091de6009d0c42e49f4b3989728354b5733->leave($__internal_9ddd3d11a7826298c25e7d3c73f73091de6009d0c42e49f4b3989728354b5733_prof);

    }

    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_0a1ed202cffcf288e6709811c0e736da81d8e56fc888fcdd778b03004ece152f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a1ed202cffcf288e6709811c0e736da81d8e56fc888fcdd778b03004ece152f->enter($__internal_0a1ed202cffcf288e6709811c0e736da81d8e56fc888fcdd778b03004ece152f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_09ab3cce086d8c5633af4330cdc298715453b4f0b54b85f1c7350db3c545c2af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09ab3cce086d8c5633af4330cdc298715453b4f0b54b85f1c7350db3c545c2af->enter($__internal_09ab3cce086d8c5633af4330cdc298715453b4f0b54b85f1c7350db3c545c2af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 12
        echo "
    ";
        // line 13
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 14
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
        // line 16
        echo "
     ";
        
        $__internal_09ab3cce086d8c5633af4330cdc298715453b4f0b54b85f1c7350db3c545c2af->leave($__internal_09ab3cce086d8c5633af4330cdc298715453b4f0b54b85f1c7350db3c545c2af_prof);

        
        $__internal_0a1ed202cffcf288e6709811c0e736da81d8e56fc888fcdd778b03004ece152f->leave($__internal_0a1ed202cffcf288e6709811c0e736da81d8e56fc888fcdd778b03004ece152f_prof);

    }

    // line 20
    public function block_body($context, array $blocks = array())
    {
        $__internal_5ed7cd11463f398a9c76e8c2906426b4c01b6c077b4d776df2f7072d5821fd4a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5ed7cd11463f398a9c76e8c2906426b4c01b6c077b4d776df2f7072d5821fd4a->enter($__internal_5ed7cd11463f398a9c76e8c2906426b4c01b6c077b4d776df2f7072d5821fd4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_f962cd4600611ed12ed307e9c50a12b72c663b4cab68b1210d48f880ced6429e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f962cd4600611ed12ed307e9c50a12b72c663b4cab68b1210d48f880ced6429e->enter($__internal_f962cd4600611ed12ed307e9c50a12b72c663b4cab68b1210d48f880ced6429e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 21
        echo "<div class=\"right_col\" role=\"main\" style=\"min-height: 478px;\">

\t  <div class=\"row\">
              <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2> Vos Points de situations sur l'incident  </h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">
                    <br>

\t\t\t\t\t\t\t\t\t\t<table class=\"table table-bordered table-hover dataTable\">
<tr>
<td> Action Réalisé </td>
<td> Date de l'action  </td>
<td> Prochaine Action </td>
<td> Date de fin prévue</td>
<td> Date prochain POS </td>
<td> Action </td>
</tr>

";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["poss"]) ? $context["poss"] : $this->getContext($context, "poss")));
        foreach ($context['_seq'] as $context["_key"] => $context["pos"]) {
            // line 50
            echo "\t<tr>
\t\t<td> ";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["pos"], 0, array(), "array"), "realizedAction", array()), "html", null, true);
            echo " </td>
\t\t<td> ";
            // line 52
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["pos"], 0, array(), "array"), "datePos", array()), "F jS \\a\\t g:ia", "Europe/Paris"), "html", null, true);
            echo " </td>
        <td> ";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["pos"], 0, array(), "array"), "nextAction", array()), "html", null, true);
            echo " </td>
        <td>
\t\t\t\t\t";
            // line 55
            if (($this->getAttribute($this->getAttribute($context["pos"], 0, array(), "array"), "resolutionDate", array()) != null)) {
                // line 56
                echo "\t\t\t\t\t";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["pos"], 0, array(), "array"), "resolutionDate", array()), "F jS \\a\\t g:ia", "Europe/Paris"), "html", null, true);
                echo "
\t\t\t\t";
            } else {
                // line 58
                echo "\t\t\t\t\t-
\t\t\t\t";
            }
            // line 60
            echo "\t\t\t\t</td>
\t\t<td>
\t\t";
            // line 62
            if (($this->getAttribute($this->getAttribute($context["pos"], 0, array(), "array"), "nextPosDate", array()) != null)) {
                // line 63
                echo "\t\t\t";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["pos"], 0, array(), "array"), "nextPosDate", array()), "F jS \\a\\t g:ia", "Europe/Paris"), "html", null, true);
                echo "
\t\t";
            } else {
                // line 65
                echo "\t\t\t-
\t\t";
            }
            // line 67
            echo "\t\t</td>\t\t
        <td> <a href=\"";
            // line 68
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("pos_delete", array("idIncident" => $this->getAttribute($this->getAttribute($this->getAttribute($context["pos"], 0, array(), "array"), "incident", array()), "id", array()), "idPos" => $this->getAttribute($this->getAttribute($context["pos"], 0, array(), "array"), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-round btn-danger\"> Delete POS </a>
\t\t\t\t<a href=\"";
            // line 69
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("pos_update", array("idIncident" => $this->getAttribute($this->getAttribute($this->getAttribute($context["pos"], 0, array(), "array"), "incident", array()), "id", array()), "idPos" => $this->getAttribute($this->getAttribute($context["pos"], 0, array(), "array"), "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-round btn-info\"> Update POS </a>
    </tr>


";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pos'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "
</table>


    </div>
 </div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>





  <div class=\"row\">
              <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2> Ajouter un point de situation  </h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">
                    <br>

  ";
        // line 103
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("attr" => array("id" => "demo-form2", "data-parsley-validate" => "", "class" => "form-horizontal form-label-left", "novalidate" => "")));
        echo "
  <div class=\"form-group\">
    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\"> Action réalisée  <span class=\"required\">*</span>
\t</label>
    <div class=\"col-md-6 col-sm-6 col-xs-12\">
        ";
        // line 108
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "realizedAction", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12")));
        echo "
    </div>
 </div>

    <div class=\"form-group\">
    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\"> Prochaine Actions
</label>
    <div class=\"col-md-6 col-sm-6 col-xs-12\">
        ";
        // line 116
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nextAction", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12")));
        echo "
    </div>
 </div>

\t\t       <div class=\"form-group\">
                                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Type de POS <span class=\"required\">*</span>
                                </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          Date de prochain POS   ";
        // line 124
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "isResolution", array()), 'widget', array("attr" => array("class" => "js-switch", "data-switchery" => "true", "style" => "display : none", "id" => "tg-button")));
        echo "    Date de Résolution

                        </div>
                        </div>



                <div class=\"form-group\" id=\"DR\">
    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\" > Date de résolution <span class=\"required\">*</span>
</label>
    <div class=\"col-md-6 col-sm-6 col-xs-12\">
        ";
        // line 135
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "resolutionDate", array()), 'widget');
        echo "
    </div>
 </div>

        <div class=\"form-group\" id=\"PP\">
    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\"> Date prochain Pos <span class=\"required\">*</span>
</label>
    <div class=\"col-md-6 col-sm-6 col-xs-12\">
        ";
        // line 143
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nextPosDate", array()), 'widget');
        echo "
    </div>
 </div>

<input type=\"submit\" class=\"btn btn-default\" value=\"Ajouter POS\"></input>
";
        // line 148
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "


                  </div>
                </div>
              </div>
  </div>
</div>



";
        
        $__internal_f962cd4600611ed12ed307e9c50a12b72c663b4cab68b1210d48f880ced6429e->leave($__internal_f962cd4600611ed12ed307e9c50a12b72c663b4cab68b1210d48f880ced6429e_prof);

        
        $__internal_5ed7cd11463f398a9c76e8c2906426b4c01b6c077b4d776df2f7072d5821fd4a->leave($__internal_5ed7cd11463f398a9c76e8c2906426b4c01b6c077b4d776df2f7072d5821fd4a_prof);

    }

    // line 161
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_a78f6aa413f90880cba7856001e73bd5b88c7b9cc39e60439e58d3023f136871 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a78f6aa413f90880cba7856001e73bd5b88c7b9cc39e60439e58d3023f136871->enter($__internal_a78f6aa413f90880cba7856001e73bd5b88c7b9cc39e60439e58d3023f136871_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_065840e504ef75c9c2e7562bcc9bc5f046f6b1988bfefb00367e1dbd02656ee5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_065840e504ef75c9c2e7562bcc9bc5f046f6b1988bfefb00367e1dbd02656ee5->enter($__internal_065840e504ef75c9c2e7562bcc9bc5f046f6b1988bfefb00367e1dbd02656ee5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 162
        echo "\t    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 163
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
        }
        unset($context["asset_url"]);
        // line 165
        echo "      ";
        
        $__internal_065840e504ef75c9c2e7562bcc9bc5f046f6b1988bfefb00367e1dbd02656ee5->leave($__internal_065840e504ef75c9c2e7562bcc9bc5f046f6b1988bfefb00367e1dbd02656ee5_prof);

        
        $__internal_a78f6aa413f90880cba7856001e73bd5b88c7b9cc39e60439e58d3023f136871->leave($__internal_a78f6aa413f90880cba7856001e73bd5b88c7b9cc39e60439e58d3023f136871_prof);

    }

    public function getTemplateName()
    {
        return "/pos/posAdd.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  439 => 165,  383 => 163,  378 => 162,  369 => 161,  347 => 148,  339 => 143,  328 => 135,  314 => 124,  303 => 116,  292 => 108,  284 => 103,  253 => 74,  242 => 69,  238 => 68,  235 => 67,  231 => 65,  225 => 63,  223 => 62,  219 => 60,  215 => 58,  209 => 56,  207 => 55,  202 => 53,  198 => 52,  194 => 51,  191 => 50,  187 => 49,  157 => 21,  148 => 20,  137 => 16,  87 => 14,  83 => 13,  80 => 12,  62 => 11,  52 => 3,  43 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'dashboard.html.twig' %}
{% block header %}
<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>


<script src=\"https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js\"> </script>
<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css\"/>
  {% block stylesheets %}

    {% stylesheets '@AppBundle/Resources/public/css/*' filter='cssrewrite' %}
    <link rel=\"stylesheet\" href=\"{{ asset_url }}\" />
    {% endstylesheets %}

     {% endblock %}
{% endblock %}

{% block body %}
<div class=\"right_col\" role=\"main\" style=\"min-height: 478px;\">

\t  <div class=\"row\">
              <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2> Vos Points de situations sur l'incident  </h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">
                    <br>

\t\t\t\t\t\t\t\t\t\t<table class=\"table table-bordered table-hover dataTable\">
<tr>
<td> Action Réalisé </td>
<td> Date de l'action  </td>
<td> Prochaine Action </td>
<td> Date de fin prévue</td>
<td> Date prochain POS </td>
<td> Action </td>
</tr>

{% for pos in poss %}
\t<tr>
\t\t<td> {{ pos[0].realizedAction }} </td>
\t\t<td> {{ pos[0].datePos | date(\"F jS \\\\a\\\\t g:ia\",\"Europe/Paris\") }} </td>
        <td> {{ pos[0].nextAction }} </td>
        <td>
\t\t\t\t\t{% if  pos[0].resolutionDate!=null %}
\t\t\t\t\t{{ pos[0].resolutionDate | date(\"F jS \\\\a\\\\t g:ia\",\"Europe/Paris\") }}
\t\t\t\t{% else %}
\t\t\t\t\t-
\t\t\t\t{% endif %}
\t\t\t\t</td>
\t\t<td>
\t\t{% if pos[0].nextPosDate !=null %}
\t\t\t{{ pos[0].nextPosDate | date(\"F jS \\\\a\\\\t g:ia\",\"Europe/Paris\") }}
\t\t{% else %}
\t\t\t-
\t\t{% endif %}
\t\t</td>\t\t
        <td> <a href=\"{{path('pos_delete',{'idIncident' : pos[0].incident.id, 'idPos' : pos[0].id })}}\" class=\"btn btn-round btn-danger\"> Delete POS </a>
\t\t\t\t<a href=\"{{path('pos_update',{'idIncident' : pos[0].incident.id, 'idPos' : pos[0].id })}}\" class=\"btn btn-round btn-info\"> Update POS </a>
    </tr>


{% endfor %}

</table>


    </div>
 </div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>





  <div class=\"row\">
              <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2> Ajouter un point de situation  </h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">
                    <br>

  {{ form_start(form, { 'attr' : { 'id' : 'demo-form2', 'data-parsley-validate': '', 'class' : 'form-horizontal form-label-left', 'novalidate': ''  } } ) }}
  <div class=\"form-group\">
    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\"> Action réalisée  <span class=\"required\">*</span>
\t</label>
    <div class=\"col-md-6 col-sm-6 col-xs-12\">
        {{ form_widget(form.realizedAction, { 'attr' : { 'class' : 'form-control col-md-7 col-xs-12'} } )}}
    </div>
 </div>

    <div class=\"form-group\">
    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\"> Prochaine Actions
</label>
    <div class=\"col-md-6 col-sm-6 col-xs-12\">
        {{ form_widget(form.nextAction, { 'attr' : { 'class' : 'form-control col-md-7 col-xs-12'} } )}}
    </div>
 </div>

\t\t       <div class=\"form-group\">
                                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Type de POS <span class=\"required\">*</span>
                                </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          Date de prochain POS   {{ form_widget(form.isResolution, { 'attr' : { 'class' : 'js-switch','data-switchery' : 'true', 'style' : 'display : none', 'id':'tg-button'} } )}}    Date de Résolution

                        </div>
                        </div>



                <div class=\"form-group\" id=\"DR\">
    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\" > Date de résolution <span class=\"required\">*</span>
</label>
    <div class=\"col-md-6 col-sm-6 col-xs-12\">
        {{ form_widget(form.resolutionDate)}}
    </div>
 </div>

        <div class=\"form-group\" id=\"PP\">
    <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\"> Date prochain Pos <span class=\"required\">*</span>
</label>
    <div class=\"col-md-6 col-sm-6 col-xs-12\">
        {{ form_widget(form.nextPosDate)}}
    </div>
 </div>

<input type=\"submit\" class=\"btn btn-default\" value=\"Ajouter POS\"></input>
{{ form_end(form) }}


                  </div>
                </div>
              </div>
  </div>
</div>



{% endblock %}

 {% block javascripts %}
\t    {% javascripts '@AppBundle/Resources/public/js/*' %}
\t        <script src=\"{{ asset_url }}\"></script>
\t    {% endjavascripts %}
      {% endblock %}
", "/pos/posAdd.html.twig", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\pos\\posAdd.html.twig");
    }
}
