<?php

/* :impactMetier:impactMetierList.html.twig */
class __TwigTemplate_97847b81878081a706483bfc4506a98ce32eafc724c97db23864107adb1e38f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("loginTemplate.html.twig", ":impactMetier:impactMetierList.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "loginTemplate.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e38a24d19ab025c3a6862be25458d166d8755a856798ae23a41404dea12cadea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e38a24d19ab025c3a6862be25458d166d8755a856798ae23a41404dea12cadea->enter($__internal_e38a24d19ab025c3a6862be25458d166d8755a856798ae23a41404dea12cadea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":impactMetier:impactMetierList.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e38a24d19ab025c3a6862be25458d166d8755a856798ae23a41404dea12cadea->leave($__internal_e38a24d19ab025c3a6862be25458d166d8755a856798ae23a41404dea12cadea_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_96ab22546cf15777356f32ccd07b92f31ad968187e6e638370327335a834e0f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96ab22546cf15777356f32ccd07b92f31ad968187e6e638370327335a834e0f4->enter($__internal_96ab22546cf15777356f32ccd07b92f31ad968187e6e638370327335a834e0f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "
";
        
        $__internal_96ab22546cf15777356f32ccd07b92f31ad968187e6e638370327335a834e0f4->leave($__internal_96ab22546cf15777356f32ccd07b92f31ad968187e6e638370327335a834e0f4_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_c5bfe983aa1e63008beabd77ca924611970c5089fb729ef5864d8c3fbf25bd87 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c5bfe983aa1e63008beabd77ca924611970c5089fb729ef5864d8c3fbf25bd87->enter($__internal_c5bfe983aa1e63008beabd77ca924611970c5089fb729ef5864d8c3fbf25bd87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "
\t\t<h1> Les impacts </h1>

    <table class=\"table\">
      <tr>
        <td> # </td>
        <td> NOM IMPACT </td>
        <td> Configuration </td>
        <td> ACTION </td>
      </tr>

    ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lesImpactMetier"]) ? $context["lesImpactMetier"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["impactmetier"]) {
            // line 20
            echo "     <tr>
        <td> ";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["impactmetier"], 0, array(), "array"), "Id", array()), "html", null, true);
            echo " </td>
        <td>  ";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["impactmetier"], 0, array(), "array"), "nomImpactMetier", array()), "html", null, true);
            echo " </td>
        <td>
          <ul>
          ";
            // line 25
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["impactmetier"], 0, array(), "array"), "Configuration", array()));
            foreach ($context['_seq'] as $context["metier"] => $context["impact"]) {
                // line 26
                echo "          <li> ";
                echo twig_escape_filter($this->env, $context["metier"], "html", null, true);
                echo " : ";
                echo twig_escape_filter($this->env, $context["impact"], "html", null, true);
                echo " </li>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['metier'], $context['impact'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "          </ul>
          </td>
        <td> <a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_add_custom", array("idConfig" => $this->getAttribute($this->getAttribute($context["impactmetier"], 0, array(), "array"), "Id", array()))), "html", null, true);
            echo "\"> Utiliser la Configuration </a> </td>

      </tr>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['impactmetier'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "

\t";
        // line 36
        $this->displayBlock('javascripts', $context, $blocks);
        // line 41
        echo "
";
        
        $__internal_c5bfe983aa1e63008beabd77ca924611970c5089fb729ef5864d8c3fbf25bd87->leave($__internal_c5bfe983aa1e63008beabd77ca924611970c5089fb729ef5864d8c3fbf25bd87_prof);

    }

    // line 36
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_d0de79ab811440907550969141cadaeef261997190c1177dd8c80517ea15090f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d0de79ab811440907550969141cadaeef261997190c1177dd8c80517ea15090f->enter($__internal_d0de79ab811440907550969141cadaeef261997190c1177dd8c80517ea15090f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 37
        echo "\t    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 38
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
        }
        unset($context["asset_url"]);
        // line 40
        echo "\t";
        
        $__internal_d0de79ab811440907550969141cadaeef261997190c1177dd8c80517ea15090f->leave($__internal_d0de79ab811440907550969141cadaeef261997190c1177dd8c80517ea15090f_prof);

    }

    public function getTemplateName()
    {
        return ":impactMetier:impactMetierList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  196 => 40,  140 => 38,  135 => 37,  129 => 36,  121 => 41,  119 => 36,  115 => 34,  105 => 30,  101 => 28,  90 => 26,  86 => 25,  80 => 22,  76 => 21,  73 => 20,  69 => 19,  56 => 8,  50 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":impactMetier:impactMetierList.html.twig", "C:\\xampp2\\htdocs\\symfony\\app/Resources\\views/impactMetier/impactMetierList.html.twig");
    }
}
