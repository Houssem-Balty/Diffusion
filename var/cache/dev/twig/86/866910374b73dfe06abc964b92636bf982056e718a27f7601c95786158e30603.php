<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_2bc4b79f290b1cfdab0e9a5e3feaa97dfdcc14d33b61d5630c4b0c1949af8f10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b249d7cdf583b30be7924815af6ada63a4e424adb5f58f6b97316860bcb4e57 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0b249d7cdf583b30be7924815af6ada63a4e424adb5f58f6b97316860bcb4e57->enter($__internal_0b249d7cdf583b30be7924815af6ada63a4e424adb5f58f6b97316860bcb4e57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0b249d7cdf583b30be7924815af6ada63a4e424adb5f58f6b97316860bcb4e57->leave($__internal_0b249d7cdf583b30be7924815af6ada63a4e424adb5f58f6b97316860bcb4e57_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_cb129c3fb0aec8a322b4e9f6faf983b6d59b17dd7e8b104a8e82bac66bd1dfa7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cb129c3fb0aec8a322b4e9f6faf983b6d59b17dd7e8b104a8e82bac66bd1dfa7->enter($__internal_cb129c3fb0aec8a322b4e9f6faf983b6d59b17dd7e8b104a8e82bac66bd1dfa7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_cb129c3fb0aec8a322b4e9f6faf983b6d59b17dd7e8b104a8e82bac66bd1dfa7->leave($__internal_cb129c3fb0aec8a322b4e9f6faf983b6d59b17dd7e8b104a8e82bac66bd1dfa7_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Resetting:reset.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Resetting/reset.html.twig");
    }
}
