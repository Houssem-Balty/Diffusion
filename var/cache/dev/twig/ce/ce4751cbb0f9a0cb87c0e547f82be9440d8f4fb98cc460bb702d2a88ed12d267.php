<?php

/* @FOSUser/Profile/show.html.twig */
class __TwigTemplate_09a4ad8255aabd699179c51e217c38fbcbc623890ea3cd003bcf5dac984d4b17 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Profile/show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_88d7f68e31301aed286dccfc6207c82bd2d1ee7e42030eb87d8205f0969c5aa3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_88d7f68e31301aed286dccfc6207c82bd2d1ee7e42030eb87d8205f0969c5aa3->enter($__internal_88d7f68e31301aed286dccfc6207c82bd2d1ee7e42030eb87d8205f0969c5aa3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_88d7f68e31301aed286dccfc6207c82bd2d1ee7e42030eb87d8205f0969c5aa3->leave($__internal_88d7f68e31301aed286dccfc6207c82bd2d1ee7e42030eb87d8205f0969c5aa3_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_993b4aa68a1a6c24c3dd3f7deb99fb6b5228c316fbce51d4f2352fdb88997789 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_993b4aa68a1a6c24c3dd3f7deb99fb6b5228c316fbce51d4f2352fdb88997789->enter($__internal_993b4aa68a1a6c24c3dd3f7deb99fb6b5228c316fbce51d4f2352fdb88997789_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "@FOSUser/Profile/show.html.twig", 4)->display($context);
        
        $__internal_993b4aa68a1a6c24c3dd3f7deb99fb6b5228c316fbce51d4f2352fdb88997789->leave($__internal_993b4aa68a1a6c24c3dd3f7deb99fb6b5228c316fbce51d4f2352fdb88997789_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Profile/show.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Profile\\show.html.twig");
    }
}
