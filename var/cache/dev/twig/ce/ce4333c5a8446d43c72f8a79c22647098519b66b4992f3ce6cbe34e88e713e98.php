<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_14d02614162b726b91f15f33cc23a256ad197a83889eaead72410a00bc9fe325 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_abe4e7d386aaf885f0f235d9441be2bc23afe2ff63a4a4a8c5a49bdf1d92396b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_abe4e7d386aaf885f0f235d9441be2bc23afe2ff63a4a4a8c5a49bdf1d92396b->enter($__internal_abe4e7d386aaf885f0f235d9441be2bc23afe2ff63a4a4a8c5a49bdf1d92396b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_e7761b20199d299b00fb9c9e79d7d622480ca00f98660d8b266354e0b208714d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e7761b20199d299b00fb9c9e79d7d622480ca00f98660d8b266354e0b208714d->enter($__internal_e7761b20199d299b00fb9c9e79d7d622480ca00f98660d8b266354e0b208714d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_abe4e7d386aaf885f0f235d9441be2bc23afe2ff63a4a4a8c5a49bdf1d92396b->leave($__internal_abe4e7d386aaf885f0f235d9441be2bc23afe2ff63a4a4a8c5a49bdf1d92396b_prof);

        
        $__internal_e7761b20199d299b00fb9c9e79d7d622480ca00f98660d8b266354e0b208714d->leave($__internal_e7761b20199d299b00fb9c9e79d7d622480ca00f98660d8b266354e0b208714d_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_70e325a916a5cd46c6a88257dadd34e028a61492dd238ad167a5a36c8c641ebb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_70e325a916a5cd46c6a88257dadd34e028a61492dd238ad167a5a36c8c641ebb->enter($__internal_70e325a916a5cd46c6a88257dadd34e028a61492dd238ad167a5a36c8c641ebb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_2c55ae4b2e7c144b6b46542c543971987f82bd43557662855214d55eeeaaf132 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2c55ae4b2e7c144b6b46542c543971987f82bd43557662855214d55eeeaaf132->enter($__internal_2c55ae4b2e7c144b6b46542c543971987f82bd43557662855214d55eeeaaf132_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_2c55ae4b2e7c144b6b46542c543971987f82bd43557662855214d55eeeaaf132->leave($__internal_2c55ae4b2e7c144b6b46542c543971987f82bd43557662855214d55eeeaaf132_prof);

        
        $__internal_70e325a916a5cd46c6a88257dadd34e028a61492dd238ad167a5a36c8c641ebb->leave($__internal_70e325a916a5cd46c6a88257dadd34e028a61492dd238ad167a5a36c8c641ebb_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_e17753d2405c9c8c36c79ccec6436ac31cdf23e5ef00a8ed2870fc1f19b7462b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e17753d2405c9c8c36c79ccec6436ac31cdf23e5ef00a8ed2870fc1f19b7462b->enter($__internal_e17753d2405c9c8c36c79ccec6436ac31cdf23e5ef00a8ed2870fc1f19b7462b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_e38ec591b42f3d4d4f0d10428d9d799850f0f440f57dcba7cf98754ee34ffd5b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e38ec591b42f3d4d4f0d10428d9d799850f0f440f57dcba7cf98754ee34ffd5b->enter($__internal_e38ec591b42f3d4d4f0d10428d9d799850f0f440f57dcba7cf98754ee34ffd5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_e38ec591b42f3d4d4f0d10428d9d799850f0f440f57dcba7cf98754ee34ffd5b->leave($__internal_e38ec591b42f3d4d4f0d10428d9d799850f0f440f57dcba7cf98754ee34ffd5b_prof);

        
        $__internal_e17753d2405c9c8c36c79ccec6436ac31cdf23e5ef00a8ed2870fc1f19b7462b->leave($__internal_e17753d2405c9c8c36c79ccec6436ac31cdf23e5ef00a8ed2870fc1f19b7462b_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_f1789c95e9ac232ad66da03da76cf44dcc324334a90d473428059c371e2ae3b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f1789c95e9ac232ad66da03da76cf44dcc324334a90d473428059c371e2ae3b0->enter($__internal_f1789c95e9ac232ad66da03da76cf44dcc324334a90d473428059c371e2ae3b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_51ac3c7e201efbcf37ca357fdcd7bf4874936ab0f6e780c040d296f32f9b990c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_51ac3c7e201efbcf37ca357fdcd7bf4874936ab0f6e780c040d296f32f9b990c->enter($__internal_51ac3c7e201efbcf37ca357fdcd7bf4874936ab0f6e780c040d296f32f9b990c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_51ac3c7e201efbcf37ca357fdcd7bf4874936ab0f6e780c040d296f32f9b990c->leave($__internal_51ac3c7e201efbcf37ca357fdcd7bf4874936ab0f6e780c040d296f32f9b990c_prof);

        
        $__internal_f1789c95e9ac232ad66da03da76cf44dcc324334a90d473428059c371e2ae3b0->leave($__internal_f1789c95e9ac232ad66da03da76cf44dcc324334a90d473428059c371e2ae3b0_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
