<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_a9234fae7fde7f521ccf316ba8400ac0a1e4ed28df29c468c9f8ce669cbcb1fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_be41a9950ab96386cfe5d5123ab95b5eeb1d98c18068adda9fee7a3e439019dd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_be41a9950ab96386cfe5d5123ab95b5eeb1d98c18068adda9fee7a3e439019dd->enter($__internal_be41a9950ab96386cfe5d5123ab95b5eeb1d98c18068adda9fee7a3e439019dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_be41a9950ab96386cfe5d5123ab95b5eeb1d98c18068adda9fee7a3e439019dd->leave($__internal_be41a9950ab96386cfe5d5123ab95b5eeb1d98c18068adda9fee7a3e439019dd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/number_widget.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\number_widget.html.php");
    }
}
