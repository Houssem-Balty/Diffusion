<?php

/* @FOSUser/Group/list.html.twig */
class __TwigTemplate_45adf36631450d1625fb8d5e7782e27a7093659f6b9549398b5988acdb2474b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d96f10b729fdc23bc35505d003bfda66f8d351ef0c2fe325c4d4dd473c809f9d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d96f10b729fdc23bc35505d003bfda66f8d351ef0c2fe325c4d4dd473c809f9d->enter($__internal_d96f10b729fdc23bc35505d003bfda66f8d351ef0c2fe325c4d4dd473c809f9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d96f10b729fdc23bc35505d003bfda66f8d351ef0c2fe325c4d4dd473c809f9d->leave($__internal_d96f10b729fdc23bc35505d003bfda66f8d351ef0c2fe325c4d4dd473c809f9d_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a7d3fb89ee9de1bb482f1d12c87ea5a206597a51a30fb8b70f52bfef853028e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a7d3fb89ee9de1bb482f1d12c87ea5a206597a51a30fb8b70f52bfef853028e6->enter($__internal_a7d3fb89ee9de1bb482f1d12c87ea5a206597a51a30fb8b70f52bfef853028e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "@FOSUser/Group/list.html.twig", 4)->display($context);
        
        $__internal_a7d3fb89ee9de1bb482f1d12c87ea5a206597a51a30fb8b70f52bfef853028e6->leave($__internal_a7d3fb89ee9de1bb482f1d12c87ea5a206597a51a30fb8b70f52bfef853028e6_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Group/list.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Group\\list.html.twig");
    }
}
