<?php

/* :impactApplication:impactApplication.html.twig */
class __TwigTemplate_03b50215e665b2e1f91fb2d63470d25d789dfc9734429e01f0e30cdeb99db771 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("loginTemplate.html.twig", ":impactApplication:impactApplication.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "loginTemplate.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ab2aed70b566caefc719e35a88c610c2a377f780dd30d14be255c659d75ff6c7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab2aed70b566caefc719e35a88c610c2a377f780dd30d14be255c659d75ff6c7->enter($__internal_ab2aed70b566caefc719e35a88c610c2a377f780dd30d14be255c659d75ff6c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":impactApplication:impactApplication.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ab2aed70b566caefc719e35a88c610c2a377f780dd30d14be255c659d75ff6c7->leave($__internal_ab2aed70b566caefc719e35a88c610c2a377f780dd30d14be255c659d75ff6c7_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_668a7b5aa7e7395f20936da9984a4ee07618fd6c8eb2716d07b63a377bc0b934 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_668a7b5aa7e7395f20936da9984a4ee07618fd6c8eb2716d07b63a377bc0b934->enter($__internal_668a7b5aa7e7395f20936da9984a4ee07618fd6c8eb2716d07b63a377bc0b934_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "
";
        
        $__internal_668a7b5aa7e7395f20936da9984a4ee07618fd6c8eb2716d07b63a377bc0b934->leave($__internal_668a7b5aa7e7395f20936da9984a4ee07618fd6c8eb2716d07b63a377bc0b934_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_e20fb76202cb223c487075f2d813943802966ebd8318828e37b6a3078c4ac77b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e20fb76202cb223c487075f2d813943802966ebd8318828e37b6a3078c4ac77b->enter($__internal_e20fb76202cb223c487075f2d813943802966ebd8318828e37b6a3078c4ac77b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "
\t\t<h1> Les impacts </h1>

    <table class=\"table\">
      <tr>
        <td> # </td>
        <td> NOM IMPACT </td>
        <td> Configuration </td>
        <td> ACTION </td>
      </tr>

    ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["applications"]) ? $context["applications"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["impactapplication"]) {
            // line 20
            echo "     <tr>
        <td> ";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["impactapplication"], 0, array(), "array"), "Id", array()), "html", null, true);
            echo " </td>
        <td>  ";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["impactapplication"], 0, array(), "array"), "nomImpactApplication", array()), "html", null, true);
            echo " </td>
        <td>
          <ul>
          ";
            // line 25
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["impactapplication"], 0, array(), "array"), "Configuration", array()));
            foreach ($context['_seq'] as $context["application"] => $context["impact"]) {
                // line 26
                echo "          <li> ";
                echo twig_escape_filter($this->env, $context["application"], "html", null, true);
                echo " : ";
                echo twig_escape_filter($this->env, $context["impact"], "html", null, true);
                echo " </li>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['application'], $context['impact'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "          </ul>
          </td>
        <td> <a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_add_custom_appli", array("idConfig" => $this->getAttribute($this->getAttribute($context["impactapplication"], 0, array(), "array"), "Id", array()))), "html", null, true);
            echo "\"> Utiliser la Configuration </a> </td>

      </tr>
      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['impactapplication'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "

\t";
        // line 36
        $this->displayBlock('javascripts', $context, $blocks);
        // line 41
        echo "
";
        
        $__internal_e20fb76202cb223c487075f2d813943802966ebd8318828e37b6a3078c4ac77b->leave($__internal_e20fb76202cb223c487075f2d813943802966ebd8318828e37b6a3078c4ac77b_prof);

    }

    // line 36
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_5a501015051143ecc983d75a51172823397087c37187d001c8d19f51661d6973 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a501015051143ecc983d75a51172823397087c37187d001c8d19f51661d6973->enter($__internal_5a501015051143ecc983d75a51172823397087c37187d001c8d19f51661d6973_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 37
        echo "\t    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 38
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
        }
        unset($context["asset_url"]);
        // line 40
        echo "\t";
        
        $__internal_5a501015051143ecc983d75a51172823397087c37187d001c8d19f51661d6973->leave($__internal_5a501015051143ecc983d75a51172823397087c37187d001c8d19f51661d6973_prof);

    }

    public function getTemplateName()
    {
        return ":impactApplication:impactApplication.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  196 => 40,  140 => 38,  135 => 37,  129 => 36,  121 => 41,  119 => 36,  115 => 34,  105 => 30,  101 => 28,  90 => 26,  86 => 25,  80 => 22,  76 => 21,  73 => 20,  69 => 19,  56 => 8,  50 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":impactApplication:impactApplication.html.twig", "C:\\xampp2\\htdocs\\symfony\\app/Resources\\views/impactApplication/impactApplication.html.twig");
    }
}
