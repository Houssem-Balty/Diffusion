<?php

/* default/history.html.twig */
class __TwigTemplate_dbf0eb1e44644c33d38cbfd020cf07823e0e4b1f187ee15853335d1dea6f61b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", "default/history.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c72a46d4382e9ea229f63aba1e2104a39a47ebb79d22c1206d9d72852c96d898 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c72a46d4382e9ea229f63aba1e2104a39a47ebb79d22c1206d9d72852c96d898->enter($__internal_c72a46d4382e9ea229f63aba1e2104a39a47ebb79d22c1206d9d72852c96d898_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/history.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c72a46d4382e9ea229f63aba1e2104a39a47ebb79d22c1206d9d72852c96d898->leave($__internal_c72a46d4382e9ea229f63aba1e2104a39a47ebb79d22c1206d9d72852c96d898_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_1bc49559e91c6fdf12acc2ba3008a8c6bbae3a527b9acdead5ea568ed20ac3d0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1bc49559e91c6fdf12acc2ba3008a8c6bbae3a527b9acdead5ea568ed20ac3d0->enter($__internal_1bc49559e91c6fdf12acc2ba3008a8c6bbae3a527b9acdead5ea568ed20ac3d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "
<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>


<script src=\"https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js\"></script>
<link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css\"/>
  ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 20
        echo "

";
        
        $__internal_1bc49559e91c6fdf12acc2ba3008a8c6bbae3a527b9acdead5ea568ed20ac3d0->leave($__internal_1bc49559e91c6fdf12acc2ba3008a8c6bbae3a527b9acdead5ea568ed20ac3d0_prof);

    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_7668c982e79664fe3c3eec8d9bd9fb971f73e1cc78583671954f935fe2352a77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7668c982e79664fe3c3eec8d9bd9fb971f73e1cc78583671954f935fe2352a77->enter($__internal_7668c982e79664fe3c3eec8d9bd9fb971f73e1cc78583671954f935fe2352a77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 14
        echo "\t  
\t";
        // line 15
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 16
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
        }
        unset($context["asset_url"]);
        // line 18
        echo "\t  
\t ";
        
        $__internal_7668c982e79664fe3c3eec8d9bd9fb971f73e1cc78583671954f935fe2352a77->leave($__internal_7668c982e79664fe3c3eec8d9bd9fb971f73e1cc78583671954f935fe2352a77_prof);

    }

    // line 24
    public function block_body($context, array $blocks = array())
    {
        $__internal_10f72699925cee159058a33cc238927168ef7e7189d4ab727bba6ac9fec0bace = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_10f72699925cee159058a33cc238927168ef7e7189d4ab727bba6ac9fec0bace->enter($__internal_10f72699925cee159058a33cc238927168ef7e7189d4ab727bba6ac9fec0bace_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 25
        echo "



<div class=\"right_col\" role=\"main\">
<div class=\"col-md-12\">
\t<div class=\"x_panel\">
\t  <div class=\"x_title\">
\t\t<h2> Timeline </h2>
\t\t<ul class=\"nav navbar-right panel_toolbox\">
\t\t  <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
\t\t  </li>
\t\t  <li class=\"dropdown\">
\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>
\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t  <li><a href=\"#\">Settings 1</a>
\t\t\t  </li>
\t\t\t  <li><a href=\"#\">Settings 2</a>
\t\t\t  </li>
\t\t\t</ul>
\t\t  </li>
\t\t  <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
\t\t  </li>
\t\t</ul>
\t\t<div class=\"clearfix\"></div>
\t  </div>
\t  <div class=\"x_content\">
\t\t\t
<ul class=\"timeline\">
\t\t
\t\t\t<li>
\t\t\t\t\t<div class=\"timeline-badge danger\"><i class=\"glyphicon glyphicon-flash\"></i></div>
\t\t\t\t\t<div class=\"timeline-panel\">
\t\t\t\t\t  <div class=\"timeline-heading\">
\t\t\t\t\t\t<h4 class=\"timeline-title\">Déclaration d'incident </h4>
\t\t\t\t\t\t<p><small class=\"text-muted\"><i class=\"glyphicon glyphicon-time\"></i> ";
        // line 60
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["incident"]) ? $context["incident"] : null), "dateCreation", array()), "F jS \\a\\t g:ia", "Europe/Paris"), "html", null, true);
        echo " </small></p>
\t\t\t\t\t  </div>
\t\t\t\t\t  <div class=\"timeline-body\">
\t\t\t\t\t\t  <h2> ";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["incident"]) ? $context["incident"] : null), "titre", array()), "html", null, true);
        echo "  </h2>
\t\t\t\t\t\t<p>  Impact : ";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["incident"]) ? $context["incident"] : null), "impact", array()), "nomImpact", array()), "html", null, true);
        echo "  <br>
\t\t\t\t\t\t\t Declaré par : Houssem Balti
\t\t\t\t\t\t</p>
\t\t\t\t\t\t<hr class=\"half-rule\"/>
\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t <span style=\"color:green\"> <a href=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_update", array("idIncident" => $this->getAttribute((isset($context["incident"]) ? $context["incident"] : null), "Id", array()))), "html", null, true);
        echo "\"> Mettre à jour </a> </span>
\t\t\t\t\t\t</p>
\t\t\t\t\t </div>
\t\t\t\t </div>
\t\t\t</li>
\t\t
\t\t";
        // line 75
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["history"]) ? $context["history"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["action"]) {
            // line 76
            echo "\t\t\t";
            if ($this->getAttribute($context["action"], "realizedAction", array(), "any", true, true)) {
                // line 77
                echo "\t\t\t\t\t        <li>
\t\t\t\t\t\t<div class=\"timeline-badge info\"><i class=\"glyphicon glyphicon-check\"></i></div>
\t\t\t\t\t\t<div class=\"timeline-panel\">
\t\t\t\t\t\t  <div class=\"timeline-heading\">
\t\t\t\t\t\t\t<h4 class=\"timeline-title\"> Point de situation </h4>
\t\t\t\t\t\t\t<p><small class=\"text-muted\"><i class=\"glyphicon glyphicon-time\"></i> ";
                // line 82
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["action"], "dateEnvoi", array()), "F jS \\a\\t g:ia", "Europe/Paris"), "html", null, true);
                echo "  </small></p>
\t\t\t\t\t\t  </div>
\t\t\t\t\t\t  <div class=\"timeline-body\">
\t\t\t\t\t\t\t<p> Action réalisée : ";
                // line 85
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "realizedAction", array()), "html", null, true);
                echo " <br>
\t\t\t\t\t\t\t";
                // line 86
                if ($this->getAttribute($context["action"], "nextAction", array(), "any", true, true)) {
                    // line 87
                    echo "\t\t\t\t\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "nextAction", array()), "html", null, true);
                    echo "
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t";
                }
                // line 90
                echo "\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
                // line 91
                if ($this->getAttribute($context["action"], "resolutionDate", array(), "any", true, true)) {
                    // line 92
                    echo "\t\t\t\t\t\t\t\t Date de résolution : ";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["action"], "resolutionDate", array()), "F jS \\a\\t g:ia", "Europe/Paris"), "html", null, true);
                    echo "
\t\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t\t";
                } else {
                    // line 95
                    echo "\t\t\t\t\t\t\t\t Date de prochain POS : ";
                    echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["action"], "nextPosDate", array()), "F jS \\a\\t g:ia", "Europe/Paris"), "html", null, true);
                    echo " <br>
\t\t\t\t\t\t\t";
                }
                // line 97
                echo "\t\t\t\t\t\t\t\tréalisé par : Houssem Balti
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t<hr class=\"half-rule\"/>
\t\t\t\t\t\t\t
\t\t\t\t\t\t  </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t  </li>
\t\t";
            } else {
                // line 112
                echo "\t\t\t    <li class=\"timeline-inverted\">
          <div class=\"timeline-badge warning\"><i class=\"glyphicon glyphicon-envelope\"></i></div>
          <div class=\"timeline-panel\">
            <div class=\"timeline-heading\">
              <h4 class=\"timeline-title\">Envoi de mail </h4>
\t\t\t  <p><small class=\"text-muted\"><i class=\"glyphicon glyphicon-time\"></i> ";
                // line 117
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["action"], "dateEnvoi", array()), "F jS \\a\\t g:ia", "Europe/Paris"), "html", null, true);
                echo "  </small></p>
            </div>
            <div class=\"timeline-body\">
             <p>
\t\t\t\t";
                // line 121
                $context["mails"] = twig_split_filter($this->env, $this->getAttribute($context["action"], "destEnCopie", array()), ";");
                // line 122
                echo "\t\t\t <h5> Destinataires : </h5> <br>  ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["mails"]) ? $context["mails"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["mail"]) {
                    // line 123
                    echo "\t\t\t\t ";
                    echo twig_escape_filter($this->env, $context["mail"], "html", null, true);
                    echo ",
\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mail'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 125
                echo "\t\t\t\t<br/> 
\t\t\t\t <h5> Destinataires d'impact : </h5>
\t\t\t\t";
                // line 127
                $context["mails"] = twig_split_filter($this->env, $this->getAttribute($context["action"], "destImpact", array()), ";");
                // line 128
                echo "\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["mails"]) ? $context["mails"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["mail"]) {
                    // line 129
                    echo "\t\t\t\t ";
                    echo twig_escape_filter($this->env, $context["mail"], "html", null, true);
                    echo ",
\t\t\t\t ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mail'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 131
                echo "\t\t\t\t
\t\t\t\t<br/>
\t\t\t\t<hr class=\"half-rule\"/>
\t\t\t\t<div class=\"modal fade bs-example-modal-lg\" id=\"";
                // line 134
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "id", array()), "html", null, true);
                echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\">
\t\t\t\t\t\t<div class=\"modal-dialog modal-lg\" role=\"document\">
\t\t\t\t\t\t  <div class=\"modal-content\">
\t\t\t\t\t\t\t";
                // line 137
                echo $this->getAttribute($context["action"], "contenu", array());
                echo "
\t\t\t\t\t\t  </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t  </div>

\t\t\t\t\t
\t\t\t\t
\t\t\t </p>
          <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#";
                // line 145
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "id", array()), "html", null, true);
                echo "\">
\t\t\tAfficher mail 
\t\t</button>
            </div>
        </li>
\t";
            }
            // line 151
            echo "\t

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['action'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 154
        echo "\t\t
\t\t";
        // line 155
        if (($this->getAttribute((isset($context["incident"]) ? $context["incident"] : null), "statut", array()) == "Résolu")) {
            // line 156
            echo "\t\t\t
\t\t<li class=\"timeline-inverted\">
          <div class=\"timeline-badge success\"><i class=\"glyphicon glyphicon-thumbs-up\"></i></div>
          <div class=\"timeline-panel\">
            <div class=\"timeline-heading\">
              <h4 class=\"timeline-title\"> Incident Résolu </h4>
\t\t\t  <p><small class=\"text-muted\"><i class=\"glyphicon glyphicon-time\"></i> ";
            // line 162
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["incident"]) ? $context["incident"] : null), "dateFin", array()), "F jS \\a\\t g:ia", "Europe/Paris"), "html", null, true);
            echo "  </small></p>
            </div>
            <div class=\"timeline-body\">
              <p> L'incident est a présent résolu </p>
            </div>
          </div>
        </li>
\t\t\t
\t\t";
        }
        // line 171
        echo "\t\t
        
 

    </ul>

    


    </div>
</div>
</div>
</div>



<!-- Modal -->


";
        
        $__internal_10f72699925cee159058a33cc238927168ef7e7189d4ab727bba6ac9fec0bace->leave($__internal_10f72699925cee159058a33cc238927168ef7e7189d4ab727bba6ac9fec0bace_prof);

    }

    // line 192
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_1d617029c2a239d871619cbf0caeb862879c5757e66efc12f8df1059f2f91a2c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d617029c2a239d871619cbf0caeb862879c5757e66efc12f8df1059f2f91a2c->enter($__internal_1d617029c2a239d871619cbf0caeb862879c5757e66efc12f8df1059f2f91a2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 193
        echo "\t\t";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 194
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
        }
        unset($context["asset_url"]);
        // line 196
        echo "\t  ";
        
        $__internal_1d617029c2a239d871619cbf0caeb862879c5757e66efc12f8df1059f2f91a2c->leave($__internal_1d617029c2a239d871619cbf0caeb862879c5757e66efc12f8df1059f2f91a2c_prof);

    }

    public function getTemplateName()
    {
        return "default/history.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  481 => 196,  425 => 194,  420 => 193,  414 => 192,  388 => 171,  376 => 162,  368 => 156,  366 => 155,  363 => 154,  355 => 151,  346 => 145,  335 => 137,  329 => 134,  324 => 131,  315 => 129,  310 => 128,  308 => 127,  304 => 125,  295 => 123,  290 => 122,  288 => 121,  281 => 117,  274 => 112,  257 => 97,  251 => 95,  244 => 92,  242 => 91,  239 => 90,  232 => 87,  230 => 86,  226 => 85,  220 => 82,  213 => 77,  210 => 76,  206 => 75,  197 => 69,  189 => 64,  185 => 63,  179 => 60,  142 => 25,  136 => 24,  128 => 18,  78 => 16,  74 => 15,  71 => 14,  65 => 13,  56 => 20,  54 => 13,  43 => 4,  37 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/history.html.twig", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\default\\history.html.twig");
    }
}
