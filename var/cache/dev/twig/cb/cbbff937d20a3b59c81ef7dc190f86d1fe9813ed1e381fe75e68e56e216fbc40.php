<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_a10fba10dac5256d8cb6e834ef3cafbb6f934ddc94c6187198fad43459e6a48c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0f502a19aa880c06eb9731161a707f266c09a32df26861a5c9d3be419028d3a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0f502a19aa880c06eb9731161a707f266c09a32df26861a5c9d3be419028d3a8->enter($__internal_0f502a19aa880c06eb9731161a707f266c09a32df26861a5c9d3be419028d3a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0f502a19aa880c06eb9731161a707f266c09a32df26861a5c9d3be419028d3a8->leave($__internal_0f502a19aa880c06eb9731161a707f266c09a32df26861a5c9d3be419028d3a8_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5b58addb02b2f83194b55447210ea97843d7440c729d8ca7890cecef3fc2b19a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b58addb02b2f83194b55447210ea97843d7440c729d8ca7890cecef3fc2b19a->enter($__internal_5b58addb02b2f83194b55447210ea97843d7440c729d8ca7890cecef3fc2b19a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_5b58addb02b2f83194b55447210ea97843d7440c729d8ca7890cecef3fc2b19a->leave($__internal_5b58addb02b2f83194b55447210ea97843d7440c729d8ca7890cecef3fc2b19a_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Group:edit.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Group/edit.html.twig");
    }
}
