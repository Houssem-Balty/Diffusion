<?php

/* @FOSUser/Group/edit.html.twig */
class __TwigTemplate_1da61531247668211be3dabb967c6366aec4794d1ca3d82c4dd6483a022a65f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ce6464b329e1ad43d5ae1b32aa9f7d64b73c5662a415b5a31101b1a4d8b2aca5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ce6464b329e1ad43d5ae1b32aa9f7d64b73c5662a415b5a31101b1a4d8b2aca5->enter($__internal_ce6464b329e1ad43d5ae1b32aa9f7d64b73c5662a415b5a31101b1a4d8b2aca5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ce6464b329e1ad43d5ae1b32aa9f7d64b73c5662a415b5a31101b1a4d8b2aca5->leave($__internal_ce6464b329e1ad43d5ae1b32aa9f7d64b73c5662a415b5a31101b1a4d8b2aca5_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a9b569ba0ff825b6cfa49fe6d04e094e8051d424a6505094e4d3f9020c8d024f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9b569ba0ff825b6cfa49fe6d04e094e8051d424a6505094e4d3f9020c8d024f->enter($__internal_a9b569ba0ff825b6cfa49fe6d04e094e8051d424a6505094e4d3f9020c8d024f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "@FOSUser/Group/edit.html.twig", 4)->display($context);
        
        $__internal_a9b569ba0ff825b6cfa49fe6d04e094e8051d424a6505094e4d3f9020c8d024f->leave($__internal_a9b569ba0ff825b6cfa49fe6d04e094e8051d424a6505094e4d3f9020c8d024f_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Group/edit.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Group\\edit.html.twig");
    }
}
