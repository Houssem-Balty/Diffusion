<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_edbd9728b4d48eb04ca946a8f3ba0d9f483afbba437d366c9e146d6d685c1372 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d91bd6cab5a1b753a8e78950ccdd5b582b4962d4268b24fcc464607545e96ad5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d91bd6cab5a1b753a8e78950ccdd5b582b4962d4268b24fcc464607545e96ad5->enter($__internal_d91bd6cab5a1b753a8e78950ccdd5b582b4962d4268b24fcc464607545e96ad5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d91bd6cab5a1b753a8e78950ccdd5b582b4962d4268b24fcc464607545e96ad5->leave($__internal_d91bd6cab5a1b753a8e78950ccdd5b582b4962d4268b24fcc464607545e96ad5_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_40d9d526817fd781839b9e05fb225635afc41c6f3833fcdec3f81f708325235c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40d9d526817fd781839b9e05fb225635afc41c6f3833fcdec3f81f708325235c->enter($__internal_40d9d526817fd781839b9e05fb225635afc41c6f3833fcdec3f81f708325235c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_40d9d526817fd781839b9e05fb225635afc41c6f3833fcdec3f81f708325235c->leave($__internal_40d9d526817fd781839b9e05fb225635afc41c6f3833fcdec3f81f708325235c_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Group:show.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Group/show.html.twig");
    }
}
