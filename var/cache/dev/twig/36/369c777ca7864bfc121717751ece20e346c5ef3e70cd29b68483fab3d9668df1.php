<?php

/* default/settings.html.twig */
class __TwigTemplate_1496b6b3e05afca5565e4edca579bfcd0ab69bf0235ab4fcfdb50be7aa4397a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", "default/settings.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8c60e805d19ede918e6180eb33d2812c346e0f5ccc178291ef083080a6812e39 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8c60e805d19ede918e6180eb33d2812c346e0f5ccc178291ef083080a6812e39->enter($__internal_8c60e805d19ede918e6180eb33d2812c346e0f5ccc178291ef083080a6812e39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/settings.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8c60e805d19ede918e6180eb33d2812c346e0f5ccc178291ef083080a6812e39->leave($__internal_8c60e805d19ede918e6180eb33d2812c346e0f5ccc178291ef083080a6812e39_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_6f09dd63a571c21d7a590247d5e8ecbb21564f9cfb39727b72d1a5eb89fb55c9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6f09dd63a571c21d7a590247d5e8ecbb21564f9cfb39727b72d1a5eb89fb55c9->enter($__internal_6f09dd63a571c21d7a590247d5e8ecbb21564f9cfb39727b72d1a5eb89fb55c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "
<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>


  
  ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 19
        echo "

";
        
        $__internal_6f09dd63a571c21d7a590247d5e8ecbb21564f9cfb39727b72d1a5eb89fb55c9->leave($__internal_6f09dd63a571c21d7a590247d5e8ecbb21564f9cfb39727b72d1a5eb89fb55c9_prof);

    }

    // line 12
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_eb8e9f372e3b87f6dc4fcba2a178b52ecb13f539713486be8c587c5c50d70baa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eb8e9f372e3b87f6dc4fcba2a178b52ecb13f539713486be8c587c5c50d70baa->enter($__internal_eb8e9f372e3b87f6dc4fcba2a178b52ecb13f539713486be8c587c5c50d70baa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 13
        echo "\t  
\t";
        // line 14
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 15
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
        }
        unset($context["asset_url"]);
        // line 17
        echo "\t  
\t ";
        
        $__internal_eb8e9f372e3b87f6dc4fcba2a178b52ecb13f539713486be8c587c5c50d70baa->leave($__internal_eb8e9f372e3b87f6dc4fcba2a178b52ecb13f539713486be8c587c5c50d70baa_prof);

    }

    // line 23
    public function block_body($context, array $blocks = array())
    {
        $__internal_983b25457757b402bd11ce27d053bf1c563c093a2dd0679817384d9e71ba9ea2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_983b25457757b402bd11ce27d053bf1c563c093a2dd0679817384d9e71ba9ea2->enter($__internal_983b25457757b402bd11ce27d053bf1c563c093a2dd0679817384d9e71ba9ea2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 24
        echo "
<div class=\"right_col\" role=\"main\">
<div class=\"col-md-12\">
\t<div class=\"x_panel\">
\t  <div class=\"x_title\">
\t\t<h2>Paramètres</h2>
\t\t<ul class=\"nav navbar-right panel_toolbox\">
\t\t  <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
\t\t  </li>
\t\t  <li class=\"dropdown\">
\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>
\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t  <li><a href=\"#\">Settings 1</a>
\t\t\t  </li>
\t\t\t  <li><a href=\"#\">Settings 2</a>
\t\t\t  </li>
\t\t\t</ul>
\t\t  </li>
\t\t  <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
\t\t  </li>
\t\t</ul>
\t\t<div class=\"clearfix\"></div>
\t  </div>
\t  <div class=\"x_content\">
\t\t
\t\t\t<!-- Gestion des utilisateurs -->
\t\t\t
\t\t<div class=\"col-md-3 col-xs-12 widget widget_tally_box\">
                        <div class=\"x_panel ui-ribbon-container fixed_height_390\">
                          <div class=\"x_title\">
                            <h2> Utilisateurs </h2>
                            <div class=\"clearfix\"></div>
                          </div>
                          <div class=\"x_content\">

                            <div style=\"text-align: center; margin-bottom: 17px\">
                             <i style=\"font-size:50px;color: #009688\" class=\"fa fa-users\"></i>
                            </div>

                            <h3 class=\"name_title\">Gérer les utilisateurs </h3>
                            <p> Ajouter/Supprimer utilisateur </p>

                            <div class=\"divider\"></div>

                           <ul style=\"list-style: none\">
\t\t\t\t\t\t\t<li> <a href=\"";
        // line 69
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user_add");
        echo "\" class=\"btn btn-success btn-sm\"> Gérer les utilisateurs </a>  </li>
\t\t\t\t\t\t\t
\t\t\t\t\t\t   </ul>

                          </div>
                        </div>
                      </div>
\t\t
\t\t<!-- Gestion des métiers -->
\t\t
\t\t<div class=\"col-md-3 col-xs-12 widget widget_tally_box\">
                        <div class=\"x_panel ui-ribbon-container fixed_height_390\">
                          <div class=\"x_title\">
                            <h2> Metiers </h2>
                            <div class=\"clearfix\"></div>
                          </div>
                          <div class=\"x_content\">

                            <div style=\"text-align: center; margin-bottom: 17px\">
                             <i  style=\"font-size:50px;color: #009688\" class=\"fa fa-area-chart\"></i>
                            </div>

                            <h3 class=\"name_title\">Gérer les métiers </h3>
                            <p> Ajouter/Supprimer metiers </p>

                            <div class=\"divider\"></div>

                           <ul style=\"list-style: none\">
\t\t\t\t\t\t\t<li> <a href=\"";
        // line 97
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("metier_add");
        echo "\" class=\"btn btn-success btn-sm\"> gérer les métiers </a>  </li>
\t\t\t\t\t\t\t
\t\t\t\t\t\t   </ul>

                          </div>
                        </div>
                      </div>
\t\t
\t\t<!--  Gestion des applications -->
\t\t
\t\t\t<div class=\"col-md-3 col-xs-12 widget widget_tally_box\">
                        <div class=\"x_panel ui-ribbon-container fixed_height_390\">
                          <div class=\"x_title\">
                            <h2> Applications </h2>
                            <div class=\"clearfix\"></div>
                          </div>
                          <div class=\"x_content\">

                            <div style=\"text-align: center; margin-bottom: 17px\">
                             <i  style=\"font-size:50px;color: #009688\" class=\"fa fa-desktop\"></i>
                            </div>

                            <h3 class=\"name_title\">Gérer les applications </h3>
                            <p> Ajouter/Supprimer applications </p>

                            <div class=\"divider\"></div>

                           <ul style=\"list-style: none\">
\t\t\t\t\t\t\t<li> <a href=\"";
        // line 125
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("application_add");
        echo "\" class=\"btn btn-success btn-sm\"> Gérer les applis  </a>  </li>
\t\t\t\t\t\t\t
\t\t\t\t\t\t   </ul>

                          </div>
                        </div>
                      </div>
\t\t\t
\t\t\t<!-- Gestion des clients -->
\t\t\t
\t\t\t\t\t
\t\t\t<div class=\"col-md-3 col-xs-12 widget widget_tally_box\">
                        <div class=\"x_panel ui-ribbon-container fixed_height_390\">
                          <div class=\"x_title\">
                            <h2> Clients </h2>
                            <div class=\"clearfix\"></div>
                          </div>
                          <div class=\"x_content\">

                            <div style=\"text-align: center; margin-bottom: 17px\">
                             <i  style=\"font-size:50px;color: #009688\" class=\"fa fa-child\"></i>
                            </div>

                            <h3 class=\"name_title\">Gérer les clients </h3>
\t\t\t\t\t\t\t<br>
                            <p> Ajouter et modifier clients </p>

                            <div class=\"divider\"></div>

                           <ul style=\"list-style: none\">
\t\t\t\t\t\t\t<li> <a href=\"";
        // line 155
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("client_add");
        echo "\" class=\"btn btn-success btn-sm\"> gérer les clients  </a>  </li>

\t\t\t\t\t\t\t
\t\t\t\t\t\t   </ul>

                          </div>
                        </div>
                      </div>


\t\t\t<div class=\"col-md-3 col-xs-12 widget widget_tally_box\">
                        <div class=\"x_panel ui-ribbon-container fixed_height_390\">
                          <div class=\"x_title\">
                            <h2> Impacts </h2>
                            <div class=\"clearfix\"></div>
                          </div>
                          <div class=\"x_content\">

                            <div style=\"text-align: center; margin-bottom: 17px\">
                             <i  style=\"font-size:50px;color: #009688\" class=\"fa fa-crosshairs\"></i>
                            </div>

                            <h3 class=\"name_title\">Gérer les impacts </h3>
                            <p> Ajouter et modifier impacts </p>

                            <div class=\"divider\"></div>

                           <ul style=\"list-style: none\">
\t\t\t\t\t\t\t<li> <a href=\"";
        // line 183
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("impact_add");
        echo "\" class=\"btn btn-success btn-sm\"> gérer les impacts  </a>  </li>
\t\t\t\t\t\t\t
\t\t\t\t\t\t   </ul>

                          </div>
                        </div>
                      </div>
\t\t\t
\t\t\t
\t\t\t
\t\t\t\t\t\t<div class=\"col-md-3 col-xs-12 widget widget_tally_box\">
                        <div class=\"x_panel ui-ribbon-container fixed_height_390\">
                          <div class=\"x_title\">
                            <h2> Impacts </h2>
                            <div class=\"clearfix\"></div>
                          </div>
                          <div class=\"x_content\">

                            <div style=\"text-align: center; margin-bottom: 17px\">
                             <i  style=\"font-size:50px;color: #009688\" class=\"fa fa-bookmark-o\"></i>
                            </div>

                            <h3 class=\"name_title\">Gérer les categories  </h3>
                            <p> Ajouter et modifier les categories </p>

                            <div class=\"divider\"></div>

                           <ul style=\"list-style: none\">
\t\t\t\t\t\t\t<li> <a href=\"";
        // line 211
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categorie_add");
        echo "\" class=\"btn btn-success btn-sm\"> gérer les categrories  </a>  </li>
\t\t\t\t\t\t\t
\t\t\t\t\t\t   </ul>

                          </div>
                        </div>
                      </div>

\t\t  
\t  </div>
\t  </div>
\t  </div>
\t</div>
</div>
\t





  ";
        
        $__internal_983b25457757b402bd11ce27d053bf1c563c093a2dd0679817384d9e71ba9ea2->leave($__internal_983b25457757b402bd11ce27d053bf1c563c093a2dd0679817384d9e71ba9ea2_prof);

    }

    // line 234
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_33b1d0a10550080837ffeecd26cb09c32c1c51a54c06538f697c27bdd01fe8ed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_33b1d0a10550080837ffeecd26cb09c32c1c51a54c06538f697c27bdd01fe8ed->enter($__internal_33b1d0a10550080837ffeecd26cb09c32c1c51a54c06538f697c27bdd01fe8ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 235
        echo "\t\t";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 236
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
        }
        unset($context["asset_url"]);
        // line 238
        echo "\t  ";
        
        $__internal_33b1d0a10550080837ffeecd26cb09c32c1c51a54c06538f697c27bdd01fe8ed->leave($__internal_33b1d0a10550080837ffeecd26cb09c32c1c51a54c06538f697c27bdd01fe8ed_prof);

    }

    public function getTemplateName()
    {
        return "default/settings.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  440 => 238,  384 => 236,  379 => 235,  373 => 234,  345 => 211,  314 => 183,  283 => 155,  250 => 125,  219 => 97,  188 => 69,  141 => 24,  135 => 23,  127 => 17,  77 => 15,  73 => 14,  70 => 13,  64 => 12,  55 => 19,  53 => 12,  43 => 4,  37 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "default/settings.html.twig", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\default\\settings.html.twig");
    }
}
