<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_3ebe5fd381bd7b6e03c0d8e550154253b24d794cbd5aa28b201a650e27f13329 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8de70f823e050de57c3ea54e9c9680b898001b1fbd083cad189a37caaa5c2923 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8de70f823e050de57c3ea54e9c9680b898001b1fbd083cad189a37caaa5c2923->enter($__internal_8de70f823e050de57c3ea54e9c9680b898001b1fbd083cad189a37caaa5c2923_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_8de70f823e050de57c3ea54e9c9680b898001b1fbd083cad189a37caaa5c2923->leave($__internal_8de70f823e050de57c3ea54e9c9680b898001b1fbd083cad189a37caaa5c2923_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/email_widget.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\email_widget.html.php");
    }
}
