<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_792e7e81b5078f29f25a6c50c5a47399cbd2e462eb5b9e422fac26e4b2a3817c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_09f0f42f6a3504cc6718c97acf6c9cdab82c6d84edf3ec9aba49397540dfc2ad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_09f0f42f6a3504cc6718c97acf6c9cdab82c6d84edf3ec9aba49397540dfc2ad->enter($__internal_09f0f42f6a3504cc6718c97acf6c9cdab82c6d84edf3ec9aba49397540dfc2ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : null), "message" => (isset($context["status_text"]) ? $context["status_text"] : null), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : null), "toarray", array()))));
        echo "
";
        
        $__internal_09f0f42f6a3504cc6718c97acf6c9cdab82c6d84edf3ec9aba49397540dfc2ad->leave($__internal_09f0f42f6a3504cc6718c97acf6c9cdab82c6d84edf3ec9aba49397540dfc2ad_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "TwigBundle:Exception:exception.json.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.json.twig");
    }
}
