<?php

/* template/templateList.html.twig */
class __TwigTemplate_a356220de14751e02a705fdcfde9f3f19c166fd38d9fb9bd73607158ee0940ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", "template/templateList.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_740ae671c0e3faacf9c8ac026973dd525e1a3ce99fa9e6a48f48864653747e95 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_740ae671c0e3faacf9c8ac026973dd525e1a3ce99fa9e6a48f48864653747e95->enter($__internal_740ae671c0e3faacf9c8ac026973dd525e1a3ce99fa9e6a48f48864653747e95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "template/templateList.html.twig"));

        $__internal_8b0cf491f5e3cf58c322f99a649ca06fed4cd33ed418dcc8b389f3c70b71764d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b0cf491f5e3cf58c322f99a649ca06fed4cd33ed418dcc8b389f3c70b71764d->enter($__internal_8b0cf491f5e3cf58c322f99a649ca06fed4cd33ed418dcc8b389f3c70b71764d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "template/templateList.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_740ae671c0e3faacf9c8ac026973dd525e1a3ce99fa9e6a48f48864653747e95->leave($__internal_740ae671c0e3faacf9c8ac026973dd525e1a3ce99fa9e6a48f48864653747e95_prof);

        
        $__internal_8b0cf491f5e3cf58c322f99a649ca06fed4cd33ed418dcc8b389f3c70b71764d->leave($__internal_8b0cf491f5e3cf58c322f99a649ca06fed4cd33ed418dcc8b389f3c70b71764d_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_ec349c575175adc19fc513d3c8bb235568c33ef025deb9c4f71f3b4c769796b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ec349c575175adc19fc513d3c8bb235568c33ef025deb9c4f71f3b4c769796b0->enter($__internal_ec349c575175adc19fc513d3c8bb235568c33ef025deb9c4f71f3b4c769796b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_155de0ca11ed92fabb345d871ef84a346d3362c1a671a7443b6d3692752bedd2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_155de0ca11ed92fabb345d871ef84a346d3362c1a671a7443b6d3692752bedd2->enter($__internal_155de0ca11ed92fabb345d871ef84a346d3362c1a671a7443b6d3692752bedd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

<script src=\"https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js\"></script>
<link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css\"/>

  ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
        
        $__internal_155de0ca11ed92fabb345d871ef84a346d3362c1a671a7443b6d3692752bedd2->leave($__internal_155de0ca11ed92fabb345d871ef84a346d3362c1a671a7443b6d3692752bedd2_prof);

        
        $__internal_ec349c575175adc19fc513d3c8bb235568c33ef025deb9c4f71f3b4c769796b0->leave($__internal_ec349c575175adc19fc513d3c8bb235568c33ef025deb9c4f71f3b4c769796b0_prof);

    }

    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_906e87fa9be636c8d2ae3c6f936ecf703daf668a420b7902249c1fcbed5631b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_906e87fa9be636c8d2ae3c6f936ecf703daf668a420b7902249c1fcbed5631b6->enter($__internal_906e87fa9be636c8d2ae3c6f936ecf703daf668a420b7902249c1fcbed5631b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_7c11bfdafd6b16c73e87854116de415291ded92c46c0fba6f5166dc2076bbf84 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7c11bfdafd6b16c73e87854116de415291ded92c46c0fba6f5166dc2076bbf84->enter($__internal_7c11bfdafd6b16c73e87854116de415291ded92c46c0fba6f5166dc2076bbf84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 13
        echo "
    ";
        // line 14
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 15
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
        // line 17
        echo "
     ";
        
        $__internal_7c11bfdafd6b16c73e87854116de415291ded92c46c0fba6f5166dc2076bbf84->leave($__internal_7c11bfdafd6b16c73e87854116de415291ded92c46c0fba6f5166dc2076bbf84_prof);

        
        $__internal_906e87fa9be636c8d2ae3c6f936ecf703daf668a420b7902249c1fcbed5631b6->leave($__internal_906e87fa9be636c8d2ae3c6f936ecf703daf668a420b7902249c1fcbed5631b6_prof);

    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
        $__internal_8ddaefa529b0e799d31553db65fd67bd0253725560864ff55fc45fa9f2e97669 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ddaefa529b0e799d31553db65fd67bd0253725560864ff55fc45fa9f2e97669->enter($__internal_8ddaefa529b0e799d31553db65fd67bd0253725560864ff55fc45fa9f2e97669_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a757773127bd41631047cdcebb17f115b26949629af9a6c8da7152c78fad6435 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a757773127bd41631047cdcebb17f115b26949629af9a6c8da7152c78fad6435->enter($__internal_a757773127bd41631047cdcebb17f115b26949629af9a6c8da7152c78fad6435_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 22
        echo "
<div class=\"right_col\" role=\"main\" style=\"min-height: 3742px;\">
<div class=\"row\">
          <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
              <div class=\"x_title\">
                <h2> Raccourcis  </h2>
                <ul class=\"nav navbar-right panel_toolbox\">
                  <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                  </li>
                  <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                  </li>
                </ul>
                <div class=\"clearfix\"></div>
              </div>
              <div class=\"x_content\">
                <br>

                <a class=\"btn btn-app\" href=\"/template/add\">
                  <i class=\"fa fa-plus\"></i> Ajouter template
                </a>

</table>


</div>
</div>
          </div>
          </div>

  <div class=\"row\">
              <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2> Liste des templates  </h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">
                    <br>

  <div id=\"datatable_wrapper\" class=\"dataTables_wrapper form-inline dt-bootstrap no-footer\">
<h1> Templates  </h1>
<table  id=\"datatable\" class=\"table table-striped table-bordered dataTable no-footer\" role=\"grid\" aria-describedby=\"datatable_info\" role=\"grid\" aria-describedby=\"datatable_info\">
<thead>
<tr role=\"row\">
<th> Titre </th>
<th> Categorie  </th>
<th> Client </th>
<th> Commentaire </th>
<th> Sujet </th>
<th> Action </th>
</tr>
</thead>

";
        // line 82
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["templates"]) ? $context["templates"] : $this->getContext($context, "templates")));
        foreach ($context['_seq'] as $context["_key"] => $context["template"]) {
            // line 83
            echo "\t<tr>
\t\t<td> ";
            // line 84
            echo twig_escape_filter($this->env, $this->getAttribute($context["template"], "titre", array()), "html", null, true);
            echo " </td>
\t\t<td>
\t\t\t";
            // line 86
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["template"], "categorie", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["entry"]) {
                // line 87
                echo "\t\t\t\t";
                echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "nomCategorie", array()), "html", null, true);
                echo "
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 89
            echo "\t\t</td>
        <td> ";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["template"], "client", array()), "clientName", array()), "html", null, true);
            echo " </td>
        <td> ";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["template"], "commentaire", array()), "html", null, true);
            echo " </td>
        <td> ";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($context["template"], "sujet", array()), "html", null, true);
            echo "</td>
\t\t<td>
<a href=\"";
            // line 94
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("template_update", array("idTemplate" => $this->getAttribute($context["template"], "Id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\"><i class=\"glyphicon glyphicon-pencil\"> </i>  Mettre à jour </a>
<a href=\"";
            // line 95
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("template_diffusion_send", array("idTemplate" => $this->getAttribute($context["template"], "Id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\"><i class=\"glyphicon glyphicon-envelope\"> </i>  Envoyer Mail </a>
<a href=\"";
            // line 96
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("template_delete", array("idTemplate" => $this->getAttribute($context["template"], "Id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\"><i class=\"glyphicon glyphicon-trash\"> </i>  Supprimer template </a>
<a href=\"";
            // line 97
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("information_diffusion_client_history", array("idTemplate" => $this->getAttribute($context["template"], "Id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\"><i class=\"glyphicon glyphicon-check\"> </i>  Historique </a>



     </td>
    </tr>


";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['template'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 106
        echo "
</table>

\t\t\t\t  </div>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
  </div>
</div>



";
        
        $__internal_a757773127bd41631047cdcebb17f115b26949629af9a6c8da7152c78fad6435->leave($__internal_a757773127bd41631047cdcebb17f115b26949629af9a6c8da7152c78fad6435_prof);

        
        $__internal_8ddaefa529b0e799d31553db65fd67bd0253725560864ff55fc45fa9f2e97669->leave($__internal_8ddaefa529b0e799d31553db65fd67bd0253725560864ff55fc45fa9f2e97669_prof);

    }

    // line 120
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_bad06b183c2da4e38817bf4da3ba2977ea19ceb63c2c107c13632d4020470f64 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bad06b183c2da4e38817bf4da3ba2977ea19ceb63c2c107c13632d4020470f64->enter($__internal_bad06b183c2da4e38817bf4da3ba2977ea19ceb63c2c107c13632d4020470f64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_a836f1b38672dee558b7cf1419338774ef8c95472efc052d1119696b87cea9dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a836f1b38672dee558b7cf1419338774ef8c95472efc052d1119696b87cea9dc->enter($__internal_a836f1b38672dee558b7cf1419338774ef8c95472efc052d1119696b87cea9dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 121
        echo "\t    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 122
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
        }
        unset($context["asset_url"]);
        // line 124
        echo "      ";
        
        $__internal_a836f1b38672dee558b7cf1419338774ef8c95472efc052d1119696b87cea9dc->leave($__internal_a836f1b38672dee558b7cf1419338774ef8c95472efc052d1119696b87cea9dc_prof);

        
        $__internal_bad06b183c2da4e38817bf4da3ba2977ea19ceb63c2c107c13632d4020470f64->leave($__internal_bad06b183c2da4e38817bf4da3ba2977ea19ceb63c2c107c13632d4020470f64_prof);

    }

    public function getTemplateName()
    {
        return "template/templateList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  379 => 124,  323 => 122,  318 => 121,  309 => 120,  287 => 106,  272 => 97,  268 => 96,  264 => 95,  260 => 94,  255 => 92,  251 => 91,  247 => 90,  244 => 89,  235 => 87,  231 => 86,  226 => 84,  223 => 83,  219 => 82,  157 => 22,  148 => 21,  137 => 17,  87 => 15,  83 => 14,  80 => 13,  62 => 12,  52 => 4,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'dashboard.html.twig' %}

{% block header %}
<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

<script src=\"https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js\"></script>
<link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css\"/>

  {% block stylesheets %}

    {% stylesheets '@AppBundle/Resources/public/css/*' filter='cssrewrite' %}
    <link rel=\"stylesheet\" href=\"{{ asset_url }}\" />
    {% endstylesheets %}

     {% endblock %}
{% endblock %}

{% block body %}

<div class=\"right_col\" role=\"main\" style=\"min-height: 3742px;\">
<div class=\"row\">
          <div class=\"col-md-12 col-sm-12 col-xs-12\">
            <div class=\"x_panel\">
              <div class=\"x_title\">
                <h2> Raccourcis  </h2>
                <ul class=\"nav navbar-right panel_toolbox\">
                  <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                  </li>
                  <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                  </li>
                </ul>
                <div class=\"clearfix\"></div>
              </div>
              <div class=\"x_content\">
                <br>

                <a class=\"btn btn-app\" href=\"/template/add\">
                  <i class=\"fa fa-plus\"></i> Ajouter template
                </a>

</table>


</div>
</div>
          </div>
          </div>

  <div class=\"row\">
              <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2> Liste des templates  </h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">
                    <br>

  <div id=\"datatable_wrapper\" class=\"dataTables_wrapper form-inline dt-bootstrap no-footer\">
<h1> Templates  </h1>
<table  id=\"datatable\" class=\"table table-striped table-bordered dataTable no-footer\" role=\"grid\" aria-describedby=\"datatable_info\" role=\"grid\" aria-describedby=\"datatable_info\">
<thead>
<tr role=\"row\">
<th> Titre </th>
<th> Categorie  </th>
<th> Client </th>
<th> Commentaire </th>
<th> Sujet </th>
<th> Action </th>
</tr>
</thead>

{% for template in templates %}
\t<tr>
\t\t<td> {{ template.titre }} </td>
\t\t<td>
\t\t\t{% for entry in template.categorie %}
\t\t\t\t{{ entry.nomCategorie }}
\t\t\t{% endfor %}
\t\t</td>
        <td> {{ template.client.clientName }} </td>
        <td> {{ template.commentaire }} </td>
        <td> {{ template.sujet }}</td>
\t\t<td>
<a href=\"{{path('template_update',{'idTemplate' : template.Id})}}\" class=\"btn btn-default btn-xs\"><i class=\"glyphicon glyphicon-pencil\"> </i>  Mettre à jour </a>
<a href=\"{{path('template_diffusion_send',{'idTemplate' : template.Id})}}\" class=\"btn btn-default btn-xs\"><i class=\"glyphicon glyphicon-envelope\"> </i>  Envoyer Mail </a>
<a href=\"{{path('template_delete',{'idTemplate' : template.Id})}}\" class=\"btn btn-default btn-xs\"><i class=\"glyphicon glyphicon-trash\"> </i>  Supprimer template </a>
<a href=\"{{path('information_diffusion_client_history',{'idTemplate' : template.Id})}}\" class=\"btn btn-default btn-xs\"><i class=\"glyphicon glyphicon-check\"> </i>  Historique </a>



     </td>
    </tr>


{% endfor %}

</table>

\t\t\t\t  </div>
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
  </div>
</div>



{% endblock %}

 {% block javascripts %}
\t    {% javascripts '@AppBundle/Resources/public/js/*' %}
\t        <script src=\"{{ asset_url }}\"></script>
\t    {% endjavascripts %}
      {% endblock %}
", "template/templateList.html.twig", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\template\\templateList.html.twig");
    }
}
