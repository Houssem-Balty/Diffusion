<?php

/* @WebProfiler/Profiler/toolbar_redirect.html.twig */
class __TwigTemplate_d2837f8bdd56b416ceef2a5f7056a9107845f721b0be7783197c0e5369d0aac8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@WebProfiler/Profiler/toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b98edc5ea785609a1e299609315aa4603b9609281fe6590f40e1e32e67a71484 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b98edc5ea785609a1e299609315aa4603b9609281fe6590f40e1e32e67a71484->enter($__internal_b98edc5ea785609a1e299609315aa4603b9609281fe6590f40e1e32e67a71484_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b98edc5ea785609a1e299609315aa4603b9609281fe6590f40e1e32e67a71484->leave($__internal_b98edc5ea785609a1e299609315aa4603b9609281fe6590f40e1e32e67a71484_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_3a501d4729909ec39d80ccba263515bf0ed4a6dd9df531f6d42690bcbbeee9ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3a501d4729909ec39d80ccba263515bf0ed4a6dd9df531f6d42690bcbbeee9ee->enter($__internal_3a501d4729909ec39d80ccba263515bf0ed4a6dd9df531f6d42690bcbbeee9ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_3a501d4729909ec39d80ccba263515bf0ed4a6dd9df531f6d42690bcbbeee9ee->leave($__internal_3a501d4729909ec39d80ccba263515bf0ed4a6dd9df531f6d42690bcbbeee9ee_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_425215ea53e67b7f54c4c500fb188f360907c577b47ed72722569ce2375abd2e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_425215ea53e67b7f54c4c500fb188f360907c577b47ed72722569ce2375abd2e->enter($__internal_425215ea53e67b7f54c4c500fb188f360907c577b47ed72722569ce2375abd2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : null), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_425215ea53e67b7f54c4c500fb188f360907c577b47ed72722569ce2375abd2e->leave($__internal_425215ea53e67b7f54c4c500fb188f360907c577b47ed72722569ce2375abd2e_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@WebProfiler/Profiler/toolbar_redirect.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\toolbar_redirect.html.twig");
    }
}
