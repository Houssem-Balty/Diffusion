<?php

/* @FOSUser/Resetting/email.txt.twig */
class __TwigTemplate_6f9d161e112f7f65925da03cfe57d5ce77e97033121efcac4cfb21b60b98397a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2c5e7d22cec76716d953502a9d47ec003c57781988da9d85e692c34eb644724c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2c5e7d22cec76716d953502a9d47ec003c57781988da9d85e692c34eb644724c->enter($__internal_2c5e7d22cec76716d953502a9d47ec003c57781988da9d85e692c34eb644724c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_2c5e7d22cec76716d953502a9d47ec003c57781988da9d85e692c34eb644724c->leave($__internal_2c5e7d22cec76716d953502a9d47ec003c57781988da9d85e692c34eb644724c_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_b75a111ca2d87938b03d6587014c60eb613ab1676b2538fa56c44d0122c86a5e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b75a111ca2d87938b03d6587014c60eb613ab1676b2538fa56c44d0122c86a5e->enter($__internal_b75a111ca2d87938b03d6587014c60eb613ab1676b2538fa56c44d0122c86a5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array())), "FOSUserBundle");
        
        $__internal_b75a111ca2d87938b03d6587014c60eb613ab1676b2538fa56c44d0122c86a5e->leave($__internal_b75a111ca2d87938b03d6587014c60eb613ab1676b2538fa56c44d0122c86a5e_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_67edf1c7bcdb7d833c5d1e4905f7451d15d5d0eb7e89ac89f7fcba4294e419b9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_67edf1c7bcdb7d833c5d1e4905f7451d15d5d0eb7e89ac89f7fcba4294e419b9->enter($__internal_67edf1c7bcdb7d833c5d1e4905f7451d15d5d0eb7e89ac89f7fcba4294e419b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null)), "FOSUserBundle");
        echo "
";
        
        $__internal_67edf1c7bcdb7d833c5d1e4905f7451d15d5d0eb7e89ac89f7fcba4294e419b9->leave($__internal_67edf1c7bcdb7d833c5d1e4905f7451d15d5d0eb7e89ac89f7fcba4294e419b9_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_022b7df2318b7eac74c12c43aada00ab7d4c8199321d9e90c1bcfe59e583fe7c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_022b7df2318b7eac74c12c43aada00ab7d4c8199321d9e90c1bcfe59e583fe7c->enter($__internal_022b7df2318b7eac74c12c43aada00ab7d4c8199321d9e90c1bcfe59e583fe7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_022b7df2318b7eac74c12c43aada00ab7d4c8199321d9e90c1bcfe59e583fe7c->leave($__internal_022b7df2318b7eac74c12c43aada00ab7d4c8199321d9e90c1bcfe59e583fe7c_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  67 => 13,  58 => 10,  52 => 8,  45 => 4,  39 => 2,  32 => 13,  30 => 8,  27 => 7,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Resetting/email.txt.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Resetting\\email.txt.twig");
    }
}
