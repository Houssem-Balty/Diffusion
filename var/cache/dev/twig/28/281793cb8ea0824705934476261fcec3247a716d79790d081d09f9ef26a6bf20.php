<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_3bef4b01357cf959310620919607ba8f75f0fe7c97335df7c5cbf9088a4e6331 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0bd3afee108dab81742bfb097a8dafdd81956ade606aa0b268e8df603df840d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0bd3afee108dab81742bfb097a8dafdd81956ade606aa0b268e8df603df840d9->enter($__internal_0bd3afee108dab81742bfb097a8dafdd81956ade606aa0b268e8df603df840d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0bd3afee108dab81742bfb097a8dafdd81956ade606aa0b268e8df603df840d9->leave($__internal_0bd3afee108dab81742bfb097a8dafdd81956ade606aa0b268e8df603df840d9_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_3c2431490f3cc34c256d64310de63e78ee11952adec84dd13cabc45720facde1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3c2431490f3cc34c256d64310de63e78ee11952adec84dd13cabc45720facde1->enter($__internal_3c2431490f3cc34c256d64310de63e78ee11952adec84dd13cabc45720facde1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_3c2431490f3cc34c256d64310de63e78ee11952adec84dd13cabc45720facde1->leave($__internal_3c2431490f3cc34c256d64310de63e78ee11952adec84dd13cabc45720facde1_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_4a8fa35084300965998c36d356f103543e4de619339fc2cb3b2c04c67c60e274 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a8fa35084300965998c36d356f103543e4de619339fc2cb3b2c04c67c60e274->enter($__internal_4a8fa35084300965998c36d356f103543e4de619339fc2cb3b2c04c67c60e274_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_4a8fa35084300965998c36d356f103543e4de619339fc2cb3b2c04c67c60e274->leave($__internal_4a8fa35084300965998c36d356f103543e4de619339fc2cb3b2c04c67c60e274_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_59c781acec567edaaa52a76cb746f50a7ffce150918a8c86f8c3af4994f9e5a0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_59c781acec567edaaa52a76cb746f50a7ffce150918a8c86f8c3af4994f9e5a0->enter($__internal_59c781acec567edaaa52a76cb746f50a7ffce150918a8c86f8c3af4994f9e5a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : null))));
        echo "
";
        
        $__internal_59c781acec567edaaa52a76cb746f50a7ffce150918a8c86f8c3af4994f9e5a0->leave($__internal_59c781acec567edaaa52a76cb746f50a7ffce150918a8c86f8c3af4994f9e5a0_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "WebProfilerBundle:Collector:router.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
