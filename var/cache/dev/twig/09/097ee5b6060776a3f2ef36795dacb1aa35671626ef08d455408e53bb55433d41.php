<?php

/* @FOSUser/Group/new.html.twig */
class __TwigTemplate_5a3c05afb877cbcbed1df1c1d2ce9daa22c9908b8cf7b45137fa81cbcabc5d41 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2468d863f5d907a3b3fcbe441a3bd32c75084c6eadb740ed996ae874bb4b89a2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2468d863f5d907a3b3fcbe441a3bd32c75084c6eadb740ed996ae874bb4b89a2->enter($__internal_2468d863f5d907a3b3fcbe441a3bd32c75084c6eadb740ed996ae874bb4b89a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2468d863f5d907a3b3fcbe441a3bd32c75084c6eadb740ed996ae874bb4b89a2->leave($__internal_2468d863f5d907a3b3fcbe441a3bd32c75084c6eadb740ed996ae874bb4b89a2_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_819481a92795186efb93792d188982e25422d9d7ee0f2e119e162a1e576cfa0c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_819481a92795186efb93792d188982e25422d9d7ee0f2e119e162a1e576cfa0c->enter($__internal_819481a92795186efb93792d188982e25422d9d7ee0f2e119e162a1e576cfa0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "@FOSUser/Group/new.html.twig", 4)->display($context);
        
        $__internal_819481a92795186efb93792d188982e25422d9d7ee0f2e119e162a1e576cfa0c->leave($__internal_819481a92795186efb93792d188982e25422d9d7ee0f2e119e162a1e576cfa0c_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Group/new.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Group\\new.html.twig");
    }
}
