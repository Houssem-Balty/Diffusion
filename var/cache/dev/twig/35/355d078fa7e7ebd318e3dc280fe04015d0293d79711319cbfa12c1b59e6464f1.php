<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_7c6dd371d4bfdb5e29e3a5893b622d5b596c3f7cfd4b3b90ad5c5e3b057d8e8e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c7e597cdcd2425d40ffc6d6ac912bc5c581d2c2b3747b2a7562477df53985036 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c7e597cdcd2425d40ffc6d6ac912bc5c581d2c2b3747b2a7562477df53985036->enter($__internal_c7e597cdcd2425d40ffc6d6ac912bc5c581d2c2b3747b2a7562477df53985036_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c7e597cdcd2425d40ffc6d6ac912bc5c581d2c2b3747b2a7562477df53985036->leave($__internal_c7e597cdcd2425d40ffc6d6ac912bc5c581d2c2b3747b2a7562477df53985036_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_fd975d050e4a830c191249a8bb6fdba5b6508cb0ea5f5f031e18bc498271ff34 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd975d050e4a830c191249a8bb6fdba5b6508cb0ea5f5f031e18bc498271ff34->enter($__internal_fd975d050e4a830c191249a8bb6fdba5b6508cb0ea5f5f031e18bc498271ff34_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_fd975d050e4a830c191249a8bb6fdba5b6508cb0ea5f5f031e18bc498271ff34->leave($__internal_fd975d050e4a830c191249a8bb6fdba5b6508cb0ea5f5f031e18bc498271ff34_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_76ed5935ad226734c8d6c49749b50067f3a71ddc3e050177801331d00956cc0a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_76ed5935ad226734c8d6c49749b50067f3a71ddc3e050177801331d00956cc0a->enter($__internal_76ed5935ad226734c8d6c49749b50067f3a71ddc3e050177801331d00956cc0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_76ed5935ad226734c8d6c49749b50067f3a71ddc3e050177801331d00956cc0a->leave($__internal_76ed5935ad226734c8d6c49749b50067f3a71ddc3e050177801331d00956cc0a_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_145a419d77d98370c15120a8067dc13db041af995cc1d9e1f24d733d538bb8b5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_145a419d77d98370c15120a8067dc13db041af995cc1d9e1f24d733d538bb8b5->enter($__internal_145a419d77d98370c15120a8067dc13db041af995cc1d9e1f24d733d538bb8b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : null))));
        echo "
";
        
        $__internal_145a419d77d98370c15120a8067dc13db041af995cc1d9e1f24d733d538bb8b5->leave($__internal_145a419d77d98370c15120a8067dc13db041af995cc1d9e1f24d733d538bb8b5_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@WebProfiler/Collector/router.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
