<?php

/* diffusion/email.html */
class __TwigTemplate_d98ad96123b23b5002dea437176ba557d48ebfdb5abda949c36793e784b0cc54 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_36ec247b9b31e4289254ce75d0d6933cbdc868c2c85e13107bb697e1a886c6dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_36ec247b9b31e4289254ce75d0d6933cbdc868c2c85e13107bb697e1a886c6dc->enter($__internal_36ec247b9b31e4289254ce75d0d6933cbdc868c2c85e13107bb697e1a886c6dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "diffusion/email.html"));

        // line 1
        echo "<!DOCTYPE html>
<html> 
    <head> 
        <title> Titre de l'incident </title>
        <style>
            /********************************************** Configuration Initiale *********************************************************/ 
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    max-width: 200px;
    max-height: 200px;
}


ul li{
    list-style: none ;
    display: inline;
    margin-left: 20%;

}

html{
    background-color: #fff;
    color: #555;
    font-family: 'Arial',sans-serif,'Lato';
    font-size: 15px;
    font-weight: 300;
    text-rendering: optimizeLegibility;

}



/************************************************** Reusable components **************************************************************/

header{
    border-bottom: 1px grey solid ;
}

.jauge{
    width: 30%;
}


.main-container{
    background-color: #fff;
    width: 50%;
    height: 100%;
    margin-top: 40px;
    margin-left: auto; 
    margin-right: auto;
    border: 1px black solid ;
    
   
    
}

td{
    height: 40px;
    border-bottom: 1px
}

.status-resolu{
    background-color: chartreuse;
    height: 10%;
    text-align: center;
    color: white;
    
}


.status-encours{
    background-color: orangered;
    height: 10%;
    text-align: center;
    color: white;
    
}


.impacts{
    text-align: center;
    width: 200px;
    height: 200px;
}

.client{
    width: 50%;
    text-align: center;
}


.mail-content{
    margin : 10px 10px 10px 10px ;

}

.mail-content h2{
text-align: center;
margin-bottom: 20px;

}

.logo{
    margin: 20px 0 20px 20px  ;
    width: 30%;
    height: auto;
}

.incident-info{
    width: 100%;
}



.incident-info : first-child{
    background-color: green;
    color: white;
}



.footer {
    background-color: rgb(231,54,20);
    color: white;
    margin-top: 20%;
    padding-top: 2%;
    padding-bottom: 2%;
}
            
        </style>
    
    </head>
    
    <body>
        <div class=\"main-container\"> 
            <header> 
                 <img class=\"logo\" src=\"http://img1.lesnumeriques.com/news/56/56759/cdiscount_logo.jpg\"/>
                
            </header>
            
            <div class=\"mail-content\">
                <h2> ";
        // line 144
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["incident"]) ? $context["incident"] : null), "getTitre", array(), "method"), "html", null, true);
        echo " </h2>
                
              
                </table>
                
                <div> 
                
                    <table>
                        <tr>
                            <td class=\"client\"> ";
        // line 153
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["incident"]) ? $context["incident"] : null), "client", array()), "clientName", array()), "html", null, true);
        echo "</td>
                            <td class=\"status-resolu\"> <b> ";
        // line 154
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["incident"]) ? $context["incident"] : null), "statut", array()), "html", null, true);
        echo " </b> </td>
                        </tr>
                        
                        <tr>
                            <td colspan=\"2\">
                                Bonjour, <br> 
                                Nous recontrons actuellement un incident sur les metiers suivants :
                            </td>
                            
                        </tr>
                        
                         <tr> 
                        <th> Nom Metier </th>
                        <th> Niveau impact </th>
                        </tr>
                        
                         <tr class=\"impacts\"> 
                            <td> QOS </td>
                            <td> <img class=\"jauge\" src=\"http://www.navocap.com/custom/img/navocap/suivi-client-SAEIV.png\"/> </td>
                        </tr>
                         <tr class=\"impacts\"> 
                            <td> DRC </td>
                            <td> <img class=\"jauge\" src=\"http://www.navocap.com/custom/img/navocap/suivi-client-SAEIV.png\"/> </td>
                        </tr>
                         <tr class=\"impacts\"> 
                            <td> Supply Chain </td>
                            <td> <img class=\"jauge\" src=\"http://www.navocap.com/custom/img/navocap/suivi-client-SAEIV.png\"/> </td>
                        </tr>
     
                        <tr> 
                            <td> Temps de résolution approximatif :</td>
                            <td> 30 minutes </td>
                        </tr>
                        
                        <tr> 
                            <td> Heure de début : </td>
                            <td> 05/05/2017, 11:11 </td>
                        </tr>
                        
                         <tr> 
                            <td> Date de fin prévue : </td>
                            <td> 05/05/2017, 11:41 </td>
                        </tr>
                        
                        <tr> 
                            <td> Ressenti Utilisateur : </td>
                            <td> Une baisse de la prise de commandes a été constatée </td>
                        </tr>
                        
                         <tr> 
                            <td> Descriptions Techniques : </td>
                            <td> Un traitement batch a provoqué des blocages sur les bases de données </td>
                        </tr>
                        
                        
                       
                    </table>
                    
                    
                    
        
                </div>
                
            
            </div>
            
        <div class=\"footer\">
            <ul> 
                <li> Logo  </li>
                <li> Link   </li>
                <li> Whatever  </li>
            </ul>
            
            <br> 
            
        
        </div>
        </div>
    </body>

</html>";
        
        $__internal_36ec247b9b31e4289254ce75d0d6933cbdc868c2c85e13107bb697e1a886c6dc->leave($__internal_36ec247b9b31e4289254ce75d0d6933cbdc868c2c85e13107bb697e1a886c6dc_prof);

    }

    public function getTemplateName()
    {
        return "diffusion/email.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 154,  179 => 153,  167 => 144,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "diffusion/email.html", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\diffusion\\email.html");
    }
}
