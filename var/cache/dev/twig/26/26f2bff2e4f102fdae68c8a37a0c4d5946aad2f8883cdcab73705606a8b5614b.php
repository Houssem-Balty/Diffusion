<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_d3e3e28b48eca8cb302c2ea30f200baaa22bb2be22667f17a0d2ff8efd547f85 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3d955ad1a69c0c59c90d9140f0dd7213f197a6680fecbfa81befe6b1ddc2393 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b3d955ad1a69c0c59c90d9140f0dd7213f197a6680fecbfa81befe6b1ddc2393->enter($__internal_b3d955ad1a69c0c59c90d9140f0dd7213f197a6680fecbfa81befe6b1ddc2393_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        // line 1
        echo "

";
        // line 3
        $this->displayBlock('fos_user_content', $context, $blocks);
        
        $__internal_b3d955ad1a69c0c59c90d9140f0dd7213f197a6680fecbfa81befe6b1ddc2393->leave($__internal_b3d955ad1a69c0c59c90d9140f0dd7213f197a6680fecbfa81befe6b1ddc2393_prof);

    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_cd5839bfa2820c0438c8f635ba8159e5d90a8df58dd7ad7614119ff5f1bae472 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cd5839bfa2820c0438c8f635ba8159e5d90a8df58dd7ad7614119ff5f1bae472->enter($__internal_cd5839bfa2820c0438c8f635ba8159e5d90a8df58dd7ad7614119ff5f1bae472_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_cd5839bfa2820c0438c8f635ba8159e5d90a8df58dd7ad7614119ff5f1bae472->leave($__internal_cd5839bfa2820c0438c8f635ba8159e5d90a8df58dd7ad7614119ff5f1bae472_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  39 => 4,  27 => 3,  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Security:login.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Security/login.html.twig");
    }
}
