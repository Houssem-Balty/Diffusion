<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_7efc97de619e69ee519630836b9365c4731cbbee8b0da430658dd474a91056f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_29044057010994285a7e064578efab2b9e32dae5355c7350728513c8b66c7d95 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_29044057010994285a7e064578efab2b9e32dae5355c7350728513c8b66c7d95->enter($__internal_29044057010994285a7e064578efab2b9e32dae5355c7350728513c8b66c7d95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_0755b7a84d30cff8eef0dab61a98b7abc404176be6c27fad13b543b8b562b2a2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0755b7a84d30cff8eef0dab61a98b7abc404176be6c27fad13b543b8b562b2a2->enter($__internal_0755b7a84d30cff8eef0dab61a98b7abc404176be6c27fad13b543b8b562b2a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        // line 1
        echo "

";
        // line 3
        $this->displayBlock('fos_user_content', $context, $blocks);
        
        $__internal_29044057010994285a7e064578efab2b9e32dae5355c7350728513c8b66c7d95->leave($__internal_29044057010994285a7e064578efab2b9e32dae5355c7350728513c8b66c7d95_prof);

        
        $__internal_0755b7a84d30cff8eef0dab61a98b7abc404176be6c27fad13b543b8b562b2a2->leave($__internal_0755b7a84d30cff8eef0dab61a98b7abc404176be6c27fad13b543b8b562b2a2_prof);

    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_9e6cfe5adb43d8676ba1d7a0014bdbf1a54caea5950575c1c8437e655864c09f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9e6cfe5adb43d8676ba1d7a0014bdbf1a54caea5950575c1c8437e655864c09f->enter($__internal_9e6cfe5adb43d8676ba1d7a0014bdbf1a54caea5950575c1c8437e655864c09f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_0a89d2991821ad87ad721cca0ea7eab23c1900f87f30842d6e47088067d148c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0a89d2991821ad87ad721cca0ea7eab23c1900f87f30842d6e47088067d148c0->enter($__internal_0a89d2991821ad87ad721cca0ea7eab23c1900f87f30842d6e47088067d148c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_0a89d2991821ad87ad721cca0ea7eab23c1900f87f30842d6e47088067d148c0->leave($__internal_0a89d2991821ad87ad721cca0ea7eab23c1900f87f30842d6e47088067d148c0_prof);

        
        $__internal_9e6cfe5adb43d8676ba1d7a0014bdbf1a54caea5950575c1c8437e655864c09f->leave($__internal_9e6cfe5adb43d8676ba1d7a0014bdbf1a54caea5950575c1c8437e655864c09f_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  48 => 4,  30 => 3,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "@FOSUser/Security/login.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Security\\login.html.twig");
    }
}
