<?php

/* @FOSUser/Resetting/check_email.html.twig */
class __TwigTemplate_c2c2d75a50780a29a7e2d86d946949b6053ab0c55c52f41e7f6f1c7879d93855 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Resetting/check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1e99a1e234240827723d91233c25d6dd21029536c815e46acd1e1fc2da30b1e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1e99a1e234240827723d91233c25d6dd21029536c815e46acd1e1fc2da30b1e8->enter($__internal_1e99a1e234240827723d91233c25d6dd21029536c815e46acd1e1fc2da30b1e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1e99a1e234240827723d91233c25d6dd21029536c815e46acd1e1fc2da30b1e8->leave($__internal_1e99a1e234240827723d91233c25d6dd21029536c815e46acd1e1fc2da30b1e8_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_7e44c3e26ee35533aa30137ab9bb09d7769beef9e0e98af540d62cda373ea747 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7e44c3e26ee35533aa30137ab9bb09d7769beef9e0e98af540d62cda373ea747->enter($__internal_7e44c3e26ee35533aa30137ab9bb09d7769beef9e0e98af540d62cda373ea747_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => (isset($context["tokenLifetime"]) ? $context["tokenLifetime"] : null)), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_7e44c3e26ee35533aa30137ab9bb09d7769beef9e0e98af540d62cda373ea747->leave($__internal_7e44c3e26ee35533aa30137ab9bb09d7769beef9e0e98af540d62cda373ea747_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 7,  40 => 6,  34 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Resetting/check_email.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Resetting\\check_email.html.twig");
    }
}
