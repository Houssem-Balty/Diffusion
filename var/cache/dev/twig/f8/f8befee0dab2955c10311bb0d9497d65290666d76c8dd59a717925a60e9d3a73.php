<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_8a8d9a2e01566e099b7cde4a5eb88d308b014c4b93cddbd7d0ebbccfad8c767e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f5bbf140ec22e2c17f17769e6260784da9003783a56d4628113a0a169fbf71a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f5bbf140ec22e2c17f17769e6260784da9003783a56d4628113a0a169fbf71a7->enter($__internal_f5bbf140ec22e2c17f17769e6260784da9003783a56d4628113a0a169fbf71a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f5bbf140ec22e2c17f17769e6260784da9003783a56d4628113a0a169fbf71a7->leave($__internal_f5bbf140ec22e2c17f17769e6260784da9003783a56d4628113a0a169fbf71a7_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_1bf6eefada1993ed1f2119d3833a82c359947cbf7edbc5fa76b206b8b96fbee4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1bf6eefada1993ed1f2119d3833a82c359947cbf7edbc5fa76b206b8b96fbee4->enter($__internal_1bf6eefada1993ed1f2119d3833a82c359947cbf7edbc5fa76b206b8b96fbee4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_1bf6eefada1993ed1f2119d3833a82c359947cbf7edbc5fa76b206b8b96fbee4->leave($__internal_1bf6eefada1993ed1f2119d3833a82c359947cbf7edbc5fa76b206b8b96fbee4_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Group:new.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Group/new.html.twig");
    }
}
