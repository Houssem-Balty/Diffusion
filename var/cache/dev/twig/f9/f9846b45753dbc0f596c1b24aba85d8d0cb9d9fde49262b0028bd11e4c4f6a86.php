<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_d203c1a0a12db891a8d627aad8e6c743450ee0dc6eefac5cbe55333965ac9f8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d2b91de68e8ab43c4bdc1ebce5492a149cf9f87da1b4f81647552b5549ae5bbb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d2b91de68e8ab43c4bdc1ebce5492a149cf9f87da1b4f81647552b5549ae5bbb->enter($__internal_d2b91de68e8ab43c4bdc1ebce5492a149cf9f87da1b4f81647552b5549ae5bbb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_d2b91de68e8ab43c4bdc1ebce5492a149cf9f87da1b4f81647552b5549ae5bbb->leave($__internal_d2b91de68e8ab43c4bdc1ebce5492a149cf9f87da1b4f81647552b5549ae5bbb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/FormTable/button_row.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\FormTable\\button_row.html.php");
    }
}
