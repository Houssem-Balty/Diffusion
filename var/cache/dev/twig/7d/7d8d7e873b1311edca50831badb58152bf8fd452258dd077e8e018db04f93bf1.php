<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_79bf1c3b703c57cb4f2d38cc8dd011e20a0a87514305ee605da87f2031f8fbb7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cb7f5954d5abaef19c3720643b6480f61c8c507dce75d26ea6fe8e3c47dcf519 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cb7f5954d5abaef19c3720643b6480f61c8c507dce75d26ea6fe8e3c47dcf519->enter($__internal_cb7f5954d5abaef19c3720643b6480f61c8c507dce75d26ea6fe8e3c47dcf519_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_cb7f5954d5abaef19c3720643b6480f61c8c507dce75d26ea6fe8e3c47dcf519->leave($__internal_cb7f5954d5abaef19c3720643b6480f61c8c507dce75d26ea6fe8e3c47dcf519_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/form_end.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_end.html.php");
    }
}
