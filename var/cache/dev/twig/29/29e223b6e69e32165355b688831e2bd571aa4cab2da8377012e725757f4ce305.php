<?php

/* template/menu.html.twig */
class __TwigTemplate_69b856a440bda2e647471670fd47bfaa54f55e83c46f085a546991dfd4c9e743 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", "template/menu.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf156ead9b486f097ac0d051ccd2f0e5ba20928f6a46ddfbdc8d557785dac9ce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf156ead9b486f097ac0d051ccd2f0e5ba20928f6a46ddfbdc8d557785dac9ce->enter($__internal_cf156ead9b486f097ac0d051ccd2f0e5ba20928f6a46ddfbdc8d557785dac9ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "template/menu.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cf156ead9b486f097ac0d051ccd2f0e5ba20928f6a46ddfbdc8d557785dac9ce->leave($__internal_cf156ead9b486f097ac0d051ccd2f0e5ba20928f6a46ddfbdc8d557785dac9ce_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_1fe921e60750d32f53c20bc83a2e53839124dca75e0613615b8e7663d7d1518d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1fe921e60750d32f53c20bc83a2e53839124dca75e0613615b8e7663d7d1518d->enter($__internal_1fe921e60750d32f53c20bc83a2e53839124dca75e0613615b8e7663d7d1518d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

  
  ";
        // line 10
        $this->displayBlock('stylesheets', $context, $blocks);
        
        $__internal_1fe921e60750d32f53c20bc83a2e53839124dca75e0613615b8e7663d7d1518d->leave($__internal_1fe921e60750d32f53c20bc83a2e53839124dca75e0613615b8e7663d7d1518d_prof);

    }

    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_ce70b122b5541e96e470b4ccf67e3df81263640a825daaf170df13962b8aa150 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ce70b122b5541e96e470b4ccf67e3df81263640a825daaf170df13962b8aa150->enter($__internal_ce70b122b5541e96e470b4ccf67e3df81263640a825daaf170df13962b8aa150_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 11
        echo "      
    ";
        // line 12
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 13
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
        // line 15
        echo "      
     ";
        
        $__internal_ce70b122b5541e96e470b4ccf67e3df81263640a825daaf170df13962b8aa150->leave($__internal_ce70b122b5541e96e470b4ccf67e3df81263640a825daaf170df13962b8aa150_prof);

    }

    // line 19
    public function block_body($context, array $blocks = array())
    {
        $__internal_3354b3d3d73f978e98d102470b147ab241ac043166488ac2569b00a8218bffb1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3354b3d3d73f978e98d102470b147ab241ac043166488ac2569b00a8218bffb1->enter($__internal_3354b3d3d73f978e98d102470b147ab241ac043166488ac2569b00a8218bffb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 20
        echo "
<div class=\"right_col\" role=\"main\" style=\"min-height: 478px;\">

        <div class=\"row\">
              <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2>Ajouter Incident <small>Décrire l'incident en Français</small></h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">
                    <br>

              <h3 class=\"box-title\"> Client</h3>
            
<table class=\"table table-bordered table-hover dataTable\">
    <tr>
      <td> NOM </td>
      <td> LANGUE </td>
      <td> ACTION </td>
    </tr>

    ";
        // line 48
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clients"]) ? $context["clients"] : null));
        foreach ($context['_seq'] as $context["clef"] => $context["client"]) {
            // line 49
            echo "    <tr>
        <td> ";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["client"], 0, array(), "array"), "clientName", array()), "html", null, true);
            echo " </td>
        <td> ";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["client"], 0, array(), "array"), "lang", array()), "html", null, true);
            echo " </td>
        <td>
            <a class=\"btn btn-round btn-success\" href=\"";
            // line 53
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("template_list", array("idClient" => $this->getAttribute($this->getAttribute($context["client"], 0, array(), "array"), "id", array()))), "html", null, true);
            echo "\"> Mail </a>
            <a class=\"btn btn-round btn-default\" href=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("information_diffusion_client_history", array("idClient" => $this->getAttribute($this->getAttribute($context["client"], 0, array(), "array"), "id", array()))), "html", null, true);
            echo "\"> historique </a> 
        </td>
    </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['clef'], $context['client'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "
</table>


                  </div>
                  </div>
                </div>
        </div>
        </div>


";
        
        $__internal_3354b3d3d73f978e98d102470b147ab241ac043166488ac2569b00a8218bffb1->leave($__internal_3354b3d3d73f978e98d102470b147ab241ac043166488ac2569b00a8218bffb1_prof);

    }

    // line 71
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_5576cfe355eb0dd69c96e8011a2b9183765b19eefbf496f0cc7f9057313a9253 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5576cfe355eb0dd69c96e8011a2b9183765b19eefbf496f0cc7f9057313a9253->enter($__internal_5576cfe355eb0dd69c96e8011a2b9183765b19eefbf496f0cc7f9057313a9253_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 72
        echo "\t    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 73
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
        }
        unset($context["asset_url"]);
        // line 75
        echo "      ";
        
        $__internal_5576cfe355eb0dd69c96e8011a2b9183765b19eefbf496f0cc7f9057313a9253->leave($__internal_5576cfe355eb0dd69c96e8011a2b9183765b19eefbf496f0cc7f9057313a9253_prof);

    }

    public function getTemplateName()
    {
        return "template/menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  279 => 75,  223 => 73,  218 => 72,  212 => 71,  194 => 58,  184 => 54,  180 => 53,  175 => 51,  171 => 50,  168 => 49,  164 => 48,  134 => 20,  128 => 19,  120 => 15,  70 => 13,  66 => 12,  63 => 11,  51 => 10,  43 => 4,  37 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "template/menu.html.twig", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\template\\menu.html.twig");
    }
}
