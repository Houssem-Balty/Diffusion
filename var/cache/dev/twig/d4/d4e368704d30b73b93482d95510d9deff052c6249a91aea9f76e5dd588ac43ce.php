<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_c3c4cf0b81769ed68b95574956fe4ac1684106931b252266c76d0e2c1d8f478d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_df699bb10a6bf84d11ab1a563cc4819df4309fed099efe520cb1f3f45338a157 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_df699bb10a6bf84d11ab1a563cc4819df4309fed099efe520cb1f3f45338a157->enter($__internal_df699bb10a6bf84d11ab1a563cc4819df4309fed099efe520cb1f3f45338a157_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_df699bb10a6bf84d11ab1a563cc4819df4309fed099efe520cb1f3f45338a157->leave($__internal_df699bb10a6bf84d11ab1a563cc4819df4309fed099efe520cb1f3f45338a157_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/form_errors.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_errors.html.php");
    }
}
