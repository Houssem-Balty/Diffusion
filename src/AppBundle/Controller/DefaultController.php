<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Incident;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Session\Session;
class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     */

 public function indexAction(Request $request)
    {
        								
								
									$qb = $this->getDoctrine()
									->getManager()
									->createQueryBuilder() ;

									// Récupération de tous les incidents non résolus
									
									$qb->select(array('u'))
									->from('AppBundle:Incident', 'u')
									->where('u.statut = :statut')
									->setParameter('statut','Non Résolu')
									->orderBy('u.dateDebut','DESC');
									
									$incidents = $qb->getQuery()->getResult();
										
									// mise à jour des statuts des incidents
									$em = $this->getDoctrine()->getManager();
									$date = new \DateTime();
							
									foreach($incidents as $incident){
											if ( $incident->getDateFin() <= $date && $incident->getDateFin()!=null ){
												$incident->setStatut("Résolu");
												$em->flush();
											}
											
									
									}
								
								
									
								
									return $this->render("default/index.html.twig",array(
															'incidents' => $incidents,
															
															)) ;
									
					
					}
					
					
					
										
					
					
					
					
					
					
					 /**
     * @Route("/settings/", name="settings")
     */

 public function testAction(Request $request)
    {
								return $this->render('default/settings.html.twig');
				}


  




}
