<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use AppBundle\Entity\incident;
use AppBundle\Entity\Template;
use AppBundle\Entity\Client;
use AppBundle\Form\ApplicationType;
use AppBundle\Form\TemplateType;
class TemplateController extends Controller
{


  /**
   * @Route("/template/list/", name="template_list")
   */

public function listAction(Request $request)
  {
        // apporter client
        

        
        $qb = $this->getDoctrine()
		->getManager()
		->createQueryBuilder() ;

		// Récupération de tous les incidents non résolus
									
		$qb->select(array('u'))
		->from('AppBundle:Template', 'u')
		->orderBy("u.client",'ASC');
      
      $templates = $qb->getQuery()->getResult();

      return $this->render("template/templateList.html.twig",array(
        'templates' => $templates,
      ));
  }


    /**
     * @Route("/template/add", name="template_add")
     */

 public function addAction(Request $request)
    {
      // Création d'un objet Login

       $template = new Template();
       $form = $this->createForm(TemplateType::class,$template);
       $form->handleRequest($request);
       if ($form->isSubmitted() && $form->isValid()){
            $template = $form->getData();
            $template->setEmetteur($this->getuser());
            //$template->setVariables($template->getVariables()->toArray());
            var_dump($template->getVariables());
		$em = $this->getDoctrine()->getEntityManager() ;
            $em->persist($template);
            $em->flush();

            return $this->redirect('/') ;

          }


      return $this->render('/template/templateAdd.html.twig',array(
       'form' => $form->createView(),
       ));


    }

    /**
     * @Route("/template/update/{idTemplate}", name="template_update")
     */

 public function updateAction(Request $request,$idTemplate)
    {
        $template = $this->getDoctrine()
        ->getManager()
        ->getRepository("AppBundle:Template")
        ->findOneById($idTemplate);
        $form = $this->createForm(TemplateType::class,$template);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
             $data = $form->getData();
             $template->setEmetteur($this->getuser());
             $em = $this->getDoctrine()->getEntityManager() ;
             foreach($template->getVariables() as $variable)
             {
                $em->flush($variable);
             }
             $em->flush($template);

             return $this->redirect('/') ;
 
           }


       return $this->render('/template/templateUpdate.html.twig',array(
        'form' => $form->createView(),
        ));

    }
    
    
    /**
     * @Route("/template/delete/{idTemplate}", name="template_delete")
     */

 public function deleteAction(Request $request,$idTemplate)
    {
        $template = $this->getDoctrine()->getRepository("AppBundle:Template")->findOneById($idTemplate);
        $em = $this->getDoctrine()->getManager();
        $em->remove($template) ;
        $em->flush();
        return $this->redirect('/');
        
    }
    
    
    /**
     * @Route("/client/information/list", name="client_information_list")
     */

 public function listClientsAction(Request $request)
    {
      $clients = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Client')
            ->createQueryBuilder('c')
            ->getQuery()
            ->iterate();

          return $this->render('template/menu.html.twig', array('clients' => $clients));

    }





}
