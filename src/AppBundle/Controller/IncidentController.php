<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Incident;
use AppBundle\Form\IncidentType;
use AppBundle\Form\ClosingType;
use AppBundle\Form\ImpactMetierType;
use AppBundle\Form\ImpactApplicationType;
use AppBundle\Form\MetierType;


class IncidentController extends Controller
{
    /**
     * @Route("/incident/list", name="incident_list")
     */

  public function listAction(Request $request)
    {
      $lesIncidents = $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('AppBundle:Incident')
      ->createQueryBuilder('c')
			->orderBy('c.dateCreation','ASC')
      ->getQuery()
      ->iterate();

    	return $this->render("incident/incident.html.twig",array(
        'incidents' => $lesIncidents,
      ));
    }




    /**
     * @Route("/incident/add/{idClient}", name="incident_add")
     */

 public function addAction(Request $request,$idClient)
    {

      


      /*
      * Récupération du nom du Client
      */

      $liaisonClient = $this->getDoctrine()
      ->getRepository('AppBundle:Client')
      ->findOneById($idClient);

      $client = $liaisonClient->getClientName();
      $incident = new Incident();
			$incident->setClient($liaisonClient);
      $incident->setStatut("Non Résolu");
			$incident->setEmetteur($this->getUser());
      $form = $this->createForm(IncidentType::class,$incident);


      /*
      * récupération des données  depuis le  formulaire de description générale de l'incident
      */
      
      $form -> handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()){
        $incident = $form->getData();
				$incident->setClient($liaisonClient);
				
				if (!$incident->getDdiConnue())
				   $incident->setDateDebut(null) ;
					 
				if (!$incident->getDfiConnue())
				   $incident->setDateFin(null) ;
				
				if (!$incident->getDdimConnue())
				   $incident->setDateDebutImpact(null) ;
					 
				if (!$incident->getDfimConnue())
				   $incident->setDateFinImpact(null) ;
        $em = $this -> getDoctrine() -> getEntityManager();
        $em->persist($incident) ;
        $em->flush();
				return $this->redirect("/incident/list");
      }



      return $this->render('/incident/incidentAdd.html.twig',array(
          'client' => $liaisonClient,
          'formDeclarerIncident' => $form->createView(),				
          ));

    }

		
		
		
		/**
     * @Route("/incident/new/", name="nouvel_incident")
     */

 public function ajouterAction(Request $request) //Add without client action
    {
			


      $incident = new Incident() ;
      $incident->setStatut("Non Résolu");
			$incident->setEmetteur($this->getUser());
      $form = $this->createForm(IncidentType::class,$incident);


      /*
      * récupération des données  depuis le  formulaire de description générale de l'incident
      */
      
      $form -> handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()){

        $incident = $form->getData();
				/*  Vérification des dates connues ou pas  */

				
				if (!$incident->getDdiConnue())
				   $incident->setDateDebut(null) ;
					 
				if (!$incident->getDfiConnue())
				   $incident->setDateFin(null) ;
				
				if (!$incident->getDdimConnue())
				   $incident->setDateDebutImpact(null) ;
					 
				if (!$incident->getDfimConnue())
				   $incident->setDateFinImpact(null) ;
					

        $em = $this -> getDoctrine() -> getEntityManager();
        $em->persist($incident) ;
        $em->flush();
				return $this->redirect('/incident/list') ;
      }  else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             
                   return $this->render('/incident/incidentAdd.html.twig',array(
                     'formDeclarerIncident' => $form->createView(),
                     'errors' => $errors,
               ));

           }



      return $this->render('/incident/incidentAdd.html.twig',array(
          'formDeclarerIncident' => $form->createView(),
          ));
		}
		
		
		
		
		
    /**
     * @Route("/incident/metier/{idMetier}", name="metier_custom_add_metier")
     */

  public function customAddAction(Request $request,$idMetier)
    {

      $metier = $this
      ->getDoctrine()
      ->getRepository('AppBundle:Metier')
      ->findOneById($idMetier);
			
			$incident = new Incident() ;
      $incident->setClient($metier->getClient());
      $incident->setStatut("Non Résolu");
      $incident->addMetierImpacte($metier);
			$form = $this->createForm(IncidentType::class,$incident);
			
			
			$form -> handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()){
        $incident = $form->getData();
        $em = $this -> getDoctrine() -> getEntityManager();
        $em->persist($incident);
        $em->flush();
      }
			
			
			return  $this->render('/incident/incidentMetierAdd.html.twig',array(
          'formDeclarerIncident' => $form->createView(),
					'incident' => $incident,
          ));
	
     

    }
		
		
		
		 /**
     * @Route("/incident/application/{idApplication}", name="incident_custom_add_application")
     */

  public function customappAddAction(Request $request,$idApplication)
    {

      $application = $this
      ->getDoctrine()
      ->getRepository('AppBundle:Application')
      ->findOneById($idApplication);
			
			$incident = new Incident() ;
			$incident->addApplicationImpactee($application);
			
			
			$form = $this->createForm(IncidentType::class,$incident);
			
			
			$form -> handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()){
        $incident = $form->getData();
				$incident->setStatut("Non Résolu");
				$incident->setClient($application->getMetiers()->get(0)->getClient());
        $em = $this -> getDoctrine() -> getEntityManager();
        $em->persist($incident) ;
        $em->flush();
      }
			
			
			return  $this->render('/incident/incidentApplicationAdd.html.twig',array(
          'formDeclarerIncident' => $form->createView(),
					'incident' => $incident,
          ));
	
     

    }
		
		
		 /**
     * @Route("/incident/clone/{idIncident}", name="incident_clone")
     */

  public function cloneAction(Request $request, $idIncident)
    {
			 $incident = $this->getDoctrine()
			 ->getRepository('AppBundle:Incident')
			 ->findOneById($idIncident) ;
			 
			 $incidentPersist = new Incident();
			 $incidentPersist->setTitre($incident->getTitre());
			 $incidentPersist->setImpact($incident->getImpact());
			 $incidentPersist->setType($incident->getType());
			 $incidentPersist->setClient($incident->getClient()) ;
			 $incidentPersist->setDescTechnique($incident->getDescTechnique());
			 $incidentPersist->setImpactMetier($incident->getImpactMetier());
			 $incidentPersist->setImpactApplication($incident->getImpactApplication());
			 
			 
			 
			 
			 
			 $form = $this->createForm(IncidentType::class,$incidentPersist);
			 $form -> handleRequest($request);
			 if ($form->isSubmitted() && $form->isValid()){
				$incidentPersist = $form->getData();
				$now = new \DateTime('now') ;
				$incidentPersist->setDateCreation($now);
				if ($incidentPersist->getDfiConnue() && $incidentPersist->getDateFin()->getTimeStamp() < $now->getTimeStamp())
						$incidentPersist->setStatut("Résolu");
				else
				$incidentPersist->setStatut("Non Résolu");
				$incidentPersist->setEmetteur($this->getUser());
				$em = $this->getDoctrine()->getEntityManager() ;
				$em->detach($incident);
				$em->persist($incidentPersist) ;
				$em->flush();
				
				return $this->redirectToRoute('incident_list') ;
				} else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             
                   return $this->render('/incident/incidentAdd.html.twig',array(
                     'formDeclarerIncident' => $form->createView(),
                     'errors' => $errors,
               ));

           }
			 
			 	return  $this->render('/incident/incidentClone.html.twig',array(
          'formDeclarerIncident' => $form->createView(),
          ));
	
		}
		
		
		/**
     * @Route("/incident/update/{idIncident}", name="incident_update")
     */

  public function updateAction(Request $request, $idIncident)
    {
			 $incident = $this->getDoctrine()
			 ->getRepository('AppBundle:Incident')
			 ->findOneById($idIncident) ;
			 
			 
			 $form = $this->createForm(IncidentType::class,$incident);
			 $form -> handleRequest($request);
			 if ($form->isSubmitted() && $form->isValid()){
				$incident = $form->getData();
				if (!$incident->getDdiConnue())
				   $incident->setDateDebut(null) ;
					 
				if (!$incident->getDfiConnue())
				   $incident->setDateFin(null) ;
				
				if (!$incident->getDdimConnue())
				   $incident->setDateDebutImpact(null) ;
					 
				if (!$incident->getDfimConnue())
				   $incident->setDateFinImpact(null) ;
				$em = $this->getDoctrine()->getEntityManager() ;
				$em->flush();
				
				return $this->redirectToRoute('incident_list') ;
				}   else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             
                   return $this->render('/incident/incidentClone.html.twig',array(
                     'formDeclarerIncident' => $form->createView(),
                     'errors' => $errors,
               ));

           }
			 
			 	return  $this->render('/incident/incidentClone.html.twig',array(
          'formDeclarerIncident' => $form->createView(),
          ));
	
		}
		
		
		
		
		
		
		  /**
     * @Route("/incident/delete/{idIncident}", name="incident_delete")
     */

  public function deleteAction($idIncident)
    {
			$incident= $this->getDoctrine()->getRepository('AppBundle:Incident')
                ->findOneById($idIncident) ;
                 $em = $this->getDoctrine()->getManager();
                 $em->remove($incident) ;
                 $em->flush();

                 return $this->redirect("/incident/list");


		}
		
				/**
     * @Route("/incident/history/{idIncident}", name="incident_history")
     */

 public function getHistory(Request $request, $idIncident) //Add without client action
    {
				$incident = $this->getDoctrine()->getRepository('AppBundle:Incident')->findOneById($idIncident) ;
				$qb = $this->getDoctrine()->getManager()->createQueryBuilder();
				
				$pos = $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('AppBundle:Pos')
      ->createQueryBuilder('c')
			->where('c.incident = :incident')
			->setParameter('incident',$incident)
			->orderBy('c.nextPosDate','DESC')
      ->getQuery()->getResult();
			

				
				$diffusion= $this
      ->getDoctrine()
      ->getManager()
      ->getRepository('AppBundle:Diffusion')
      ->createQueryBuilder('c')
			->where('c.incident = :incident')
			->setParameter('incident',$incident)
			->orderBy('c.dateEnvoi','DESC')
      ->getQuery()->getResult();
			
			$history = array_merge($pos,$diffusion) ;
			usort ($history,"self::compareDate");
			
			
			return $this->render('/default/history.html.twig',array('history' => $history, 'incident' => $incident)) ;

			
			

				
		}
		
		
		 /**
     * @Route("/incident/clore/{idIncident}", name="incident_clore")
     */

			 public function cloreAction(Request $request, $idIncident) // clôturer un incident
				{
						
			 $incident = $this->getDoctrine()
			 ->getRepository('AppBundle:Incident')
			 ->findOneById($idIncident) ;
	
			$incident->setDateFin(new \DateTime()) ;
			$incident->setDateFinImpact(new \DateTime()) ;
			 
			 $form = $this->createForm(ClosingType::class,$incident);
			 $form -> handleRequest($request);
			  if ($form->isSubmitted() && $form->isValid()){
						$incident = $form->getData();
						$incident->setDfiConnue(1);
						$incident->setDfimConnue(1);
						$incident->setStatut("Résolu");
						$em = $this->getDoctrine()->getManager();
						$em->flush();
						
						return $this->redirectToRoute('incident_list');
					} 
				
			 
			 return $this->render('/incident/incidentClore.html.twig',array('form' => $form->createView(), 'incident' => $incident)) ;
			 
			 
			 }
		
		
		function compareDate($a,$b){
				
				return strcmp($a->getDateEnvoi()->getTimestamp(),$b->getDateEnvoi()->getTimestamp()) ;
			
		}





}
